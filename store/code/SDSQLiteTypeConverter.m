//
// Created by Nail Sharipov on 02/06/15.
// Copyright (c) 2015 Tensor. All rights reserved.
//

#import "SDSQLiteTypeConverter.h"


@implementation SDSQLiteTypeConverter

int const SD_TRUE = 1;
int const SD_FALSE = 0;

+ (NSUInteger)boolToInt:(BOOL)value {
    return value ? SD_TRUE : SD_FALSE;
}

+ (BOOL)intToBool:(NSUInteger)value {
    return value != SD_FALSE;
}

+ (NSDate *)timeStampToDate:(NSUInteger)value {
    return [NSDate dateWithTimeIntervalSince1970:value];
}

+ (NSUInteger)dateToTimeStamp:(NSDate *)value {
    return (NSUInteger)value.timeIntervalSince1970;
}

@end