////////////////////////////////////////////////////////////////////////////////
//
// Created by Nail Sharipov on 01.05.15.
// Copyright (c) 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

@class SDDatabaseManager;
@class FMResultSet;
@class SDModel;

@protocol SDShema <NSObject>

@property (weak, nonatomic) SDDatabaseManager *databaseManager;

- (NSString *)name;
- (NSString *)table;
- (void)creatShemaVersion:(NSUInteger)version;
- (void)dropShemaVersion:(NSUInteger)version;

- (SDModel *)instantiateModel:(FMResultSet *) result;

@end