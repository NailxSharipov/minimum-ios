//
// Created by Nail Sharipov on 02/06/15.
// Copyright (c) 2015 Tensor. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SDSQLiteTypeConverter : NSObject

+ (NSUInteger)boolToInt:(BOOL)value;
+ (BOOL)intToBool:(NSUInteger)value;

+ (NSDate *)timeStampToDate:(NSUInteger)value;
+ (NSUInteger)dateToTimeStamp:(NSDate *)value;

@end