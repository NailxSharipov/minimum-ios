////////////////////////////////////////////////////////////////////////////////
//
// Created by Nail Sharipov on 01.05.15.
// Copyright (c) 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "SDDatabaseManager.h"
#import "FMDB.h"
#import "SDShema.h"

@interface SDDatabaseManager () {
    NSUInteger _dbVersion;
    NSString *_dbName;
    NSMutableDictionary *_shemaDict;
    FMDatabaseQueue *_queue;
}

@end

@implementation SDDatabaseManager

//-------------------------------------------------------------------------------------------
#pragma mark - Initialization & Destruction
//-------------------------------------------------------------------------------------------

- (instancetype)initWithVersion:(NSNumber *)version shemaClassArray:(NSArray<Class> *)shemaClassArray {
    self = [super init];
    if (self != nil) {
        _dbVersion = [version unsignedIntegerValue];
        _shemaDict = [NSMutableDictionary new];
        [self registerShema:shemaClassArray];
    }
    return self;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Public Methods
//-------------------------------------------------------------------------------------------

- (NSString *)tableName:(NSString *)name {
    return [NSString stringWithFormat:@"%@_v%lu", name, (unsigned long)_dbVersion];
}

- (NSString *)tableNameForShema:(id<SDShema>)shema {
    return [self tableName:shema.name];
}

- (NSString *)tableNameForShemaClass:(Class)clazz {
    return [self tableNameForShema:[self shemaForClass:clazz]];
}

- (id<SDShema>)shemaForClass:(Class)clazz {
    NSString *className = NSStringFromClass(clazz);
    return _shemaDict[className];
}

- (void)registerShema:(NSArray *)shemaClassArray {
    for(Class clazz in shemaClassArray) {
        id<SDShema> shema = [[clazz alloc] init];
        NSString *className = NSStringFromClass(clazz);
        _shemaDict[className] = shema;
        shema.databaseManager = self;
    }
}

- (void)openDatabase:(NSString *)name {
    if (![name isEqual: _dbName]) {
        _dbName = [NSString stringWithFormat:@"%@.db", name];
        if (_queue != nil) {
            [self closeDatabase];
        }
        [self openDatabase];
    }
}

- (void)closeDatabase {
    [_queue close];
    _queue = nil;
}

- (void)runInDatabase:(void (^)())block {
    [_queue inDatabase:^(FMDatabase *db) {
        block();
    }];
}

- (void)runInTransaction:(void (^)(BOOL *rollback))block {
    [_queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        _database = db;
        block(rollback);
    }];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (NSString *)dbFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = paths[0];
    return [documentsPath stringByAppendingPathComponent:_dbName];
}

- (void)openDatabase {
    NSString *dbFilePath = self.dbFilePath;
    NSLog(@"open db: %@", dbFilePath);
    _queue = [FMDatabaseQueue databaseQueueWithPath:dbFilePath];
    [_queue inDatabase:^(FMDatabase *db) {
        _database = db;
#ifdef DEBUG
        [db setCrashOnErrors:YES];
        [db setTraceExecution:YES];
#endif
        [db setLogsErrors:YES];
        [db setShouldCacheStatements:YES];
    }];
    [self createShema];
}

- (void)createShema {
    [_queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        NSUInteger currentVersion = [db userVersion];
        if (_dbVersion > currentVersion) {
            if (currentVersion > 0) {
                for (NSString *key in _shemaDict) {
                    id<SDShema> shema = _shemaDict[key];
                    [shema dropShemaVersion:currentVersion];
                }
            }
            for (NSString *key in _shemaDict) {
                id<SDShema> shema = _shemaDict[key];
                [shema creatShemaVersion:_dbVersion];
            }
            [db setUserVersion:(uint32_t)_dbVersion];
        }
    }];
}

@end