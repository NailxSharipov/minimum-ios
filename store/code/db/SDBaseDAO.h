////////////////////////////////////////////////////////////////////////////////
//
//  SDBaseDAO.h
//  store
//
//  Created by Nail Sharipov on 13/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

#import "SDDatabaseManager.h"

@protocol SDBaseDAO <NSObject>

@required

@property (weak, nonatomic) SDDatabaseManager *databaseManager;

@end
