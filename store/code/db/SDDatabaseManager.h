////////////////////////////////////////////////////////////////////////////////
//
// Created by Nail Sharipov on 01.05.15.
// Copyright (c) 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

@class FMDatabase;

@protocol SDShema;

@interface SDDatabaseManager : NSObject

@property (strong, nonatomic, readonly) NSString *name;
@property (weak, nonatomic, readonly) FMDatabase *database;

- (instancetype)initWithVersion:(NSNumber *)version shemaClassArray:(NSArray<Class> *)shemaClassArray;

- (NSString *)tableName:(NSString *)name;

- (NSString *)tableNameForShema:(id<SDShema>)shema;

- (NSString *)tableNameForShemaClass:(Class)clazz;

- (id<SDShema>)shemaForClass:(Class)clazz;

- (void)openDatabase:(NSString *)name;

- (void)closeDatabase;

- (void)runInDatabase:(void (^)())block;

- (void)runInTransaction:(void (^)(BOOL *rollback))block;

- (FMDatabase *)database;

@end