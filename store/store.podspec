Pod::Spec.new do |spec|

  spec.name         = 'store'
  spec.version      = '0.1.0'
  spec.license      = { :type => 'BSD' }
  spec.authors      = 'Nail Sharipov'
  spec.summary      = 'ORM over SQLite'
  spec.requires_arc = true
  spec.source_files = 'code/**/*.{h,m}'

  spec.dependency 'FMDB', '>= 2.6'
  
  
end