////////////////////////////////////////////////////////////////////////////////
//
//  TXTaskExecutor.h
//  core
//
//  Created by Шарипов Н.Г. on 17/12/15.
//  Copyright © 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXTaskExecutor.h"


@interface TXSerialTaskExecutor : NSObject<TXTaskExecutor>

- (instancetype)initWithName:(NSString *)name;


@end
