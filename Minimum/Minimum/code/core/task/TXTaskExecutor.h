////////////////////////////////////////////////////////////////////////////////
//
//  TXTaskExecutor.h
//  core
//
//  Created by Шарипов Н.Г. on 17/12/15.
//  Copyright © 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>


@protocol TXTask;

@protocol TXTaskExecutor <NSObject>

@required

- (void)executeTask:(id<TXTask>)task;

@end