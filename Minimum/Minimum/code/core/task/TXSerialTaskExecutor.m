////////////////////////////////////////////////////////////////////////////////
//
//  TXTaskExecutor.m
//  core
//
//  Created by Шарипов Н.Г. on 17/12/15.
//  Copyright © 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXSerialTaskExecutor.h"
#import "TXTask.h"


@implementation TXSerialTaskExecutor {
    dispatch_queue_t _serialQueue;
    const char *_queueName;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Initialization & Destruction
//-------------------------------------------------------------------------------------------

- (instancetype)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _queueName = [name UTF8String];
        _serialQueue = dispatch_queue_create(_queueName, DISPATCH_QUEUE_SERIAL);
    }
    return self;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTaskExecutor>
//-------------------------------------------------------------------------------------------

- (void)executeTask:(id<TXTask>)task {
    __weak id<TXTask> weakTask = task;
    if (self.isCurrentQueue) {
        [task run];
    } else {
        dispatch_async(_serialQueue, ^{
            @autoreleasepool {
                if (weakTask != nil) {
                    [weakTask run];
                }
            }
        });
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (BOOL)isCurrentQueue {
    return dispatch_get_specific(_queueName) != NULL;
}


@end
