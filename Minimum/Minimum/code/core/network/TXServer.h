////////////////////////////////////////////////////////////////////////////////
//
//  TXServer.h
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>


@protocol TXServerTask;

@protocol TXServer <NSObject>

@required

@property (strong, nonatomic) NSString *domain;
@property (readonly, weak, nonatomic) NSString *rootUrl;

- (void)executeGetTask:(id<TXServerTask>)task;


@end
