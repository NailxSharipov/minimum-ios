////////////////////////////////////////////////////////////////////////////////
//
//  TXServerImpl.m
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXServerImpl.h"
#import "TXServerTask.h"


@implementation TXServerImpl {
    NSURLSession *_getSession;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Initialization & Destruction
//-------------------------------------------------------------------------------------------

- (instancetype)init {
    self = [super init];
    if (self) {
        _getSession = [self buildGetSession];
    }
    return self;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Interface Methods
//-------------------------------------------------------------------------------------------
- (NSString *)rootUrl {
    return [NSString stringWithFormat:@"http://%@/fm.html", _domain];
}


- (void)executeGetTask:(id<TXServerTask>)task {
    __weak id<TXServerTask> weakTask = task;
    NSURLSessionDataTask *dataTask = [_getSession dataTaskWithRequest:task.request
                                                     completionHandler:^(NSData *data, NSURLResponse *response, NSError *httpError) {
                                                         if (weakTask != nil) {
                                                             if (httpError != nil) {
                                                                 [weakTask onFailureWithError:httpError];
                                                             } else {
                                                                 NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                                                 [weakTask onSuccessWithResponse:httpResponse andData:data];
                                                             }
                                                         }
                                                     }];
    
    [weakTask setHttpTask:dataTask];
    [dataTask resume];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (NSURLSession *)buildGetSession {
    NSOperationQueue *queue = [NSOperationQueue new];
    queue.maxConcurrentOperationCount = 10;
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    config.allowsCellularAccess = YES;
    [config setHTTPAdditionalHeaders:@{
                                       @"Accept-Language": @"ru-RU"
                                       }];
    config.timeoutIntervalForRequest = 20.0;
    config.timeoutIntervalForResource = 20.0;
    
    
    return [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:queue];
}


@end
