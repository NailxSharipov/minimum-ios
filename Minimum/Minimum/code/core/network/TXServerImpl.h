////////////////////////////////////////////////////////////////////////////////
//
//  TXServerImpl.h
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXServer.h"


@interface TXServerImpl : NSObject<TXServer, NSURLSessionTaskDelegate>

@property (strong, nonatomic) NSString *domain;
@property (readonly, weak, nonatomic) NSString *rootUrl;

@end
