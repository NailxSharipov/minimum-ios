////////////////////////////////////////////////////////////////////////////////
//
//  TXServerTask.h
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>


@protocol TXServerTask <NSObject>

@required

- (NSURLRequest *)request;

@optional

@property (strong, nonatomic) NSURLSessionTask *httpTask;

- (void)onSuccessWithResponse:(NSHTTPURLResponse *)response andData:(NSData *)data;

- (void)onFailureWithError:(NSError *)error;

- (void)cancel;


@end
