////////////////////////////////////////////////////////////////////////////////
//
//  TXUrlEditor.h
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>


@interface TXUrlEditor : NSObject

- (instancetype) initWithServerURL:(NSString *)url;

- (void)reset;

- (void) addParameter:(NSString *)parameter Value:(NSString *)value;

- (void) addEmptyParameter:(NSString *)emptyParameter;

- (void) setServerURL:(NSString *)url;

- (NSString *) getFormattedURL;

- (NSURL *) getURL;


@end
