////////////////////////////////////////////////////////////////////////////////
//
//  TXUrlEditor.m
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXUrlEditor.h"
#import "NSString+Encode.h"


@implementation TXUrlEditor {
    NSMutableDictionary *parameters;
    NSMutableArray *emptyParameters;
    NSString *serverURL;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Initialization & Destruction
//-------------------------------------------------------------------------------------------

- (id) init {
    self = [super init];
    if (self) {
        [self reset];
    }
    return self;
}

- (instancetype) initWithServerURL:(NSString *)url {
    self = [super init];
    if (self) {
        [self reset];
        serverURL = url;
    }
    return self;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Interface Methods
//-------------------------------------------------------------------------------------------

- (void)reset {
    parameters = [[NSMutableDictionary alloc] init];
    emptyParameters = [[NSMutableArray alloc] init];
    serverURL = @"";
}

- (void) setServerURL:(NSString *)url {
    serverURL = url;
}

- (void) addParameter:(NSString *)parameter Value:(NSString *)value {
    [parameters setObject:value forKey:parameter];
}

- (void) addEmptyParameter:(NSString *)emptyParameter {
    [emptyParameters addObject: emptyParameter];
}

- (NSString *) getFormattedURL {
    NSString *url = [NSString stringWithFormat:@"%@", serverURL];

    {
        NSArray *keys = [parameters allKeys];
        NSUInteger n = [keys count];
        if (n > 0) {
            url = [NSString stringWithFormat:@"%@?", url];
            for(NSUInteger i = 0; i < n - 1;  i++) {
                NSString *parameter = [keys objectAtIndex:i];
                NSString *value = [parameters objectForKey:parameter];
                NSString *encodedValue = [value encodeString];
                url = [NSString stringWithFormat:@"%@%@=%@&", url, parameter, encodedValue];
            }
            NSString *parameter = [keys objectAtIndex:n - 1];
            NSString *value = [parameters objectForKey:parameter];
            NSString *encodedValue = [value encodeString];
            url = [NSString stringWithFormat:@"%@%@=%@", url, parameter, encodedValue];
        }
    } 
    {
        NSUInteger n = [emptyParameters count];
        if (n > 0) {
            if ([parameters count] > 0) {
                url = [NSString stringWithFormat:@"%@&", url];
            } else {
                url = [NSString stringWithFormat:@"%@?", url];
            }
            for(NSUInteger i = 0; i < n - 1;  i++) {
                NSString *emptyParameter = [emptyParameters objectAtIndex:i];
                url = [NSString stringWithFormat:@"%@%@&", url, emptyParameter];
            }
            NSString *emptyParameter = [emptyParameters objectAtIndex:n - 1];
            url = [NSString stringWithFormat:@"%@%@", url, emptyParameter];
        }
    }
    return url;
}

- (NSURL *) getURL {
    NSString *formattedURL = [self getFormattedURL];
    NSLog(@"url: %@", formattedURL);
    NSURL *url = [NSURL URLWithString:formattedURL];
    return url;
}


@end
