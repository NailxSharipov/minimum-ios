//
//  UIColor+Helper.m
//  Sbis
//
//  Created by Developer on 25.03.15.
//  Copyright (c) 2015 Tensor. All rights reserved.
//

#import "UIColor+Utils.h"

@implementation UIColor (Utils)

+ (UIColor *)colorWithNormalRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue {
    return [UIColor colorWithNormalRed:red green:green blue:blue alpha:1.0f];
}

+ (UIColor *)colorWithNormalRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha {
    float r = red / 255.0f;
    float g = green / 255.0f;
    float b = blue / 255.0f;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:alpha];
}

@end
