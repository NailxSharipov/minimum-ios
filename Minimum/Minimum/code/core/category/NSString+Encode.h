//
// Created by Nail Sharipov on 08/04/15.
// Copyright (c) 2015 Tensor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Encode)

- (NSString *)encodeString;

- (NSString *)decodeString;

- (BOOL)isHtml;

@end