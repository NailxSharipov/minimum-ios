////////////////////////////////////////////////////////////////////////////////
//
//  NSString+Url.h
//  core
//
//  Created by Nail Sharipov on 08/04/15.
//  Copyright (c) 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "NSString+Encode.h"


@implementation NSString (Encode)

static NSMutableCharacterSet *SDDecodeCharacterSet;

+ (void) initialize {
    SDDecodeCharacterSet = [[NSMutableCharacterSet alloc] init];
    [SDDecodeCharacterSet formUnionWithCharacterSet:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [SDDecodeCharacterSet removeCharactersInString:@"="];
    [SDDecodeCharacterSet removeCharactersInString:@"&"];
    [SDDecodeCharacterSet removeCharactersInString:@"+"];
    [SDDecodeCharacterSet removeCharactersInString:@"?"];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Interface Methods
//-------------------------------------------------------------------------------------------

- (NSString *)encodeString {
    NSString *encodedString = [self stringByAddingPercentEncodingWithAllowedCharacters:SDDecodeCharacterSet];
    return encodedString;
}

- (NSString *)decodeString {
    return [self stringByRemovingPercentEncoding];
}

- (BOOL)isHtml {
    NSUInteger position = [[self lowercaseString] rangeOfString:@"<html>"].location;
    return position != NSNotFound;
}

@end