////////////////////////////////////////////////////////////////////////////////
//
//  TXHistoryViewController.h
//  Minimum
//
//  Created by Nail Sharipov on 17/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXLoadHistoryTask;
@class TXDeleteHistoryTask;
@class TXQueryHistoryTask;
@class TXCurrentOrder;
@class TXMainViewController;

@protocol TXTaskExecutor;

@interface TXHistoryViewController : UIViewController

@property (strong, nonatomic) UIViewController *profile;

@property (strong, nonatomic) TXMainViewController *main;
@property (strong, nonatomic) id<TXTaskExecutor> taskExecutor;
@property (strong, nonatomic) TXLoadHistoryTask *loadHistoryTask;
@property (strong, nonatomic) TXDeleteHistoryTask *deleteHistoryTask;
@property (strong, nonatomic) TXQueryHistoryTask *queryHistoryTask;
@property (strong, nonatomic) TXCurrentOrder *currentOrder;

@end
