////////////////////////////////////////////////////////////////////////////////
//
//  TXHistoryViewController.m
//  Minimum
//
//  Created by Nail Sharipov on 17/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////


#import "TXHistoryViewController.h"
#import "TXMainViewController.h"
#import "TXTaskExecutor.h"
#import "TXCurrentOrder.h"
#import "TXLoadHistoryTask.h"
#import "TXDeleteHistoryTask.h"
#import "TXQueryHistoryTask.h"
#import "TXFastHistoryTableViewSource.h"
#import "TXColorPallete.h"
#import "TXOrder.h"

@interface TXHistoryViewController () <TXLoadHistoryDelegate, TXDeleteHistoryDelegate, TXQueryHistoryTaskDelegate, TXFastHistoryTableViewSourceDelegate>
@property (weak, nonatomic) IBOutlet UINavigationItem *tabbarTitle;

@property (weak, nonatomic) IBOutlet UITableView *fastHistoryTable;

@end

@implementation TXHistoryViewController {
    TXFastHistoryTableViewSource *_fastHistoryTableSource;
    UIRefreshControl *_refreshControl;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Override Methods
//-------------------------------------------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [TXColorPallete orangeColor];
    _tabbarTitle.title = NSLocalizedString(@"Repeat order", nil);
    [self prepareFastHistoryTable];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _queryHistoryTask.size = 20;
    _queryHistoryTask.index = 0;
    _queryHistoryTask.dateTimeStamp = 0;
    _queryHistoryTask.delegate = self;
    [_taskExecutor executeTask:_queryHistoryTask];
    [self stopLoad];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXFastHistoryTableViewSourceDelegate>
//-------------------------------------------------------------------------------------------

- (void)onSelectOrderItem:(TXOrder *)order {
    [_currentOrder setWithOrder:order];
    [self dismissViewControllerAnimated:YES completion:^(){
        [_main toggleMenu];
    }];
}
- (void)onLastItemShowed:(NSUInteger)index {
    // NOT READY!!!!
    /*
    _loadHistoryTask.delegate = self;
    _loadHistoryTask.index = index + 1;
    _loadHistoryTask.size = 20;
    [_taskExecutor executeTask:_loadHistoryTask];
    */
}

- (void)onDeleteItem:(TXOrder *)order {
    _deleteHistoryTask.delegate = self;
    _deleteHistoryTask.order = order;
    [_taskExecutor executeTask:_deleteHistoryTask];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXDeleteHistoryDelegate>
//-------------------------------------------------------------------------------------------

- (void)onDeleteHistoryTaskSuccess:(TXOrder *)order {
    [_fastHistoryTableSource deleteItem:order];
}

- (void)onDeleteHistoryTaskError:(NSString *)error {

}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXLoadHistoryDelegate>
//-------------------------------------------------------------------------------------------

- (void)onQueryHistoryTaskSuccess:(NSArray<TXOrder *> *)history {
    _fastHistoryTableSource.itemList = [[NSMutableArray alloc] initWithArray:history];
    [_fastHistoryTable reloadData];
}

- (void)onQueryHistoryTaskError:(NSString *)error {

}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXLoadHistoryDelegate>
//-------------------------------------------------------------------------------------------

- (void)onLoadHistoryTaskSuccess:(NSUInteger)lastDateTimeStamp {
    if (lastDateTimeStamp > 0) {
        _queryHistoryTask.dateTimeStamp = lastDateTimeStamp;
        _queryHistoryTask.delegate = self;
        [_taskExecutor executeTask:_queryHistoryTask];
        [self stopLoad];
    }
}

- (void)onLoadHistoryTaskError:(NSString *)error {
    [self stopLoad];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (void)startLoad {
    _loadHistoryTask.delegate = self;
    _loadHistoryTask.index = 0;
    _loadHistoryTask.size = 20;
    [_taskExecutor executeTask:_loadHistoryTask];
}

- (void)stopLoad {
    if(_refreshControl.isRefreshing) {
        CGPoint offset = _fastHistoryTable.contentOffset;
        [_refreshControl endRefreshing];
        _fastHistoryTable.contentOffset = offset;
    }
}

- (void)prepareFastHistoryTable {
    _fastHistoryTableSource = [[TXFastHistoryTableViewSource alloc] init];
    _fastHistoryTableSource.isDark = NO;
    _fastHistoryTableSource.isEditable = YES;
    _fastHistoryTableSource.tableView = _fastHistoryTable;
    _fastHistoryTableSource.delegate = self;
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self
                        action:@selector(startLoad)
              forControlEvents:UIControlEventValueChanged];
    [_fastHistoryTable addSubview:_refreshControl];
    
}

@end
