////////////////////////////////////////////////////////////////////////////////
//
//  TXFastHistoryTableViewSource.m
//  Minimum
//
//  Created by Nail Sharipov on 16/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXFastHistoryTableViewSource.h"
#import "TXFastHistoryTableViewCell.h"

@interface TXFastHistoryTableViewSource () <UITableViewDelegate, UITableViewDataSource>


@end

@implementation TXFastHistoryTableViewSource

//-------------------------------------------------------------------------------------------
#pragma mark - Public Methods
//-------------------------------------------------------------------------------------------

- (void)setTableView:(UITableView *)tableView {
    [tableView registerNib:[TXFastHistoryTableViewCell cellNib] forCellReuseIdentifier:[TXFastHistoryTableViewCell cellIdentifier]];
    tableView.delegate = self;
    tableView.dataSource = self;
    _tableView = tableView;
}

- (void)deleteItem:(TXOrder *)order {
    NSUInteger index = 0;
    for (TXOrder *item in _itemList) {
        if (order == item) {
            NSArray *deleteIndexPaths = @[[NSIndexPath indexPathForRow:index inSection:0]];
            [_tableView beginUpdates];
            [_itemList removeObjectAtIndex:index];
            [_tableView deleteRowsAtIndexPaths:deleteIndexPaths withRowAnimation:UITableViewRowAnimationFade];
            [_tableView endUpdates];
            break;
        }
        index++;
    }
}


//-------------------------------------------------------------------------------------------
#pragma mark - <UITableViewDataSource>
//-------------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_itemList != nil) {
        return _itemList.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TXFastHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[TXFastHistoryTableViewCell cellIdentifier]];
    cell.isDark = _isDark;
    return cell;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <UITableViewDelegate>
//-------------------------------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView willDisplayCell:(TXFastHistoryTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ix = (NSUInteger) indexPath.row;
    cell.item = _itemList[ix];
    if (ix == _itemList.count - 1 && _delegate != nil) {
        [_delegate onLastItemShowed: ix];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ix = (NSUInteger) indexPath.row;
    TXOrder *order = _itemList[ix];
    CGFloat cellHeight = [TXFastHistoryTableViewCell cellHeight:order];
    return cellHeight;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return _isEditable;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath  {
    return NSLocalizedString(@"Delete", nil);;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSUInteger ix = (NSUInteger) indexPath.row;
        TXOrder *order = _itemList[ix];
        [_delegate onDeleteItem:order];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ix = (NSUInteger) indexPath.row;
    TXOrder *order = _itemList[ix];
    [_delegate onSelectOrderItem:order];
}

@end
