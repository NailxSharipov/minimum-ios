////////////////////////////////////////////////////////////////////////////////
//
//  TXFastHistoryTableViewCell.m
//  Minimum
//
//  Created by Nail Sharipov on 16/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXFastHistoryTableViewCell.h"
#import "TXPlaceTableViewSource.h"
#import "TXPlaceTableViewCell.h"

@interface TXFastHistoryTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UITableView *placeTable;
@property (weak, nonatomic) IBOutlet UIImageView *ico;


@end

@implementation TXFastHistoryTableViewCell {
    TXPlaceTableViewSource *_tableSource;
    TXOrder *_order;
}


//-------------------------------------------------------------------------------------------
#pragma mark - Class Methods
//-------------------------------------------------------------------------------------------

+ (CGFloat)cellHeight:(TXOrder *)order {
    return 16.0f + 16.0f * order.placeList.count + 4.0f;
}

+ (NSString *)cellIdentifier {
    return @"TXFastHistoryTableViewCell";
}

+ (UINib *)cellNib {
    return [UINib nibWithNibName:@"TXFastHistoryTableViewCell" bundle:nil];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Overridden Methods
//-------------------------------------------------------------------------------------------

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    if (highlighted) {
        self.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.1f];
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Public Methods
//-------------------------------------------------------------------------------------------

- (void)setItem:(TXOrder *)item {
    if (_order != item) {
        _date.text = item.textDate;
        if (_tableSource == nil) {
            _tableSource = [[TXPlaceTableViewSource alloc] init];
            _tableSource.isDark = _isDark;
            _tableSource.tableView = _placeTable;
            _placeTable.rowHeight = [TXPlaceTableViewCell cellHeight];
            self.backgroundColor = [UIColor clearColor];
            _ico.image = [_ico.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            if (_isDark) {
                _ico.tintColor = [UIColor whiteColor];
            } else {
                _ico.tintColor = [UIColor blackColor];
            }
        }
        _tableSource.itemList = item.placeList;
        [_placeTable reloadData];
        _order = item;
    }
}

@end
