////////////////////////////////////////////////////////////////////////////////
//
//  TXFastHistoryTableViewCell.h
//  Minimum
//
//  Created by Nail Sharipov on 16/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

#import "TXOrder.h"

@interface TXFastHistoryTableViewCell : UITableViewCell

+ (CGFloat)cellHeight:(TXOrder *)order;

+ (NSString *)cellIdentifier;

+ (UINib *)cellNib;

@property (weak, nonatomic) TXOrder *item;
@property (nonatomic) BOOL isDark;

@end
