////////////////////////////////////////////////////////////////////////////////
//
//  TXFastHistoryTableViewSource.h
//  Minimum
//
//  Created by Nail Sharipov on 16/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXOrder;

@protocol TXFastHistoryTableViewSourceDelegate <NSObject>

@required

- (void)onSelectOrderItem:(TXOrder *)order;
- (void)onLastItemShowed:(NSUInteger)index;

@optional

- (void)onDeleteItem:(TXOrder *)order;


@end

@interface TXFastHistoryTableViewSource : NSObject

@property (weak, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray<TXOrder *> *itemList;
@property (nonatomic) BOOL isDark;
@property (nonatomic) BOOL isEditable;
@property (weak, nonatomic) id<TXFastHistoryTableViewSourceDelegate> delegate;

- (void)deleteItem:(TXOrder *)order;

@end
