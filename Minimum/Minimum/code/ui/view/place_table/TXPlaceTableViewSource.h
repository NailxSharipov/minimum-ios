////////////////////////////////////////////////////////////////////////////////
//
//  TXPlaceTableViewSource.h
//  Minimum
//
//  Created by Nail Sharipov on 16/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXPlace;

@interface TXPlaceTableViewSource : NSObject

@property (weak, nonatomic)UITableView *tableView;
@property (strong, nonatomic)NSArray<TXPlace *> *itemList;
@property (strong, nonatomic)NSArray<NSString *> *stringList;
@property (nonatomic) BOOL isDark;


@end
