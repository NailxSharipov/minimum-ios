////////////////////////////////////////////////////////////////////////////////
//
//  TXPlaceTableViewCell.m
//  Minimum
//
//  Created by Nail Sharipov on 16/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXPlaceTableViewCell.h"
#import "TXPlace.h"

@interface TXPlaceTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *address;

@end

@implementation TXPlaceTableViewCell {
    BOOL _isPrepared;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Class Methods
//-------------------------------------------------------------------------------------------

+ (CGFloat)cellHeight {
    return 16.0f;
}

+ (NSString *)cellIdentifier {
    return @"TXPlaceTableViewCell";
}

+ (UINib *)cellNib {
    return [UINib nibWithNibName:@"TXPlaceTableViewCell" bundle:nil];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Public Methods
//-------------------------------------------------------------------------------------------

- (void)setItem:(TXPlace *)item {
    if (!_isPrepared) {
        self.backgroundView = [UIView new];
        self.backgroundColor = [UIColor clearColor];
        self.selectedBackgroundView = [UIView new];
        _isPrepared = YES;
        if (_isDark) {
            _address.textColor = [UIColor whiteColor];
        } else {
            _address.textColor = [UIColor blackColor];
        }
    }
    _address.text = item.address;
}

- (void)setString:(NSString *)string {
    if (!_isPrepared) {
        self.backgroundView = [UIView new];
        self.backgroundColor = [UIColor clearColor];
        self.selectedBackgroundView = [UIView new];
        _isPrepared = YES;
        _address.textColor = [UIColor darkGrayColor];
    }
    _address.text = string;
}

@end