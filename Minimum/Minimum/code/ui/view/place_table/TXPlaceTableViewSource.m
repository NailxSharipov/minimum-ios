////////////////////////////////////////////////////////////////////////////////
//
//  TXPlaceTableViewSource.m
//  Minimum
//
//  Created by Nail Sharipov on 16/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXPlaceTableViewSource.h"
#import "TXPlaceTableViewCell.h"

@interface TXPlaceTableViewSource () <UITableViewDelegate, UITableViewDataSource>


@end

@implementation TXPlaceTableViewSource

//-------------------------------------------------------------------------------------------
#pragma mark - Public Methods
//-------------------------------------------------------------------------------------------

- (void)setTableView:(UITableView *)tableView {
    [tableView registerNib:[TXPlaceTableViewCell cellNib] forCellReuseIdentifier:[TXPlaceTableViewCell cellIdentifier]];
    tableView.delegate = self;
    tableView.dataSource = self;
    _tableView = tableView;
}


//-------------------------------------------------------------------------------------------
#pragma mark - <UITableViewDataSource>
//-------------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_itemList != nil) {
        return _itemList.count;
    } else if (_stringList != nil) {
        return _stringList.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TXPlaceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[TXPlaceTableViewCell cellIdentifier]];
    cell.isDark = _isDark;
    return cell;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <UITableViewDelegate>
//-------------------------------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView willDisplayCell:(TXPlaceTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ix = (NSUInteger) indexPath.row;
    if (_itemList != nil) {
        cell.item = _itemList[ix];
    } else if (_stringList != nil) {
        cell.string = _stringList[ix];
    }
}


@end