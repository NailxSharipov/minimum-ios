////////////////////////////////////////////////////////////////////////////////
//
//  TXPlaceTableViewCell.h
//  Minimum
//
//  Created by Nail Sharipov on 16/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXPlace;

@interface TXPlaceTableViewCell : UITableViewCell

+ (CGFloat)cellHeight;

+ (NSString *)cellIdentifier;

+ (UINib *)cellNib;

@property (weak, nonatomic) TXPlace *item;
@property (weak, nonatomic) NSString *string;
@property (nonatomic) BOOL isDark;


@end
