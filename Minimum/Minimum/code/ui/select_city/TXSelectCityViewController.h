////////////////////////////////////////////////////////////////////////////////
//
//  TXSelectCityViewController.h
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>


@class TXLoadCityTask;

@class TXInitUserWorkspaceTask;
@protocol TXTaskExecutor;
@protocol TXCityDAO;
@protocol TXRouter;


@interface TXSelectCityViewController : UIViewController

@property (strong, nonatomic) id<TXTaskExecutor> taskExecutor;
@property (strong, nonatomic) TXLoadCityTask *loadCityTask;
@property (strong, nonatomic) id<TXCityDAO> cityDAO;
@property (strong, nonatomic) id<TXRouter> routeToSelectCity;
@property (strong, nonatomic) TXInitUserWorkspaceTask *prepareUserWorkspaceTask;

@end
