////////////////////////////////////////////////////////////////////////////////
//
//  TXSelectCityTableViewSource.h
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXCity;

@protocol TXSelectCityTableViewSourceDelegate <NSObject>

@required

- (void)onSelectCity:(TXCity *) city;

@end

@interface TXSelectCityTableViewSource : NSObject

@property (strong, nonatomic)NSArray<TXCity *> *itemList;
@property (strong, nonatomic)TXCity *actualCity;
@property (weak, nonatomic)id<TXSelectCityTableViewSourceDelegate> delegate;

- (void)registerTableView:(UITableView *)tableView;


@end
