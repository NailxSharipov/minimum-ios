////////////////////////////////////////////////////////////////////////////////
//
//  TXSelectCityTableViewCell.m
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXSelectCityTableViewCell.h"

#import "TXCity.h"
#import "TXColorPallete.h"

@interface TXSelectCityTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *title;

@end

@implementation TXSelectCityTableViewCell

//-------------------------------------------------------------------------------------------
#pragma mark - Class Methods
//-------------------------------------------------------------------------------------------

+ (CGFloat)cellHeight {
    return 44.0f;
}

+ (NSString *)cellIdentifier {
    return @"TXSelectCityTableViewCell";
}

+ (UINib *)cellNib {
    return [UINib nibWithNibName:@"TXSelectCityTableViewCell" bundle:nil];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Overridden Methods
//-------------------------------------------------------------------------------------------

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Public Methods
//-------------------------------------------------------------------------------------------

- (void)setItem:(TXCity *)item isActual:(BOOL)actual {
    _title.text = item.title;
    if (actual) {
        _title.textColor = [TXColorPallete orangeColor];
    } else {
        _title.textColor = [UIColor darkGrayColor];
    }
}

@end
