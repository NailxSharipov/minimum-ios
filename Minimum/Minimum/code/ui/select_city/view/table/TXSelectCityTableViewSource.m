////////////////////////////////////////////////////////////////////////////////
//
//  TXSelectCityTableViewSource.m
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXSelectCityTableViewSource.h"
#import "TXSelectCityTableViewCell.h"
#import "TXCity.h"


@interface TXSelectCityTableViewSource () <UITableViewDelegate, UITableViewDataSource>


@end


@implementation TXSelectCityTableViewSource

- (void)registerTableView:(UITableView *)tableView {
    [tableView registerNib:[TXSelectCityTableViewCell cellNib] forCellReuseIdentifier:[TXSelectCityTableViewCell cellIdentifier]];
    tableView.delegate = self;
    tableView.dataSource = self;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <UITableViewDataSource>
//-------------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_itemList != nil) {
        return _itemList.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [tableView dequeueReusableCellWithIdentifier:[TXSelectCityTableViewCell cellIdentifier]];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <UITableViewDelegate>
//-------------------------------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView willDisplayCell:(TXSelectCityTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ix = (NSUInteger) indexPath.row;
    TXCity *city = _itemList[ix];
    [cell setItem:city isActual:[city.cityId isEqual: _actualCity.cityId]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ix = (NSUInteger) indexPath.row;
    TXCity *city = _itemList[ix];
    [_delegate onSelectCity:city];
}


@end
