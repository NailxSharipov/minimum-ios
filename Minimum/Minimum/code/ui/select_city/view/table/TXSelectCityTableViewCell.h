////////////////////////////////////////////////////////////////////////////////
//
//  TXSelectCityTableViewCell.h
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXCity;

@interface TXSelectCityTableViewCell : UITableViewCell

+ (CGFloat)cellHeight;

+ (NSString *)cellIdentifier;

+ (UINib *)cellNib;

- (void)setItem:(TXCity *)item isActual:(BOOL)actual;

@end
