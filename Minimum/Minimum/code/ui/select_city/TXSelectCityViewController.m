////////////////////////////////////////////////////////////////////////////////
//
//  TXSelectCityViewController.m
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXSelectCityViewController.h"

#import "TXLoadCityTask.h"
#import "TXInitUserWorkspaceTask.h"
#import "TXSelectCityTableViewSource.h"
#import "TXTaskExecutor.h"
#import "TXCityDAO.h"
#import "TXColorPallete.h"
#import "TXRouter.h"


@interface TXSelectCityViewController () <TXLoadCityTaskDelegate, TXSelectCityTableViewSourceDelegate, TXInitUserWorkspaceTaskDelegate>

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UINavigationItem *navBarTitle;


@end

@implementation TXSelectCityViewController {
    TXSelectCityTableViewSource *_tableSource;
    UIRefreshControl *_refreshControl;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXSelectCityTableViewSourceDelegate>
//-------------------------------------------------------------------------------------------

- (void)onInitUserWorkspaceTaskSuccess {
    [self close];
}

- (void)onInitUserWorkspaceTaskError:(NSString *)error {
    [self close];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXLoadCityTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onLoadCityTaskSuccess {
    _tableSource.itemList = _cityDAO.cityData;
    [_table reloadData];
    [self stopLoad];
}

- (void)onLoadCityTaskError:(NSString *)error {
    [self stopLoad];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXSelectCityTableViewSourceDelegate>
//-------------------------------------------------------------------------------------------

- (void)onSelectCity:(TXCity *) city {
    _tableSource.actualCity = city;
    [_table reloadData];
    _cityDAO.actualCity = city;
    _prepareUserWorkspaceTask.delegate = self;
    [_taskExecutor executeTask:_prepareUserWorkspaceTask];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Overridden Methods
//-------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    _loadCityTask.delegate = self;
    _navBarTitle.title = NSLocalizedString(@"Select a city", nil);
    self.view.backgroundColor = [TXColorPallete orangeColor];
    [self prepareTable];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _tableSource.actualCity = _cityDAO.actualCity;
    [self stopLoad];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self close];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (void)startLoad {
    [_taskExecutor executeTask:_loadCityTask];
}

- (void)stopLoad {
    if(_refreshControl.isRefreshing) {
        CGPoint offset = _table.contentOffset;
        [_refreshControl endRefreshing];
        _table.contentOffset = offset;
    }
}

- (void)prepareTable {
    _tableSource = [[TXSelectCityTableViewSource alloc] init];
    _tableSource.delegate = self;

    _tableSource.itemList = _cityDAO.cityData;
    [_tableSource registerTableView:_table];
    
    _table.rowHeight =  UITableViewAutomaticDimension;
    
    [_table reloadData];
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self
                        action:@selector(startLoad)
              forControlEvents:UIControlEventValueChanged];
    [_table addSubview:_refreshControl];
}

- (void)close {
    if (self.navigationController != nil) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
