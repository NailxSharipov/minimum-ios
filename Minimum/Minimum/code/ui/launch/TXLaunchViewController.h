////////////////////////////////////////////////////////////////////////////////
//
//  TXLaunchViewController.h
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXLoadCityTask;
@class TXLoginTask;
@class TXInitUserWorkspaceTask;
@protocol TXUserDAO;
@protocol TXTaskExecutor;
@protocol TXRouter;


@interface TXLaunchViewController : UIViewController

@property (strong, nonatomic) id<TXTaskExecutor> taskExecutor;
@property (strong, nonatomic) TXLoadCityTask *loadCityTask;
@property (strong, nonatomic) TXLoginTask *loginTask;
@property (strong, nonatomic) TXInitUserWorkspaceTask *prepareUserWorkspaceTask;
@property (strong, nonatomic) id<TXUserDAO> userDAO;
@property (strong, nonatomic) id<TXRouter> routeToLogin;
@property (strong, nonatomic) id<TXRouter> routeToMain;

@end
