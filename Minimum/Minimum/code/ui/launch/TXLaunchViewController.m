////////////////////////////////////////////////////////////////////////////////
//
//  TXLaunchViewController.m
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXLaunchViewController.h"

#import "TXLoadCityTask.h"
#import "TXLoginTask.h"
#import "TXInitUserWorkspaceTask.h"
#import "TXTaskExecutor.h"
#import "TXRouter.h"
#import "TXColorPallete.h"
#import "TXUserDAO.h"

@interface TXLaunchViewController () <TXLoadCityTaskDelegate, TXLoginTaskDelegate, TXInitUserWorkspaceTaskDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *repeatButton;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@end

@implementation TXLaunchViewController

//-------------------------------------------------------------------------------------------
#pragma mark - <TXLoadCityTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onLoadCityTaskSuccess {
    TXUser *user = _userDAO.actualUser;
    NSString *phone = user.phone;
    NSString *password = [_userDAO getPassword];
    if (user != nil && phone != nil && password != nil) {
        _loginTask.delegate = self;
        _loginTask.phone = phone;
        _loginTask.password = password;
        [_taskExecutor executeTask:_loginTask];
    } else {
        [_routeToLogin routeFrom:self];    
    }
}

- (void)onLoadCityTaskError:(NSString *)error {
    _errorLabel.text = error;
    [self stopLoad];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXLoginTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onLoginTaskSuccess {
    _prepareUserWorkspaceTask.delegate = self;
    [_taskExecutor executeTask:_prepareUserWorkspaceTask];
}

- (void)onLoginTaskError:(NSString *)error {
    [_routeToLogin routeFrom:self];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXInitUserWorkspaceTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onInitUserWorkspaceTaskSuccess {
    [_routeToMain routeFrom:self];
}

- (void)onInitUserWorkspaceTaskError:(NSString *)error {
    [_routeToLogin routeFrom:self];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Overridden Methods
//-------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [TXColorPallete orangeColor];
    [_repeatButton setTitle:NSLocalizedString(@"Repeat", nil) forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _loadCityTask.delegate = self;
    [_taskExecutor executeTask:_loadCityTask];
    [self startLoad];
}

- (IBAction)repeatButtonPressed:(id)sender {
    _loadCityTask.delegate = self;
    [_taskExecutor executeTask:_loadCityTask];
    [self startLoad];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (void)startLoad {
    _repeatButton.hidden = YES;
    _errorLabel.hidden = YES;
    _activityIndicator.hidden = NO;
}

- (void)stopLoad {
    _repeatButton.hidden = NO;
    _errorLabel.hidden = NO;
    _activityIndicator.hidden = YES;
}

@end
