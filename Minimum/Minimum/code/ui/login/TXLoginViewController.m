////////////////////////////////////////////////////////////////////////////////
//
//  TXLoginViewController.m
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXLoginViewController.h"

#import "TXLoginTask.h"
#import "TXInitUserWorkspaceTask.h"
#import "TXSelectCityViewController.h"
#import "TXTaskExecutor.h"
#import "TXUserDAO.h"
#import "TXCityDAO.h"
#import "TXPhoneNumberField.h"
#import "TXRouter.h"
#import "TXColorPallete.h"
#import "TXRemindPasswordTask.h"

@interface TXLoginViewController () <TXLoginTaskDelegate, TXInitUserWorkspaceTaskDelegate, TXRemindPasswordTaskDelegate>

@property (weak, nonatomic) IBOutlet TXPhoneNumberField *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *smsButton;
@property (weak, nonatomic) IBOutlet UIButton *cityButton;


@end

@implementation TXLoginViewController

//-------------------------------------------------------------------------------------------
#pragma mark - <TXRemindPasswordTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onRemindPasswordTaskError:(NSString *)error {
    if (error != nil) {
        [self showAlert:error];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXInitUserWorkspaceTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onInitUserWorkspaceTaskSuccess {
    [_routeToMain routeFrom:self.navigationController];
}

- (void)onInitUserWorkspaceTaskError:(NSString *)error {

}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXLoginTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onLoginTaskSuccess {
    _prepareUserWorkspaceTask.delegate = self;
    [_taskExecutor executeTask:_prepareUserWorkspaceTask];
}

- (void)onLoginTaskError:(NSString *)error {
    if (error != nil) {
        [self showAlert:error];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Overridden Methods
//-------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [TXColorPallete orangeColor];
    _loginTask.delegate = self;
    _phoneLabel.text = NSLocalizedString(@"Phone", nil);
    _passwordLabel.text = NSLocalizedString(@"Password", nil);
    _cityLabel.text = NSLocalizedString(@"City", nil);
    [_loginButton setTitle:NSLocalizedString(@"Enter", nil) forState:UIControlStateNormal];
    [_smsButton setTitle:NSLocalizedString(@"Get password", nil) forState:UIControlStateNormal];
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap)];
    [self.view addGestureRecognizer:singleFingerTap];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    TXUser *user = _userDAO.actualUser;
    if (user != nil) {
        _passwordField.text = [_userDAO getPassword];
    }
    TXCity *city = _cityDAO.actualCity;
    if (city != nil) {
        [_cityButton setTitle:city.title forState:UIControlStateNormal];
    } else {
        [_cityButton setTitle:NSLocalizedString(@"Select a city", nil) forState:UIControlStateNormal];
    }
    if (city != nil && user != nil) {
        _phoneField.prefix = city.phonePrefix;
        _phoneField.unformatText = user.phone;
    }
}

- (IBAction)loginButtonPressed:(id)sender {
    NSString *phone = _phoneField.unformatText;
    NSString *password = _passwordField.text;
    
    TXCity *city = _cityDAO.actualCity;
    if (city == nil) {
        [self showAlert:NSLocalizedString(@"Select a city", nil)];
        return;
    }
    if (phone == nil || phone.length != 10) {
        [self showAlert:NSLocalizedString(@"Phone number is incorrect", nil)];
        return;
    }

    if (password == nil || password.length == 0) {
        [self showAlert:NSLocalizedString(@"Password is not set", nil)];
        return;
    }
    
    _loginTask.phone = phone;
    _loginTask.password = password;
    
    [_taskExecutor executeTask: _loginTask];
}

- (IBAction)cityButtonPressed:(id)sender {
    [self.navigationController pushViewController:_selectCity animated:YES];
}

- (IBAction)smsButtonPressed:(id)sender {
    NSString *phone = _phoneField.unformatText;
    if (phone != nil && phone.length == 10) {
        _remidPasswordTask.phone = phone;
        _remidPasswordTask.delegate = self;
        [_taskExecutor executeTask: _remidPasswordTask];
    } else {
        [self showAlert:NSLocalizedString(@"Phone number is incorrect", nil)];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (void)showAlert:(NSString *)alert {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:alert
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)singleTap {
    [_phoneField resignFirstResponder];
    [_passwordField resignFirstResponder];
}

@end
