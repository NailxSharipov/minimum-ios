////////////////////////////////////////////////////////////////////////////////
//
//  TXLoginNavigationController.h
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>


@class TXLoginViewController;


@interface TXLoginNavigationController : UINavigationController

@property (strong, nonatomic) TXLoginViewController *login;

@end
