////////////////////////////////////////////////////////////////////////////////
//
//  TXLoginViewController.h
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXLoginTask;
@class TXInitUserWorkspaceTask;
@class TXRemindPasswordTask;
@class TXSelectCityViewController;

@protocol TXTaskExecutor;
@protocol TXUserDAO;
@protocol TXCityDAO;
@protocol TXRouter;


@interface TXLoginViewController : UIViewController

@property (strong, nonatomic) id<TXCityDAO> cityDAO;
@property (strong, nonatomic) id<TXUserDAO> userDAO;
@property (strong, nonatomic) id<TXTaskExecutor> taskExecutor;
@property (strong, nonatomic) TXLoginTask *loginTask;
@property (strong, nonatomic) TXRemindPasswordTask *remidPasswordTask;
@property (strong, nonatomic) TXSelectCityViewController *selectCity;
@property (strong, nonatomic) id<TXRouter> routeToMain;
@property (strong, nonatomic) TXInitUserWorkspaceTask *prepareUserWorkspaceTask;

@end
