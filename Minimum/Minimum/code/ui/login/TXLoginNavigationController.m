////////////////////////////////////////////////////////////////////////////////
//
//  TXLoginNavigationController.m
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXLoginNavigationController.h"

#import "TXLoginViewController.h"

@interface TXLoginNavigationController ()

@end

@implementation TXLoginNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBarHidden:YES animated:NO];
    [self pushViewController:_login animated:NO];
}

@end
