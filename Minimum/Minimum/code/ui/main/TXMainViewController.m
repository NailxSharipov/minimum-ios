////////////////////////////////////////////////////////////////////////////////
//
//  TXMainViewController.m
//  Minimum
//
//  Created by Nail Sharipov on 08/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXMainViewController.h"
#import "TXColorPallete.h"
#import "TXRouter.h"


@interface TXMainViewController () <SWRevealViewControllerDelegate>


@end

@implementation TXMainViewController

//-------------------------------------------------------------------------------------------
#pragma mark - Initialization & Destruction
//-------------------------------------------------------------------------------------------

- (instancetype)initWithRear:(UIViewController *)rear front:(UIViewController *)front {
    self = [super initWithRearViewController:rear frontViewController:front];
    if (self) {
        self.delegate = self;
    }
    return self;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Overridden Methods
//-------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setFrontViewPosition: FrontViewPositionLeft animated: YES];
    self.view.backgroundColor = [TXColorPallete orangeColor];
    [self panGestureRecognizer];
    [self tapGestureRecognizer];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Public Methods
//-------------------------------------------------------------------------------------------

- (void) toggleMenu {
    [self revealToggleAnimated: YES];
}

@end