////////////////////////////////////////////////////////////////////////////////
//
//  TXMainViewController.h
//  Minimum
//
//  Created by Nail Sharipov on 08/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import <SWRevealViewController.h>
#import "TXPassable.h"


@protocol TXRouter;


@interface TXMainViewController : SWRevealViewController<TXPassable>

@property (nonatomic, strong) NSDictionary *intent;

- (instancetype)initWithRear:(UIViewController *)rear front:(UIViewController *)front;

- (void)toggleMenu;

@end
