////////////////////////////////////////////////////////////////////////////////
//
//  TXCurrentOrderTableViewSource.m
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXCurrentOrderTableViewSource.h"
#import "TXCurrentOrderTableViewCell.h"
#import "TXDetailOrder.h"


@interface TXCurrentOrderTableViewSource () <UITableViewDelegate, UITableViewDataSource>


@end


@implementation TXCurrentOrderTableViewSource

- (void)setTableView:(UITableView *)tableView {
    [tableView registerNib:[TXCurrentOrderTableViewCell cellNib] forCellReuseIdentifier:[TXCurrentOrderTableViewCell cellIdentifier]];
    tableView.delegate = self;
    tableView.dataSource = self;
    _tableView = tableView;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <UITableViewDataSource>
//-------------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_itemList != nil) {
        return _itemList.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TXCurrentOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[TXCurrentOrderTableViewCell cellIdentifier]];
    cell.delegate = _delegate;
    return cell;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <UITableViewDelegate>
//-------------------------------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ix = (NSUInteger) indexPath.row;
    TXDetailOrder *detailOrder = _itemList[ix];
    NSUInteger count = 0;
    if (detailOrder != nil && detailOrder.placeList != nil) {
        count = detailOrder.placeList.count;
    }
    CGFloat cellHeight = [TXCurrentOrderTableViewCell cellHeight:count];
    return cellHeight;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(TXCurrentOrderTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ix = (NSUInteger) indexPath.row;
    cell.item = _itemList[ix];
}


@end
