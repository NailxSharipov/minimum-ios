////////////////////////////////////////////////////////////////////////////////
//
//  TXCurrentOrderTableViewSource.h
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXDetailOrder;

@protocol TXCurrentOrderTableViewSourceDelegate <NSObject>

- (void)onCancelPressed:(TXDetailOrder *)detailOrder;

@end

@interface TXCurrentOrderTableViewSource : NSObject

@property (weak, nonatomic)UITableView *tableView;
@property (strong, nonatomic)NSArray<TXDetailOrder *> *itemList;
@property (weak, nonatomic) id<TXCurrentOrderTableViewSourceDelegate> delegate;

@end
