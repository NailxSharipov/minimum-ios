////////////////////////////////////////////////////////////////////////////////
//
//  TXCurrentOrderTableViewCell.m
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXCurrentOrderTableViewCell.h"
#import "TXColorPallete.h"
#import "TXDetailOrder.h"
#import "TXPlaceTableViewSource.h"
#import "TXPlaceTableViewCell.h"
#import "TXCurrentOrderTableViewSource.h"

@interface TXCurrentOrderTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UILabel *car;
@property (weak, nonatomic) IBOutlet UILabel *carNumber;
@property (weak, nonatomic) IBOutlet UILabel *driver;
@property (weak, nonatomic) IBOutlet UILabel *driverPhone;
@property (weak, nonatomic) IBOutlet UILabel *priceDesription;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;

@end

@implementation TXCurrentOrderTableViewCell {
    TXPlaceTableViewSource *_tableSource;
    TXDetailOrder *_detailOrder;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Class Methods
//-------------------------------------------------------------------------------------------

+ (CGFloat)cellHeight:(NSUInteger)placeCount {
    return 24.0 * 4 + 20.0 + 20.0 + 48.0 + [TXPlaceTableViewCell cellHeight] * placeCount;
}

+ (NSString *)cellIdentifier {
    return @"TXCurrentOrderTableViewCell";
}

+ (UINib *)cellNib {
    return [UINib nibWithNibName:@"TXCurrentOrderTableViewCell" bundle:nil];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [_delegate onCancelPressed:_detailOrder];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Public Methods
//-------------------------------------------------------------------------------------------

- (void)setItem:(TXDetailOrder *)item {
    _detailOrder = item;
    self.backgroundColor = [TXColorPallete sheetColor];
    _status.text = item.status;
    _car.text = item.car;
    _carNumber.text = item.carNumber;
    _driverPhone.text = item.phone;
    _price.text = item.price;
    if (item.code >= 100) {
        _cancelButton.enabled = NO;
    } else {
        _cancelButton.enabled = YES;
    }
    _driver.text = NSLocalizedString(@"driver phone", nil);
    _priceDesription.text = NSLocalizedString(@"order price", nil);
    [_cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    
    if (_tableSource == nil) {
        _tableSource = [TXPlaceTableViewSource new];
        _tableSource.tableView = _table;
        _table.rowHeight = [TXPlaceTableViewCell cellHeight];
    }
    _tableHeight.constant = [TXPlaceTableViewCell cellHeight] * item.placeList.count;
    _tableSource.stringList = item.placeList;
    [_table reloadData];
}


@end
