////////////////////////////////////////////////////////////////////////////////
//
//  TXCurrentOrderTableViewCell.h
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXDetailOrder;
@protocol TXCurrentOrderTableViewSourceDelegate;

@interface TXCurrentOrderTableViewCell : UITableViewCell

+ (CGFloat)cellHeight:(NSUInteger)placeCount;

+ (NSString *)cellIdentifier;

+ (UINib *)cellNib;

- (void)setItem:(TXDetailOrder *)item;

@property (weak, nonatomic) id<TXCurrentOrderTableViewSourceDelegate> delegate;

@end
