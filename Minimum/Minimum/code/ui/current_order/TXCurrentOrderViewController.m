////////////////////////////////////////////////////////////////////////////////
//
//  TXCurrentOrderViewController.m
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXCurrentOrderViewController.h"
#import "TXTaskExecutor.h"
#import "TXColorPallete.h"
#import "TXCurrentOrderTableViewSource.h"
#import "TXCurrentOrderTableViewCell.h"

@interface TXCurrentOrderViewController () <TXQueryDetailOrderTaskDelegate, TXCurrentOrderTableViewSourceDelegate, TXCancelOrderTaskDelegate>

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UINavigationItem *tabbarTitle;

@end

@implementation TXCurrentOrderViewController {
    TXCurrentOrderTableViewSource *_tableSource;
    UIRefreshControl *_refreshControl;
    NSTimer *_timer;
}


//-------------------------------------------------------------------------------------------
#pragma mark - <TXCancelOrderTaskDelegate>
//-------------------------------------------------------------------------------------------
- (void)onCancelOrderTaskSuccess {
    [self updateDetailOrderList];
}

- (void)onCancelOrderTaskError:(NSString *)error {
    [self updateDetailOrderList];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXCurrentOrderTableViewSourceDelegate>
//-------------------------------------------------------------------------------------------

- (void)onCancelPressed:(TXDetailOrder *)detailOrder {
    _cancelOrderTask.delegate = self;
    _cancelOrderTask.detailOrder = detailOrder;
    [_taskExecutor executeTask:_cancelOrderTask];
}

//-------------------------------------------------------------------------------------------
#pragma mark - TXQueryDetailOrderTaskDelegate
//-------------------------------------------------------------------------------------------

- (void)onQueryDetailOrderTaskSuccess:(NSArray<TXDetailOrder *> *)detailOrderList {
    _tableSource.itemList = detailOrderList;
    [_table reloadData];
    [self stopLoad];
}

- (void)onQueryDetailOrderTaskError:(NSString *)error {


}

//-------------------------------------------------------------------------------------------
#pragma mark - Override Methods
//-------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [TXColorPallete orangeColor];
    _table.backgroundColor = [TXColorPallete sheetColor];
    _tabbarTitle.title = NSLocalizedString(@"Current order", nil);
    [self prepareTable];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateDetailOrderList];
    _timer = [NSTimer scheduledTimerWithTimeInterval:20.0
                                              target:self
                                            selector:@selector(updateDetailOrderList)
                                            userInfo:nil
                                             repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self stopLoad];
    if (_timer != nil) {
        [_timer invalidate];
    }
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (void)updateDetailOrderList {
    _queryDetailOrderTask.delegate = self;
    [_taskExecutor executeTask:_queryDetailOrderTask];
}
- (void)stopLoad {
    if(_refreshControl.isRefreshing) {
        CGPoint offset = _table.contentOffset;
        [_refreshControl endRefreshing];
        _table.contentOffset = offset;
    }
}

- (void)prepareTable {
    _tableSource = [TXCurrentOrderTableViewSource new];
    _tableSource.tableView = _table;
    _tableSource.delegate = self;
    _refreshControl = [UIRefreshControl new];
    [_refreshControl addTarget:self
                        action:@selector(updateDetailOrderList)
              forControlEvents:UIControlEventValueChanged];
    [_table addSubview:_refreshControl];
}

@end
