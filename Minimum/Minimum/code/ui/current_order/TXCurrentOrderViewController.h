////////////////////////////////////////////////////////////////////////////////
//
//  TXCurrentOrderViewController.h
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import "TXQueryDetailOrderTask.h"
#import "TXCancelOrderTask.h"

@protocol TXTaskExecutor;

@interface TXCurrentOrderViewController : UIViewController

@property (strong, nonatomic) TXQueryDetailOrderTask *queryDetailOrderTask;
@property (strong, nonatomic) TXCancelOrderTask *cancelOrderTask;
@property (strong, nonatomic) id<TXTaskExecutor> taskExecutor;

@end
