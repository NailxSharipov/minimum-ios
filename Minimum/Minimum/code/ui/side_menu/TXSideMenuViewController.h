////////////////////////////////////////////////////////////////////////////////
//
//  TXSideMenuViewController.h
//  Minimum
//
//  Created by Nail Sharipov on 08/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXLoadHistoryTask;
@class TXQueryHistoryTask;
@class TXCurrentOrder;
@class TXMainViewController;
@class TXMakeOrderViewController;

@protocol TXUserDAO;
@protocol TXCityDAO;
@protocol TXRouter;
@protocol TXTaskExecutor;


@interface TXSideMenuViewController : UIViewController

@property (strong, nonatomic) TXMainViewController *main;
@property (strong, nonatomic) UIViewController *history;
@property (strong, nonatomic) id<TXCityDAO> cityDAO;
@property (strong, nonatomic) id<TXUserDAO> userDAO;
@property (strong, nonatomic) id<TXRouter> routeExit;
@property (strong, nonatomic) id<TXRouter> routeToSelectCity;
@property (strong, nonatomic) UIViewController *profile;
@property (strong, nonatomic) UIViewController *currentOrderVC;
@property (strong, nonatomic) TXMakeOrderViewController *makeOrder;
@property (strong, nonatomic) TXCurrentOrder *currentOrder;

@property (strong, nonatomic) id<TXTaskExecutor> taskExecutor;
@property (strong, nonatomic) TXLoadHistoryTask *loadHistoryTask;
@property (strong, nonatomic) TXQueryHistoryTask *queryHistoryTask;

@end
