////////////////////////////////////////////////////////////////////////////////
//
//  TXSideMenuTableViewCell.h
//  Minimum
//
//  Created by Nail Sharipov on 10/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import "TXSideMenuCell.h"

@class TXSideMenuItem;

@interface TXSideMenuTableViewCell : UITableViewCell<TXSideMenuCell>

+ (CGFloat)cellHeight;

+ (NSString *)cellIdentifier;

+ (UINib *)cellNib;

@property (weak, nonatomic) TXSideMenuItem *item;

@end
