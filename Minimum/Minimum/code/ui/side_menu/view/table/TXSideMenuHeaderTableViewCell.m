////////////////////////////////////////////////////////////////////////////////
//
//  TXSideMenuHeaderTableViewCell.m
//  Minimum
//
//  Created by Nail Sharipov on 11/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXSideMenuHeaderTableViewCell.h"
#import "TXSideMenuItem.h"
#import "TXThinLineView.h"

@interface TXSideMenuHeaderTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *ico;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet TXThinLineView *separator;

@end

@implementation TXSideMenuHeaderTableViewCell

//-------------------------------------------------------------------------------------------
#pragma mark - Class Methods
//-------------------------------------------------------------------------------------------

+ (CGFloat)cellHeight {
    return 44.0f;
}

+ (NSString *)cellIdentifier {
    return @"TXSideMenuHeaderTableViewCell";
}

+ (UINib *)cellNib {
    return [UINib nibWithNibName:@"TXSideMenuHeaderTableViewCell" bundle:nil];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Overridden Methods
//-------------------------------------------------------------------------------------------

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Public Methods
//-------------------------------------------------------------------------------------------

- (void)setItem:(TXSideMenuItem *)item {
    _item = item;
    _ico.image = [item.ico imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ico.tintColor = item.icoColor;
    
    _title.text = item.title;
    _title.textColor = item.titleColor;
    
    self.backgroundColor = [UIColor clearColor];
    self.backgroundView = [UIView new];
    self.selectedBackgroundView = [UIView new];
    
    _separator.hidden = item.isSeparatorHiden;
}

@end
