////////////////////////////////////////////////////////////////////////////////
//
//  TXSideMenuCell.h
//  Minimum
//
//  Created by Nail Sharipov on 11/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

@class TXSideMenuItem;

@protocol TXSideMenuCell <NSObject>

@property (weak, nonatomic) TXSideMenuItem *item;

@end
