////////////////////////////////////////////////////////////////////////////////
//
//  TXSideMenuTableViewSource.m
//  Minimum
//
//  Created by Nail Sharipov on 10/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXSideMenuTableViewSource.h"
#import "TXSideMenuItem.h"
#import "TXSideMenuTableViewCell.h"
#import "TXSideMenuHeaderTableViewCell.h"


@interface TXSideMenuTableViewSource () <UITableViewDelegate, UITableViewDataSource>
    

@end

@implementation TXSideMenuTableViewSource

- (void)setTableView:(UITableView *)tableView {
    [tableView registerNib:[TXSideMenuTableViewCell cellNib] forCellReuseIdentifier:[TXSideMenuTableViewCell cellIdentifier]];
    [tableView registerNib:[TXSideMenuHeaderTableViewCell cellNib] forCellReuseIdentifier:[TXSideMenuHeaderTableViewCell cellIdentifier]];
    tableView.delegate = self;
    tableView.dataSource = self;
    _tableView = tableView;
}

- (void)updateItem:(TXSideMenuItem *)item {
    if (_itemList != nil) {
        NSUInteger i = 0;
        for (TXSideMenuItem *elem in _itemList) {
            if (item == elem) {
                break;
            }
            i++;
        }
        
        if (i < _itemList.count) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
            
            [_tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - <UITableViewDataSource>
//-------------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_itemList != nil) {
        return _itemList.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ix = (NSUInteger) indexPath.row;
    TXSideMenuItem *item = _itemList[ix];
    if (item.isHeader) {
        return [tableView dequeueReusableCellWithIdentifier:[TXSideMenuHeaderTableViewCell cellIdentifier]];
    } else {
        return [tableView dequeueReusableCellWithIdentifier:[TXSideMenuTableViewCell cellIdentifier]];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - <UITableViewDelegate>
//-------------------------------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView willDisplayCell:(id<TXSideMenuCell>)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ix = (NSUInteger) indexPath.row;
    cell.item = _itemList[ix];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ix = (NSUInteger) indexPath.row;
    [_delegate onSelectItem:_itemList[ix]];
}


@end
