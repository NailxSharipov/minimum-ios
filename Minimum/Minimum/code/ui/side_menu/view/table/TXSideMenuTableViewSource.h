////////////////////////////////////////////////////////////////////////////////
//
//  TXSideMenuTableViewSource.h
//  Minimum
//
//  Created by Nail Sharipov on 10/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXSideMenuItem;

@protocol TXSideMenuTableViewSourceDelegate <NSObject>

@required

- (void)onSelectItem:(TXSideMenuItem *)item;

@end

@interface TXSideMenuTableViewSource : NSObject

@property (weak, nonatomic)UITableView *tableView;
@property (strong, nonatomic)NSArray<TXSideMenuItem *> *itemList;
@property (weak, nonatomic)id<TXSideMenuTableViewSourceDelegate> delegate;

- (void)updateItem:(TXSideMenuItem *)item;


@end
