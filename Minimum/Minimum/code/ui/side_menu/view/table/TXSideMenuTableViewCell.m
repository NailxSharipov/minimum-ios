////////////////////////////////////////////////////////////////////////////////
//
//  TXSideMenuTableViewCell.m
//  Minimum
//
//  Created by Nail Sharipov on 10/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXSideMenuTableViewCell.h"
#import "TXSideMenuItem.h"
#import "TXColorPallete.h"
#import "TXThinLineView.h"

@interface TXSideMenuTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *ico;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet TXThinLineView *separator;

@end

@implementation TXSideMenuTableViewCell

//-------------------------------------------------------------------------------------------
#pragma mark - Class Methods
//-------------------------------------------------------------------------------------------

+ (CGFloat)cellHeight {
    return 44.0f;
}

+ (NSString *)cellIdentifier {
    return @"TXSideMenuTableViewCell";
}

+ (UINib *)cellNib {
    return [UINib nibWithNibName:@"TXSideMenuTableViewCell" bundle:nil];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Overridden Methods
//-------------------------------------------------------------------------------------------

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    if (highlighted) {
        self.backgroundColor = [TXColorPallete sideMenuSelectColor];
    } else {
        self.backgroundColor = [TXColorPallete sideMenuCellColor];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Public Methods
//-------------------------------------------------------------------------------------------

- (void)setItem:(TXSideMenuItem *)item {
    _item = item;
    _ico.image = [item.ico imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _ico.tintColor = item.icoColor;

    _title.text = item.title;
    _title.textColor = item.titleColor;
    
    self.backgroundColor = [TXColorPallete sideMenuCellColor];
    
    _separator.hidden = item.isSeparatorHiden;
}

@end
