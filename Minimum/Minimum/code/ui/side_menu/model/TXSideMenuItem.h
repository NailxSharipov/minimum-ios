////////////////////////////////////////////////////////////////////////////////
//
//  TXSideMenuItem.h
//  Minimum
//
//  Created by Nail Sharipov on 10/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@interface TXSideMenuItem : NSObject

@property (nonatomic) NSUInteger index;
@property (nonatomic) BOOL isHeader;
@property (nonatomic) BOOL isSeparatorHiden;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) UIColor *titleColor;
@property (strong, nonatomic) UIImage *ico;
@property (strong, nonatomic) UIColor *icoColor;

@end
