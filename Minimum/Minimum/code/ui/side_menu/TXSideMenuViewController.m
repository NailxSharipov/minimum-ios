////////////////////////////////////////////////////////////////////////////////
//
//  TXSideMenuViewController.m
//  Minimum
//
//  Created by Nail Sharipov on 08/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXSideMenuViewController.h"
#import "TXMakeOrderViewController.h"
#import "TXMainViewController.h"
#import "TXPlaceView.h"
#import "TXTaskExecutor.h"
#import "TXLoadHistoryTask.h"
#import "TXQueryHistoryTask.h"
#import "TXSideMenuTableViewSource.h"
#import "TXSideMenuTableViewCell.h"
#import "TXSideMenuItem.h"
#import "TXFastHistoryTableViewSource.h"
#import "TXColorPallete.h"
#import "TXCityDAO.h"
#import "TXUserDAO.h"
#import "TXPhoneNumberField.h"
#import "TXRouter.h"
#import "TXCurrentOrder.h"

const NSUInteger TXHistoryPageSize = 10;

@interface TXSideMenuViewController () <TXSideMenuTableViewSourceDelegate, TXLoadHistoryDelegate, TXQueryHistoryTaskDelegate, TXFastHistoryTableViewSourceDelegate>

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UITableView *fastHistoryTable;

@end

@implementation TXSideMenuViewController {
    TXSideMenuTableViewSource *_tableSource;
    TXFastHistoryTableViewSource *_fastHistoryTableSource;
    
    TXSideMenuItem *_citeMenuItem;
    TXSideMenuItem *_profileMenuItem;
    TXSideMenuItem *_curentOrderMenuItem;
    TXSideMenuItem *_historyMenuItem;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [TXColorPallete sideMenuColor];
    [self prepareTable];
    [self prepareFastHistoryTable];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    TXCity *city = _cityDAO.actualCity;
    _citeMenuItem.title = city.title;
    [_tableSource updateItem:_citeMenuItem];
    _queryHistoryTask.size = TXHistoryPageSize;
    _queryHistoryTask.index = 0;
    _queryHistoryTask.dateTimeStamp = 0;
    _queryHistoryTask.delegate = self;
    [_taskExecutor executeTask:_queryHistoryTask];
    
    _loadHistoryTask.delegate = self;
    _loadHistoryTask.index = 0;
    _loadHistoryTask.size = TXHistoryPageSize;
    [_taskExecutor executeTask:_loadHistoryTask];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXQueryHistoryTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onQueryHistoryTaskSuccess:(NSArray<TXOrder *> *)history {
    _fastHistoryTableSource.itemList = [[NSMutableArray alloc] initWithArray:history];
    if (_fastHistoryTableSource.itemList == nil || _fastHistoryTableSource.itemList.count == 0) {
        _fastHistoryTable.hidden = YES;
    } else {
        _fastHistoryTable.hidden = NO;
    }
    [_fastHistoryTable reloadData];
}

- (void)onQueryHistoryTaskError:(NSString *)error {

}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXFastHistoryTableViewSourceDelegate>
//-------------------------------------------------------------------------------------------

- (void)onSelectOrderItem:(TXOrder *)order {
    [_currentOrder setWithOrder:order];
    [_makeOrder.placeView reloadData];
    [_main toggleMenu];
}
- (void)onLastItemShowed:(NSUInteger)index {}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXLoadHistoryDelegate>
//-------------------------------------------------------------------------------------------

- (void)onLoadHistoryTaskSuccess:(NSUInteger)lastDateTimeStamp {
    _queryHistoryTask.dateTimeStamp = lastDateTimeStamp;
    _queryHistoryTask.delegate = self;
    [_taskExecutor executeTask:_queryHistoryTask];
}

- (void)onLoadHistoryTaskError:(NSString *)error {
    
}

//-------------------------------------------------------------------------------------------
#pragma mark - <UITableViewDataSource>
//-------------------------------------------------------------------------------------------

- (void)onSelectItem:(TXSideMenuItem *)item {
    if (item == _citeMenuItem) {
        [_routeToSelectCity routeFrom:_main];
    } else if (item == _profileMenuItem) {
        [_main presentViewController:_profile animated:YES completion:nil];
    } else if (item == _curentOrderMenuItem) {
        [_main presentViewController:_currentOrderVC animated:YES completion:nil];
    } else if (item == _historyMenuItem) {
        [_main presentViewController:_history animated:YES completion:nil];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Override Methods
//-------------------------------------------------------------------------------------------

- (BOOL)prefersStatusBarHidden {
    return NO;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (void)prepareTable {
    NSMutableArray<TXSideMenuItem *> *menuItems = [[NSMutableArray alloc] init];
    
    {
        // city
        
        TXSideMenuItem *item = [[TXSideMenuItem alloc] init];
        item.index = 0;
        item.title = @"";
        item.titleColor = TXColorPallete.orangeColor;
        
        item.ico = [UIImage imageNamed:@"ico_city"];
        item.icoColor = TXColorPallete.orangeColor;
        item.isHeader = YES;
        item.isSeparatorHiden = NO;
        
        [menuItems addObject:item];
        
        _citeMenuItem = item;
    }
    
    {
        // profile
        
        TXSideMenuItem *item = [[TXSideMenuItem alloc] init];
        item.index = 1;
        item.title = NSLocalizedString(@"Profile", nil);
        item.titleColor = [UIColor whiteColor];
        
        item.ico = [UIImage imageNamed:@"ico_profile"];
        item.icoColor = [UIColor whiteColor];
        item.isHeader = NO;
        item.isSeparatorHiden = NO;
        
        [menuItems addObject:item];
        
        _profileMenuItem = item;
    }
    
    {
        // current order
        
        TXSideMenuItem *item = [[TXSideMenuItem alloc] init];
        item.index = 2;
        item.title = NSLocalizedString(@"Current order", nil);
        item.titleColor = [UIColor whiteColor];
        
        item.ico = [UIImage imageNamed:@"ico_taxi"];
        item.icoColor = [UIColor whiteColor];
        item.isHeader = NO;
        item.isSeparatorHiden = NO;
        
        [menuItems addObject:item];
        
        _curentOrderMenuItem = item;
    }
    
    {
        // history
        
        TXSideMenuItem *item = [[TXSideMenuItem alloc] init];
        item.index = 3;
        item.title = NSLocalizedString(@"Repeat order", nil);
        item.titleColor = [UIColor whiteColor];
        
        item.ico = [UIImage imageNamed:@"ico_repeat_taxi"];
        item.icoColor = [UIColor whiteColor];
        item.isHeader = NO;
        item.isSeparatorHiden = YES;
        
        [menuItems addObject:item];
        
        _historyMenuItem = item;
    }
    
    _tableSource = [[TXSideMenuTableViewSource alloc] init];
    _tableSource.delegate = self;
    
    _tableSource.itemList = menuItems;
    _tableSource.tableView = _table;
    
    _table.rowHeight = TXSideMenuTableViewCell.cellHeight;
    
    [_table reloadData];

}

- (void)prepareFastHistoryTable {
    _fastHistoryTable.backgroundColor = [TXColorPallete sideMenuBottomColor];
    _fastHistoryTableSource = [[TXFastHistoryTableViewSource alloc] init];
    _fastHistoryTableSource.isDark = YES;
    _fastHistoryTableSource.isEditable = NO;
    _fastHistoryTableSource.tableView = _fastHistoryTable;
    _fastHistoryTableSource.delegate = self;
    if (_fastHistoryTableSource.itemList == nil || _fastHistoryTableSource.itemList.count == 0) {
        _fastHistoryTable.hidden = YES;
    } else {
        _fastHistoryTable.hidden = NO;
    }
}

@end
