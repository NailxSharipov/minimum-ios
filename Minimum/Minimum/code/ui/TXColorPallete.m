//
//  TXColorPallete.m
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//

#import "TXColorPallete.h"
#import "UIColor+Utils.h"

@implementation TXColorPallete

+ (UIColor *)navigationBarColor {
    return [UIColor colorWithNormalRed:255 green:187 blue:57];
}

+ (UIColor *)navigationBarTitleColor {
    return [UIColor blackColor];
}

+ (UIColor *)orangeColor {
    return [UIColor colorWithNormalRed:253 green:161 blue:45];
}

+ (UIColor *)sheetColor {
    return [UIColor colorWithNormalRed:255 green:212 blue:119];
}

+ (UIColor *)sideMenuColor {
    return [UIColor colorWithNormalRed:40 green:40 blue:40];
}

+ (UIColor *)sideMenuCellColor {
    return [UIColor colorWithNormalRed:52 green:52 blue:52];
}

+ (UIColor *)sideMenuBottomColor {
    return [UIColor colorWithNormalRed:68 green:68 blue:68];
}

+ (UIColor *)sideMenuSelectColor {
    return [UIColor colorWithNormalRed:46 green:46 blue:46];
}

@end
