////////////////////////////////////////////////////////////////////////////////
//
//  TXEditOrderViewController.m
//  Minimum
//
//  Created by Nail Sharipov on 08/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXEditOrderViewController.h"
#import "TXOrderNavigationController.h"
#import "TXDetailPlaceTableViewSource.h"
#import "TXColorPallete.h"
#import "TXDetailPlaceViewController.h"
#import "TXEditPlaceViewController.h"
#import "TXPlace.h"

@interface TXEditOrderViewController () <TXDetailPlaceTableViewSourceDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UIView *sheet;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *content;
@property (weak, nonatomic) IBOutlet UINavigationItem *titleBar;
@property (weak, nonatomic) IBOutlet UILabel *streetTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentBottomConstraint;


@end

@implementation TXEditOrderViewController {
    UIViewController *_activeViewController;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXDetailPlaceTableViewSourceDelegate>
//-------------------------------------------------------------------------------------------

- (void)onSelectItem:(TXPlace *)place {
    [_place setWithPlace:place];

    if (_place.isFrom && (_place.definition == nil || _place.definition.length == 0)) {
        [self showEditPlace];
    } else if (_place.isStreet && (_place.home == nil || _place.home.length == 0)) {
        [self showEditPlace];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - <UISearchBarDelegate>
//-------------------------------------------------------------------------------------------

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSString *key = searchText;
    [self showDetailPlace];
    if (key.length >= 3) {
        [_detailPlace showByKey:key];
    } else {
        [_detailPlace showLast];
    }
}


//-------------------------------------------------------------------------------------------
#pragma mark - Overridden Methods
//-------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [TXColorPallete orangeColor];
    _sheet.backgroundColor = [TXColorPallete sheetColor];
    _searchBar.placeholder = NSLocalizedString(@"type an address", nil);
    _searchBar.delegate = self;
    _streetTitle.text = NSLocalizedString(@"Street", nil);

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _contentBottomConstraint.constant = 0.0f;
    _titleBar.title = _place.action;
    _searchBar.text = _place.name;
    if (_place.isEmpty) {
        [self showDetailPlace];
    } else {
        [self showEditPlace];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:@"UIKeyboardWillHideNotification"
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (_searchBar.text == nil || _searchBar.text.length < 2) {
        [_searchBar becomeFirstResponder];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)placeOnMapButtonPressed:(id)sender {
    [(TXOrderNavigationController *)self.navigationController placeOnMapPlace: _place];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (void)showDetailPlace {
    if (_activeViewController != _detailPlace) {
        [self clearContent];

        _detailPlace.delegate = self;
        CGRect frame = _content.frame;
        _detailPlace.view.frame = CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height);
        
        [_content addSubview:_detailPlace.view];
        [self addChildViewController:_detailPlace];
        [_detailPlace didMoveToParentViewController:self];
        
        _activeViewController = _detailPlace;
    }
    [_detailPlace showLast];
}

- (void)showEditPlace {
    _editPlace.place = _place;
    _searchBar.text = _place.name;
    if (_activeViewController != _editPlace) {
        [self clearContent];
        CGRect frame = _content.frame;
        _editPlace.view.frame = CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height);
        
        [_content addSubview:_editPlace.view];
        [self addChildViewController:_editPlace];
        [_editPlace didMoveToParentViewController:self];
        
        _activeViewController = _editPlace;
    }
}

- (void)clearContent {
    if (_activeViewController == _detailPlace) {
        [_detailPlace removeFromRoot];
    } else if (_activeViewController == _editPlace) {
        [_editPlace removeFromRoot];
    }
}

- (void)keyboardWillShow:(NSNotification *)note {
    // resize tableView
    NSDictionary *userInfo = [note userInfo];
    double time = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGFloat height = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    if (height > 0) {
        [UIView animateWithDuration:time
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             _contentBottomConstraint.constant = height;
                             [_content.superview layoutIfNeeded];
                         }
                         completion:nil
        ];
    }
}

- (void)keyboardWillHide:(NSNotification *)note {
    NSDictionary *userInfo = [note userInfo];
    double time = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:time
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         _contentBottomConstraint.constant = 0.0f;
                         [_content.superview layoutIfNeeded];
                     }
                     completion:nil
     ];
}

@end
