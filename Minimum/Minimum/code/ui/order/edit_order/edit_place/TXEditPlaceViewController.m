////////////////////////////////////////////////////////////////////////////////
//
//  TXEditPlaceViewController.m
//  Minimum
//
//  Created by Nail Sharipov on 28/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXEditPlaceViewController.h"
#import "TXPlace.h"

@interface TXEditPlaceViewController ()

@property (weak, nonatomic) IBOutlet UILabel *homeTitle;
@property (weak, nonatomic) IBOutlet UITextField *homeField;
@property (weak, nonatomic) IBOutlet UIView *homeView;
@property (weak, nonatomic) IBOutlet UILabel *definitionTitle;
@property (weak, nonatomic) IBOutlet UITextView *definitionField;
@property (weak, nonatomic) IBOutlet UIView *definitionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *definationTop;


@end

@implementation TXEditPlaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _homeTitle.text = NSLocalizedString(@"Street number", nil);
    _definitionTitle.text = NSLocalizedString(@"Defination", nil);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_place.isStreet) {
        _homeField.text = _place.home;
        _homeView.hidden = NO;
        _homeTitle.hidden = NO;
        _definationTop.constant = 100.0f;
    } else {
        _homeField.text = nil;
        _homeView.hidden = YES;
        _homeTitle.hidden = YES;
        _definitionField.text = nil;
        _definationTop.constant = 32.0f;
    }
    
    if (_place.isFrom) {
        _definitionView.hidden = NO;
        _definitionTitle.hidden = NO;
        _definitionField.text = _place.definition;
    } else {
        _definitionView.hidden = YES;
        _definitionTitle.hidden = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (_place.isStreet) {
        if (_place.home == nil || _place.home.length == 0) {
            [_homeField becomeFirstResponder];
        } else if (_place.isFrom && (_place.definition == nil || _place.definition.length == 0)) {
            [_definitionField becomeFirstResponder];
        }
    } else if (_place.isFrom && (_place.definition == nil || _place.definition.length == 0)) {
        [_definitionField becomeFirstResponder];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    if (_place.isStreet) {
        _place.home = _homeField.text;
    }
    if (_place.isFrom) {
        _place.definition = _definitionField.text;
    }
}

- (void)removeFromRoot {
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}


@end
