////////////////////////////////////////////////////////////////////////////////
//
//  TXEditPlaceViewController.h
//  Minimum
//
//  Created by Nail Sharipov on 28/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXPlace;

@interface TXEditPlaceViewController : UIViewController

@property (strong, nonatomic) TXPlace *place;

- (void)removeFromRoot;

@end
