////////////////////////////////////////////////////////////////////////////////
//
//  TXEditOrderViewController.h
//  Minimum
//
//  Created by Nail Sharipov on 08/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXDetailPlaceViewController;
@class TXEditPlaceViewController;
@class TXPlace;

@interface TXEditOrderViewController : UIViewController

@property (weak, nonatomic) TXPlace *place;
@property (strong, nonatomic) TXDetailPlaceViewController *detailPlace;
@property (strong, nonatomic) TXEditPlaceViewController *editPlace;

@end
