////////////////////////////////////////////////////////////////////////////////
//
//  TXDetailPlaceViewController.h
//  Minimum
//
//  Created by Nail Sharipov on 24/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXQuerryLastPlaceTask;
@class TXQuerryPlaceByKeyTask;
@protocol TXDetailPlaceTableViewSourceDelegate;
@protocol TXTaskExecutor;

@interface TXDetailPlaceViewController : UIViewController

@property (strong, nonatomic) TXQuerryLastPlaceTask *querryLastPlaceTask;
@property (strong, nonatomic) TXQuerryPlaceByKeyTask *querryPlaceByKeyTask;
@property (strong, nonatomic) id<TXTaskExecutor> taskExecutor;
@property (weak, nonatomic) id<TXDetailPlaceTableViewSourceDelegate> delegate;

- (void)showLast;
- (void)showByKey:(NSString *)key;
- (void)removeFromRoot;

@end
