////////////////////////////////////////////////////////////////////////////////
//
//  TXDetailPlaceViewController.m
//  Minimum
//
//  Created by Nail Sharipov on 24/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXDetailPlaceViewController.h"
#import "TXQuerryLastPlaceTask.h"
#import "TXQuerryPlaceByKeyTask.h"
#import "TXDetailPlaceTableViewSource.h"
#import "TXTaskExecutor.h"


@interface TXDetailPlaceViewController () <TXQuerryLastPlaceTaskDelegate, TXQuerryPlaceByKeyDelegate>

@property (weak, nonatomic) IBOutlet UITableView *table;

@end

@implementation TXDetailPlaceViewController {
    TXDetailPlaceTableViewSource *_tableSource;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXQuerryLastPlaceTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onQuerryLastPlaceTaskSuccess:(NSArray<TXPlace *> *)placeList {
    _tableSource.itemList = placeList;
    [_table reloadData];
    if (_tableSource.itemList == nil || _tableSource.itemList.count == 0) {
        _table.hidden = YES;
    } else {
        _table.hidden = NO;
    }
}

- (void)onQuerryLastPlaceTaskError:(NSString *)error {

}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXQuerryPlaceByKeyDelegate>
//-------------------------------------------------------------------------------------------

- (void)onQuerryPlaceByKeyTaskSuccess:(NSArray<TXPlace *> *)placeList {
    _tableSource.itemList = placeList;
    [_table reloadData];
    if (_tableSource.itemList == nil || _tableSource.itemList.count == 0) {
        _table.hidden = YES;
    } else {
        _table.hidden = NO;
    }
}

- (void)onQuerryPlaceByKeyTaskError:(NSString *)error {

}

- (void)onQuerryPlaceByKeyTaskStartServer {

}

- (void)onQuerryPlaceByKeyTaskStopServer {

}

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableSource = [TXDetailPlaceTableViewSource new];
    _tableSource.tableView = _table;
    _tableSource.delegate = _delegate;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_tableSource.itemList == nil || _tableSource.itemList.count == 0) {
        _table.hidden = YES;
    } else {
        _table.hidden = NO;
    }
}

- (void)showLast {
    _querryLastPlaceTask.delegate = self;
    [_taskExecutor executeTask:_querryLastPlaceTask];
}

- (void)showByKey:(NSString *)key {
    _querryPlaceByKeyTask.delegate = self;
    [_querryPlaceByKeyTask cancel];
    _querryPlaceByKeyTask.key = key;
    _querryPlaceByKeyTask.objectType = nil;
    _querryPlaceByKeyTask.number = nil;
    [_taskExecutor executeTask:_querryPlaceByKeyTask];
}

- (void)removeFromRoot {
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}


@end
