////////////////////////////////////////////////////////////////////////////////
//
//  TXHistoryPlaceTableViewCell.h
//  Minimum
//
//  Created by Nail Sharipov on 24/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXPlace;

@interface TXDetailPlaceTableViewCell : UITableViewCell

+ (CGFloat)cellHeight;

+ (NSString *)cellIdentifier;

+ (UINib *)cellNib;

- (void)setItem:(TXPlace *)item;

@end
