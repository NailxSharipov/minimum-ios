////////////////////////////////////////////////////////////////////////////////
//
//  TXHistoryPlaceTableViewCell.m
//  Minimum
//
//  Created by Nail Sharipov on 24/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXDetailPlaceTableViewCell.h"
#import "TXPlace.h"

@interface TXDetailPlaceTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *object;
@property (weak, nonatomic) IBOutlet UILabel *address;

@end

@implementation TXDetailPlaceTableViewCell

//-------------------------------------------------------------------------------------------
#pragma mark - Class Methods
//-------------------------------------------------------------------------------------------

+ (CGFloat)cellHeight {
    return 44.0f;
}

+ (NSString *)cellIdentifier {
    return @"TXDetailPlaceTableViewCell";
}

+ (UINib *)cellNib {
    return [UINib nibWithNibName:@"TXDetailPlaceTableViewCell" bundle:nil];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Overridden Methods
//-------------------------------------------------------------------------------------------

- (void)awakeFromNib {

}

//-------------------------------------------------------------------------------------------
#pragma mark - Public Methods
//-------------------------------------------------------------------------------------------

- (void)setItem:(TXPlace *)item {
    _object.text = item.typeName;
    _address.text = item.address;
}


@end
