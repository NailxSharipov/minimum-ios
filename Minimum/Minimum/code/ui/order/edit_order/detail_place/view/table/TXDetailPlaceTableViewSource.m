////////////////////////////////////////////////////////////////////////////////
//
//  TXDetailPlaceTableSource.m
//  Minimum
//
//  Created by Nail Sharipov on 24/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXDetailPlaceTableViewSource.h"
#import "TXDetailPlaceTableViewCell.h"


@interface TXDetailPlaceTableViewSource () <UITableViewDelegate, UITableViewDataSource>


@end

@implementation TXDetailPlaceTableViewSource

//-------------------------------------------------------------------------------------------
#pragma mark - Public Methods
//-------------------------------------------------------------------------------------------

- (void)setTableView:(UITableView *)tableView {
    [tableView registerNib:[TXDetailPlaceTableViewCell cellNib] forCellReuseIdentifier:[TXDetailPlaceTableViewCell cellIdentifier]];
    tableView.delegate = self;
    tableView.dataSource = self;
//    tableView.rowHeight = [TXDetailPlaceTableViewCell cellHeight];
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 80.0f;
    _tableView = tableView;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <UITableViewDataSource>
//-------------------------------------------------------------------------------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_itemList != nil) {
        return _itemList.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TXDetailPlaceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[TXDetailPlaceTableViewCell cellIdentifier]];
    return cell;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <UITableViewDelegate>
//-------------------------------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView willDisplayCell:(TXDetailPlaceTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ix = (NSUInteger) indexPath.row;
    cell.item = _itemList[ix];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ix = (NSUInteger) indexPath.row;
    TXPlace *place = _itemList[ix];
    [_delegate onSelectItem:place];
}


@end