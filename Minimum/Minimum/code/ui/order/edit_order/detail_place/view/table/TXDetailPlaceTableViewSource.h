////////////////////////////////////////////////////////////////////////////////
//
//  TXDetailPlaceTableSource.h
//  Minimum
//
//  Created by Nail Sharipov on 24/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXPlace;

@protocol TXDetailPlaceTableViewSourceDelegate <NSObject>

@required

- (void)onSelectItem:(TXPlace *)place;

@end

@interface TXDetailPlaceTableViewSource : NSObject

@property (weak, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSArray<TXPlace *> *itemList;
@property (weak, nonatomic) id<TXDetailPlaceTableViewSourceDelegate> delegate;

@end
