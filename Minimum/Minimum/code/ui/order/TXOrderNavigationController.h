////////////////////////////////////////////////////////////////////////////////
//
//  TXOrderNavigationController.h
//  Minimum
//
//  Created by Nail Sharipov on 08/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXPlace;
@class TXEditOrderViewController;
@class TXPlaceOnMapViewController;

@interface TXOrderNavigationController : UINavigationController

@property(strong, nonatomic) UIViewController *makeOrder;
@property(strong, nonatomic) TXEditOrderViewController *editOrder;
@property(strong, nonatomic) TXPlaceOnMapViewController *placeOnMap;

- (void)editPlace:(TXPlace *)place;
- (void)placeOnMapPlace:(TXPlace *)place;

@end
