////////////////////////////////////////////////////////////////////////////////
//
//  TXPlaceOnMapViewController.m
//  Minimum
//
//  Created by Nail Sharipov on 30/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXPlaceOnMapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "TXLocationManager.h"
#import "TXTaskExecutor.h"
#import "TXColorPallete.h"

@interface TXPlaceOnMapViewController () <GMSMapViewDelegate, TXDecodeLocationTaskDelegate>

@property (weak, nonatomic) IBOutlet UINavigationItem *titleBar;
@property (weak, nonatomic) IBOutlet UIView *sheet;
@property (weak, nonatomic) IBOutlet UIView *placeView;
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *markerView;

@end

@implementation TXPlaceOnMapViewController {
    GMSMapView *_mapView;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXDecodeLocationTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onDecodeLocationTaskSuccess:(TXPlace *)place {
    if (place != nil) {
        _placeLabel.text = place.address;
        [_place setWithPlace:place];
    }
}

- (void)onDecodeLocationTaskError:(NSString *)error {
    _placeLabel.text = NSLocalizedString(@"Address not found", nil);
}

//-------------------------------------------------------------------------------------------
#pragma mark - <GMSMapViewDelegate>
//-------------------------------------------------------------------------------------------

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {

    _decodeLocationTask.delegate = self;
    [_decodeLocationTask cancel];
    CLLocationCoordinate2D point = mapView.camera.target;
    _decodeLocationTask.latitude = point.latitude;
    _decodeLocationTask.longitude = point.longitude;
    [_taskExecutor executeTask:_decodeLocationTask];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Overridden Methods
//-------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [TXColorPallete orangeColor];
    _titleBar.title = NSLocalizedString(@"Set destination point", nil);
    if (_mapView == nil) {
        double latitude = _locationManager.latitude;
        double longitude = _locationManager.longitude;
        GMSCameraPosition *camera;
        if (longitude != 0.0 || latitude != 0.0) {
            camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                                    longitude:longitude
                                                                         zoom:17];
        } else {
            camera = [GMSCameraPosition cameraWithLatitude:55.0
                                                                    longitude:55.0
                                                                         zoom:10];
        }

        
        CGRect rect = _sheet.frame;
        rect.origin.x = 0.0f;
        rect.origin.y = 0.0f;
        
        _mapView = [GMSMapView mapWithFrame:rect camera:camera];
        [_mapView setMapType:kGMSTypeNormal];
        _mapView.myLocationEnabled = YES;
        _mapView.delegate = self;
        [_sheet addSubview:_mapView];
        
        _markerView.layer.zPosition = 1;
    }
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

@end
