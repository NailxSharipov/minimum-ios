////////////////////////////////////////////////////////////////////////////////
//
//  TXPlaceOnMapViewController.h
//  Minimum
//
//  Created by Nail Sharipov on 30/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import "TXDecodeLocationTask.h"

@protocol TXTaskExecutor;
@class TXLocationManager;
@class TXPlace;

@interface TXPlaceOnMapViewController : UIViewController

@property (weak, nonatomic) TXLocationManager *locationManager;
@property (strong, nonatomic) TXPlace *place;
@property (strong, nonatomic) TXDecodeLocationTask *decodeLocationTask;
@property (strong, nonatomic) id<TXTaskExecutor> taskExecutor;

@end
