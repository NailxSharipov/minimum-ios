////////////////////////////////////////////////////////////////////////////////
//
//  TXMakeOrderViewController.m
//  Minimum
//
//  Created by Nail Sharipov on 08/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXMakeOrderViewController.h"
#import "TXCurrentOrder.h"
#import "TXPlaceView.h"
#import "TXLocationManager.h"
#import "TXMainViewController.h"
#import "TXOrderNavigationController.h"
#import "TXColorPallete.h"
#import "TXLocationManager.h"
#import "TXMakeOrderTask.h"
#import "TXCityDAO.h"
#import "TXUserDAO.h"
#import "TXTaskExecutor.h"
#import "TXProfileViewController.h"

@interface TXMakeOrderViewController () <TXPlaceViewDelegate, TXLocationManagerDelegate, TXMakeOrderTaskDelegate>

@property (weak, nonatomic) IBOutlet UIView *sheet;
@property (weak, nonatomic) IBOutlet UINavigationItem *titleBar;
@property (weak, nonatomic) IBOutlet UIButton *makeOrderButton;
@property (weak, nonatomic) IBOutlet UILabel *orderPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@end

@implementation TXMakeOrderViewController

//-------------------------------------------------------------------------------------------
#pragma mark - <TXMakeOrderTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onMakeOrderTaskSuccess:(NSDictionary *)dict {
    if (dict != nil) {
        NSString *orderId = dict[@"orderId"];
        NSString *price = dict[@"price"];
        NSString *error = dict[@"error"];
        if (orderId != nil && orderId.length > 0) {
            _currentOrder.orderId = orderId;
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@""
                                                  message:NSLocalizedString(@"Your order is accepted", nil)
                                                  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *goAction = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"GO", nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction *action) {
                                            [self presentViewController:_currentOrderVC animated:YES completion:nil];
                                        }];
            
            [alertController addAction:goAction];
            [self presentViewController:alertController animated:YES completion:nil];
        } else if (price != nil && price.length > 0) {
            NSString *money = _cityDAO.actualCity.money;
            _orderPriceLabel.text = [NSString stringWithFormat:@"%@ %@", price, money];
            _makeOrderButton.enabled = YES;
        } else if (error != nil && error.length > 0) {
            _errorLabel.text = error;
        }
    }
}

- (void)onMakeOrderTaskError:(NSString *)error {
    _errorLabel.text = error;
    _makeOrderButton.enabled = NO;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXPlaceViewDelegate>
//-------------------------------------------------------------------------------------------

- (void)onCellPressed:(TXPlace *)place {
    [(TXOrderNavigationController *)self.navigationController editPlace:place];
}

- (void)onReload {
    _makeOrderButton.enabled = NO;
    _errorLabel.text = @"";
    if ([_currentOrder isValid]) {
        _makeOrderTask.delegate = self;
        _makeOrderTask.isPriceOnly = YES;
        [_taskExecutor executeTask:_makeOrderTask];
    } else {
        _orderPriceLabel.text = @"";
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXLocationManagerDelegate>
//-------------------------------------------------------------------------------------------

- (void)onLocationManagerSuccess:(TXPlace *)place {
    TXPlace *firstPlace = _currentOrder.placeList[0];
    if (firstPlace != nil && place!= nil && firstPlace.isEmpty) {
        [firstPlace setWithPlace: place];
        [_placeView reloadData];
    }
}

- (void)onLocationManagerError:(NSString *)error {

}


//-------------------------------------------------------------------------------------------
#pragma mark - Overridden Methods
//-------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [TXColorPallete orangeColor];
    _sheet.backgroundColor = [TXColorPallete sheetColor];
    _placeView.currentOrder = _currentOrder;
    _placeView.placeViewDelegate = self;
    _titleBar.title = NSLocalizedString(@"Make an order", nil);
    [_makeOrderButton setTitle:NSLocalizedString(@"Make the order", nil) forState:UIControlStateNormal];
    _locationManager.delegate = self;
}

- (IBAction)menuButtonPressed:(id)sender {
    [_main toggleMenu];
}

- (IBAction)makeOrderButtonPressed:(id)sender {
    _errorLabel.text = @"";
    [self makeOrder];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    TXPlace *firstPlace = _currentOrder.placeList[0];
    TXPlace *place = _locationManager.place;
    if (firstPlace != nil && place != nil && firstPlace.isEmpty) {
        [firstPlace setWithPlace: place];
    }
    [_placeView reloadData];
    _errorLabel.text = @"";
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.revealViewController.panGestureRecognizer.enabled = YES;
    [_locationManager checkAccess];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.revealViewController.panGestureRecognizer.enabled = NO;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (void)makeOrder {

    TXUser *user = _userDAO.actualUser;
    NSString *phone = user.phone;
    NSString *secondPhone = user.secondPhone;

    TXPlace *from = _currentOrder.getNotEmptyPlaceList.firstObject;
        
    if (from.definition == nil || from.definition.length == 0) {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"Attention", nil)
                                              message:NSLocalizedString(@"Set defination", nil)
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action) {
                                       [(TXOrderNavigationController *)self.navigationController editPlace:from];
                                   }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else if (secondPhone != nil && secondPhone.length == 10 && ![secondPhone isEqualToString:phone]) {
        NSString *question = NSLocalizedString(@"Second phone question", nil);
        NSString *message = [NSString stringWithFormat:question, phone, secondPhone];
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"Attention", nil)
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Continue", nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action) {
                                       [self orderIsValidToGo];
                                   }];
        [alertController addAction:okAction];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Correct", nil)
                                   style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction *action) {
                                       [_main presentViewController:_profile animated:YES completion:^() {
                                           [_profile.notifyPhonePhield becomeFirstResponder];
                                       }];
                                   }];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        [self orderIsValidToGo];
    }
}

- (void)orderIsValidToGo {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"Attention", nil)
                                          message:NSLocalizedString(@"Are you sure, you want to make the order", nil)
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Yes", nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   _makeOrderTask.delegate = self;
                                   _makeOrderTask.isPriceOnly = NO;
                                   [_makeOrderTask cancel];
                                   [_taskExecutor executeTask:_makeOrderTask];
                               }];
    
    [alertController addAction:yesAction];

    UIAlertAction *noAction = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"No", nil)
                                style:UIAlertActionStyleDestructive
                                handler:nil];
    
    [alertController addAction:noAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
