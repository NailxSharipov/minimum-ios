////////////////////////////////////////////////////////////////////////////////
//
//  TXMakeOrderViewController.h
//  Minimum
//
//  Created by Nail Sharipov on 08/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXPlaceView;
@class TXCurrentOrder;
@class TXMainViewController;
@class TXLocationManager;
@class TXMakeOrderTask;
@class TXProfileViewController;

@protocol TXCityDAO;
@protocol TXUserDAO;
@protocol TXTaskExecutor;

@interface TXMakeOrderViewController : UIViewController

@property (weak, nonatomic) IBOutlet TXPlaceView *placeView;

@property (weak, nonatomic) TXCurrentOrder *currentOrder;
@property (weak, nonatomic) id<TXCityDAO> cityDAO;
@property (strong, nonatomic) id<TXUserDAO> userDAO;
@property (weak, nonatomic) TXMainViewController *main;
@property (weak, nonatomic) TXLocationManager *locationManager;
@property (strong, nonatomic) TXMakeOrderTask *makeOrderTask;
@property (strong, nonatomic) id<TXTaskExecutor> taskExecutor;
@property (strong, nonatomic) TXProfileViewController *profile;
@property (strong, nonatomic) UIViewController *currentOrderVC;


@end
