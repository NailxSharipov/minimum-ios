////////////////////////////////////////////////////////////////////////////////
//
//  TXOrderNavigationController.m
//  Minimum
//
//  Created by Nail Sharipov on 08/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXOrderNavigationController.h"
#import "TXEditOrderViewController.h"
#import "TXPlaceOnMapViewController.h"

@interface TXOrderNavigationController ()

@end

@implementation TXOrderNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.interactivePopGestureRecognizer setDelegate:nil];
    [self setNavigationBarHidden:YES animated:NO];
    [self pushViewController:_makeOrder animated:NO];
}

- (void)editPlace:(TXPlace *)place {
    _editOrder.place = place;
    [self pushViewController:_editOrder animated:YES];
}

- (void)placeOnMapPlace:(TXPlace *)place {
    _placeOnMap.place = place;
    [self pushViewController:_placeOnMap animated:YES];
}

@end
