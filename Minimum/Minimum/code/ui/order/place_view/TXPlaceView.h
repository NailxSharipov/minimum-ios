////////////////////////////////////////////////////////////////////////////////
//
//  TXOrderView.h
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class TXPlace;
@class TXCurrentOrder;

@protocol TXPlaceViewDelegate <NSObject>

- (void)onCellPressed:(TXPlace *)place;
- (void)onReload;

@end

@interface TXPlaceView : UIScrollView

@property(weak, nonatomic) TXCurrentOrder *currentOrder;
@property(weak, nonatomic) id<TXPlaceViewDelegate> placeViewDelegate;

- (void)reloadData;
- (void)commonInit;

@end
