//
//  TXPlaceCell.h
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TXPlace;

@interface TXPlaceCell : UIView

@property (strong, nonatomic, readonly) UIButton* main;
@property (strong, nonatomic, readonly) UILabel* error;
@property (strong, nonatomic, readonly) UIButton* cross;
@property (strong, nonatomic) TXPlace *place;
@property (nonatomic)BOOL isLast;

+ (CGFloat) HEIGHT;

@end
