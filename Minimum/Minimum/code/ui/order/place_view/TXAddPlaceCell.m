//
//  TXAddPlaceCell.m
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "TXAddPlaceCell.h"
#import "TXThinLineView.h"

@implementation TXAddPlaceCell


static CGFloat height = 44.0f;

+ (CGFloat) HEIGHT {
    return height;
}

- (void)commonInit {
    {
        TXThinLineView *line = [[TXThinLineView alloc] init];
        line.frame = CGRectMake(0.0f, 0.0f, self.frame.size.width, 1.0f);
        line.lineColor = [[UIColor alloc] initWithWhite:100.0f / 255.0f alpha:0.4f];
        line.backgroundColor = [UIColor clearColor];
        line.align = @(TSLineAlignTop);
        line.depth = 1.0f;
        [self addSubview:line];
    }
    {
        TXThinLineView *line = [[TXThinLineView alloc] init];
        line.frame = CGRectMake(0.0f, self.frame.size.height - 1.0f, self.frame.size.width, 1.0f);
        line.lineColor = [[UIColor alloc] initWithWhite:100.0f / 255.0f alpha:0.4f];
        line.backgroundColor = [UIColor clearColor];
        line.align = TSLineAlignBottom;
        line.depth = 1.0f;
        [self addSubview:line];
    }
    
//    self.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.4f];
    
    CGRect mainRect = CGRectMake(0.0f, 0.0f, self.frame.size.width, height);
    _main = [[UIButton alloc] initWithFrame:mainRect];
    _main.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.4f];
    [self addSubview:_main];
    
    CGRect crossRect = CGRectMake(self.frame.size.width - height, 0.0f, height, height);
    UIImageView *cross = [[UIImageView alloc] initWithFrame:crossRect];
    cross.image = [UIImage imageNamed:@"ico_plus"];
    [self addSubview:cross];
}

- (id)initWithFrame:(CGRect)frame {
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGRect frmRect = frame;
    frmRect.size.width = screenSize.width;
    self = [super initWithFrame:frmRect];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

@end
