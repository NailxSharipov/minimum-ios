////////////////////////////////////////////////////////////////////////////////
//
//  TXOrderView.m
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXPlaceView.h"
#import "TXPlaceCell.h"
#import "TXAddPlaceCell.h"
#import "TXCurrentOrder.h"
#import "TXPlace.h"

@implementation TXPlaceView {
    NSMutableArray *_cellList;
    TXAddPlaceCell *_addCell;
    NSMutableArray<UIButton *> *_switchButtonList;
}

- (void)commonInit {
    if (_cellList == nil) {
        _cellList = [NSMutableArray new];
        NSUInteger n = _currentOrder.placeList.count;
        CGRect screenBound = [[UIScreen mainScreen] bounds];
        CGSize screenSize = screenBound.size;
        CGFloat height = [TXPlaceCell HEIGHT];
        CGRect rect = CGRectMake(0.0f, n * height, screenSize.width, height);
        _addCell = [[TXAddPlaceCell alloc] initWithFrame:rect];
        [_addCell.main addTarget:self action:@selector(addButtonPressed:) forControlEvents:UIControlEventTouchDown];
        
        [self addSubview:_addCell];
    }
    [self reloadData];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)reloadData {
    NSUInteger dataCount = _currentOrder.placeList.count;
    
    if (dataCount > 0) {
        NSUInteger cellCount = _cellList.count;
        CGSize size = self.frame.size;
        CGFloat height = [TXPlaceCell HEIGHT];
        
        // add new cells
        {
            for(NSUInteger i = cellCount; i < dataCount; i++) {
                CGRect rect = CGRectMake(0.0f, i * height, size.width, height);
                TXPlaceCell *cell = [[TXPlaceCell alloc] initWithFrame:rect];
                [cell.cross addTarget:self action:@selector(removeButtonPressed:) forControlEvents:UIControlEventTouchDown];
                [cell.main addTarget:self action:@selector(mainButtonPressed:) forControlEvents:UIControlEventTouchDown];
                cell.main.tag = i;
                [_cellList addObject:cell];
                [self addSubview:cell];
            }
        }
        
        // move plus cell
        {
            _addCell.frame = CGRectMake(0.0f, dataCount * height, size.width, height);
        }
        
        // hide unused cells
        for(NSUInteger i = dataCount; i < cellCount; i++) {
            TXPlaceCell *cell = [_cellList objectAtIndex:i];
            cell.hidden = YES;
        }
        
        // set data
        for(NSUInteger i = 0; i < dataCount; i++) {
            TXPlaceCell *cell = _cellList[i];
            TXPlace *place = _currentOrder.placeList[i];
            [cell.main setTitle:place.description forState:UIControlStateNormal];
            cell.place = place;
            if (place.name == nil) {
                cell.error.hidden = YES;
            } else if (place.isCorect && place.isFrom && (place.definition == nil || place.definition.length == 0)) {
                cell.error.hidden = NO;
                cell.error.text = NSLocalizedString(@"set defination low", nil);
            } else if (!place.isCorect) {
                cell.error.hidden = NO;
                cell.error.text = NSLocalizedString(@"Incorrect place", nil);
            } else {
                cell.error.hidden = YES;            
            }
            
            cell.hidden = NO;
            cell.isLast = i == dataCount - 1;
        }
        
        [self setContentSize:CGSizeMake(size.width, (dataCount + 2) * height)];
       

        // add switch button
        {
            if (_switchButtonList != nil) {
                for(UIButton *button in _switchButtonList) {
                    [button removeFromSuperview];
                }
            }
            
            NSUInteger size = dataCount - 1;
            _switchButtonList = [[NSMutableArray alloc] initWithCapacity:size];
            
            CGFloat icoSize = 30.0f;
            
            UIImage *image = [UIImage imageNamed:@"ico_switch"];
            
            for(NSUInteger i = 0; i < size; i++) {
                CGRect rect = CGRectMake(0.0f, (i + 1) * height - 0.5f * icoSize, icoSize, icoSize);
                UIButton *button = [[UIButton alloc] initWithFrame:rect];
                [button setBackgroundImage:image forState:UIControlStateNormal];
                [button addTarget:self action:@selector(switchButtonPressed:) forControlEvents:UIControlEventTouchDown];
                button.tag = i;
                _switchButtonList[i] = button;
                [self addSubview:button];
            }
        }
        
        
        // update
        [_placeViewDelegate onReload];
    }
}

- (void)switchButtonPressed:(UIButton*)sender {
    [_currentOrder switchPlace: sender.tag];
    [self reloadData];
}

- (void)removeButtonPressed:(UIButton*)sender {
    TXPlaceCell *cell = (TXPlaceCell*)[sender superview];
    [_currentOrder removePlace:cell.place];
    [self reloadData];
}

- (void)addButtonPressed:(UIButton*)sender {
    [_currentOrder addPlace];
    [self reloadData];
}

- (void)mainButtonPressed:(UIButton*)sender {
    TXPlace *place = _currentOrder.placeList[sender.tag];
    [_placeViewDelegate onCellPressed:place];
}


@end
