////////////////////////////////////////////////////////////////////////////////
//
//  OrderCell.m
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXPlaceCell.h"
#import "TXThinLineView.h"


@implementation TXPlaceCell {
    TXThinLineView *_bottomBorder;
}

const CGFloat leftMargin = 32.0f;
const CGFloat height = 44.0f;

+ (CGFloat) HEIGHT {
    return height;
}

- (void)commonInit {
    _bottomBorder = [[TXThinLineView alloc] init];
    _bottomBorder.frame = CGRectMake(leftMargin, self.frame.size.height - 1.0f, self.frame.size.width, 1.0f);
    _bottomBorder.lineColor = [[UIColor alloc] initWithWhite:0.4f alpha:0.4f];
    _bottomBorder.backgroundColor = [UIColor clearColor];
    _bottomBorder.align = TSLineAlignBottom;
    _bottomBorder.depth = 1.0f;
    [self addSubview:_bottomBorder];
    
    self.backgroundColor = [UIColor colorWithWhite:1.0f alpha:1.0f];
    
    CGFloat x = leftMargin;
    CGFloat mainWidth = self.frame.size.width - leftMargin - height;
    CGRect mainRect = CGRectMake(x, 0.0f, mainWidth, height);
    _main = [[UIButton alloc] initWithFrame:mainRect];
    _main.titleLabel.font = [UIFont systemFontOfSize:15];
    _main.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_main setTitleColor:[UIColor darkTextColor] forState: UIControlStateNormal];
    [self addSubview:_main];
    
    CGRect errorRect = CGRectMake(x, height - 12.0f, mainWidth, 12.0f);
    _error = [[UILabel alloc] initWithFrame:errorRect];
    _error.font = [UIFont systemFontOfSize:8];
    _error.textColor = [UIColor redColor];
    [self addSubview:_error];
    
    x = x + mainWidth;
    CGRect crossRect = CGRectMake(x, 0.0f, height, height);
    _cross = [[UIButton alloc] initWithFrame:crossRect];
    [_cross setBackgroundImage:[UIImage imageNamed:@"ico_cross"] forState:UIControlStateNormal];
    [self addSubview:_cross];
}

- (id)initWithFrame:(CGRect)frame {
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGRect frmRect = frame;
    frmRect.size.width = screenSize.width;
    self = [super initWithFrame:frmRect];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)setIsLast:(BOOL)isLast {
    _isLast = isLast;
    _bottomBorder.hidden = isLast;
}

@end
