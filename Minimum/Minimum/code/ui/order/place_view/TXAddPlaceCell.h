//
//  TXAddPlaceCell.h
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TXAddPlaceCell : UIView

@property (strong, nonatomic, readonly) UIButton* main;

+ (CGFloat) HEIGHT;

@end
