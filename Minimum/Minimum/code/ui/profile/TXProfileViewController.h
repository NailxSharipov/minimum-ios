////////////////////////////////////////////////////////////////////////////////
//
//  TXProfileViewController.h
//  Minimum
//
//  Created by Nail Sharipov on 09/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import "TXPhoneNumberField.h"

@protocol TXUserDAO;
@protocol TXCityDAO;
@protocol TXTaskExecutor;
@class TXChangePasswordTask;

@interface TXProfileViewController : UIViewController

@property UIViewController *main;
@property (strong, nonatomic) id<TXUserDAO> userDAO;
@property (strong, nonatomic) id<TXCityDAO> cityDAO;
@property (strong, nonatomic) id<TXTaskExecutor> taskExecutor;
@property (strong, nonatomic) TXChangePasswordTask *changePasswordTask;
@property (weak, nonatomic) IBOutlet TXPhoneNumberField *notifyPhonePhield;

@end
