////////////////////////////////////////////////////////////////////////////////
//
//  TXProfileViewController.m
//  Minimum
//
//  Created by Nail Sharipov on 09/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXProfileViewController.h"
#import "TXColorPallete.h"
#import "TXTaskExecutor.h"
#import "TXUserDAO.h"
#import "TXCityDAO.h"
#import "TXChangePasswordTask.h"
#import "TXPhoneNumberField.h"

@interface TXProfileViewController () <TXChangePasswordTaskDelegate>

@property (weak, nonatomic) IBOutlet UILabel *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *changePasswordField;
@property (weak, nonatomic) IBOutlet UIView *sheet;
@property (weak, nonatomic) IBOutlet UINavigationItem *tabbarTitle;
@property (weak, nonatomic) IBOutlet UIButton *exitButton;

@end

@implementation TXProfileViewController

//-------------------------------------------------------------------------------------------
#pragma mark - <TXChangePasswordTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onChangePasswordTaskSuccess {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)onChangePasswordTaskError:(NSString *)error {
    [self showAlert:error];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Override Methods
//-------------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [TXColorPallete orangeColor];
    _sheet.backgroundColor = [TXColorPallete sheetColor];
    _tabbarTitle.title = NSLocalizedString(@"Profile", nil);
    CGRect btnRect = _exitButton.frame;
    CGRect imgRect = _exitButton.imageView.frame;
    _exitButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, imgRect.size.width);
    _exitButton.imageEdgeInsets = UIEdgeInsetsMake(0, btnRect.size.width - imgRect.size.width, 0, 0);
    [_exitButton setTitle:NSLocalizedString(@"Exit", nil) forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    TXUser *user = _userDAO.actualUser;

    if (user != nil) {
        TXCity *city = _cityDAO.actualCity;
        NSString *prefix = city.phonePrefix;
        _notifyPhonePhield.prefix = prefix;
        _notifyPhonePhield.unformatText = user.secondPhone;
        _phoneField.text = [TXPhoneNumberField getTextFormat:user.phone prefix:prefix];
    }
}

- (IBAction)exitButtonPressed:(id)sender {
    [_userDAO deletePassword];
    [_main dismissViewControllerAnimated:NO completion:nil];
    [_main dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButtonPressed:(id)sender {
    NSString *secondPhone = _notifyPhonePhield.unformatText;
    if (secondPhone != nil && secondPhone.length == 10) {
        _userDAO.actualUser.secondPhone = secondPhone;
    } else {
        _userDAO.actualUser.secondPhone = nil;
        _notifyPhonePhield.unformatText = nil;
    }
    [_userDAO save];
    
    NSString *password = _changePasswordField.text;
    if (password != nil && password.length > 0) {
        _changePasswordTask.delegate = self;
        _changePasswordTask.password = password;
        [_taskExecutor executeTask:_changePasswordTask];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void) showAlert:(NSString *)alert {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:alert
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
