//
//  TXColorPallete.h
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TXColorPallete : NSObject

+ (UIColor *)navigationBarColor;
+ (UIColor *)navigationBarTitleColor;
+ (UIColor *)orangeColor;
+ (UIColor *)sheetColor;
+ (UIColor *)sideMenuColor;
+ (UIColor *)sideMenuCellColor;
+ (UIColor *)sideMenuBottomColor;
+ (UIColor *)sideMenuSelectColor;

@end
