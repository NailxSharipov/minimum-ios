////////////////////////////////////////////////////////////////////////////////
//
//  TXReadHistoryTask.m
//  Minimum
//
//  Created by Nail Sharipov on 14/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXQueryHistoryTask.h"
#import "TXOrderDAO.h"
#import "FMDB.h"

@implementation TXQueryHistoryTask

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    [_orderDAO.databaseManager runInDatabase:^{
        NSArray<TXOrder *> *orderList = nil;
        if (_dateTimeStamp == 0) {
            orderList = [_orderDAO queryFromIndex:_index size:_size];
        } else {
            orderList = [_orderDAO queryFromDate:_dateTimeStamp];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [_delegate onQueryHistoryTaskSuccess:orderList];
        });
    }];
}

@end
