////////////////////////////////////////////////////////////////////////////////
//
//  TXQuerryPlaceByKeyTask.m
//  Minimum
//
//  Created by Nail Sharipov on 25/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXQuerryPlaceByKeyTask.h"
#import "TXPlaceDAO.h"
#import "TXServer.h"
#import "TXUserDAO.h"
#import "TXUrlEditor.h"
#import "NSString+Encode.h"
#import "FMDB.h"

@implementation TXQuerryPlaceByKeyTask {
    NSURLSessionTask *_httpTask;
    BOOL _canceled;
}


//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    [self cancel];
    _canceled = NO;
    _httpTask = nil;
    [_placeDAO.databaseManager runInDatabase:^{
        NSArray<TXPlace *> *placeList = nil;
        if (_objectType == nil) {
            placeList = [_placeDAO queryByKey:_key];
        } else {
            TXPlace *place = [_placeDAO queryByKey:_key andObjectType:_objectType];
            if (place != nil) {
                if (_number != nil) {
                    place.home = _number;
                }
                placeList = @[place];
                [_delegate onQuerryPlaceByKeyTaskSuccess:placeList];
                return; // no need result from server
            }
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            if (_canceled == NO) {
                [_delegate onQuerryPlaceByKeyTaskSuccess:placeList];
            }
        });
        
        NSString *phone = _userDAO.actualUser.phone;
        NSString *password = [_userDAO getPassword];
        
        if (phone != nil && phone.length > 0 && password != nil && password.length > 0) {
            TXUrlEditor *urlEditor = [[TXUrlEditor alloc] initWithServerURL: _server.rootUrl];
            
            [urlEditor addParameter:@"t" Value:@"6"];
            [urlEditor addParameter:@"l" Value:phone];
            [urlEditor addParameter:@"p" Value:password];
            [urlEditor addParameter:@"w" Value:_key];
            [urlEditor addParameter:@"pg" Value:@"1"];
            [urlEditor addParameter:@"lm" Value:@"20"];
            
            if (_objectType != nil) {
                [urlEditor addParameter:@"f" Value:[NSString stringWithFormat:@"%@", _objectType.stringValue]];
            }
            
            NSURL *url = [urlEditor getURL];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            request.HTTPMethod = @"GET";
            _request = request;
            
            [_server executeGetTask:self];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_delegate onQuerryPlaceByKeyTaskError:NSLocalizedString(@"User login or password not set", nil)];
                [_delegate onQuerryPlaceByKeyTaskStopServer];
            });
        }
        

    }];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXServerTask>
//-------------------------------------------------------------------------------------------

- (void)setHttpTask:(NSURLSessionTask *)task {
    _httpTask = task;
}

- (void)onSuccessWithResponse:(NSHTTPURLResponse *)response andData:(NSData *)data {
    NSInteger code = response.statusCode;
    if (code >= 200 & code < 300) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding];
        if (text != nil) {
            if (text.isHtml == NO) {
                if (_canceled == NO) {
                    [_placeDAO.databaseManager runInTransaction:^(BOOL *rollBack) {
                        NSArray<TXPlace *> *placeList = [self parsePlaceList:text];
                        if (placeList != nil && _objectType != nil) {
                            for (TXPlace *p in placeList) {
                                p.key = [_key lowercaseString];
                            }
                        }
                        
                        [_placeDAO saveList:placeList];
                        
                        NSArray<TXPlace *> *repeatPlaceList = nil;
                        if (_objectType == nil) {
                            repeatPlaceList = [_placeDAO queryByKey:_key];
                        } else {
                            TXPlace *place = [_placeDAO queryByKey:_key andObjectType:_objectType];
                            if (place != nil) {
                                if (_number == nil) {
                                    place.home = _number;
                                }
                                repeatPlaceList = @[place];
                            }
                        }
                        
                        if (_objectType != nil) {
                            [_delegate onQuerryPlaceByKeyTaskSuccess:repeatPlaceList];                        
                        } else {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (_canceled == NO) {
                                    [_delegate onQuerryPlaceByKeyTaskSuccess:repeatPlaceList];
                                }
                            });
                        }
                    }];
                }
            } else {
                [self onQuerryPlaceByKeyTaskError:NSLocalizedString(@"Could not connect to server", nil)];
            }
        } else {
            [self onQuerryPlaceByKeyTaskError:NSLocalizedString(@"Incorrect result from server", nil)];
        }
    } else {
        [self onQuerryPlaceByKeyTaskError:NSLocalizedString(@"Bad code result from server", nil)];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [_delegate onQuerryPlaceByKeyTaskStopServer];
    });
}

- (void)onFailureWithError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [_delegate onQuerryPlaceByKeyTaskStopServer];
    });
}

- (void)cancel {
    _canceled = YES;
    if (_httpTask != nil) {
        [_httpTask cancel];
        dispatch_async(dispatch_get_main_queue(), ^{
            [_delegate onQuerryPlaceByKeyTaskStopServer];
        });
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Method
//-------------------------------------------------------------------------------------------

- (void)onQuerryPlaceByKeyTaskError:(NSString *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [_delegate onQuerryPlaceByKeyTaskError:error];
    });
    
}

- (NSArray<TXPlace *> *)parsePlaceList:(NSString *) text {
    NSMutableArray *placeList = [NSMutableArray new];
    
    NSUInteger n = [text length];
    
    unichar separator = '\n';
    
    NSUInteger start = 0;
    NSUInteger end;
    
    for (NSUInteger i = 0; i < n; i++) {
        if (i == n - 1 || [text characterAtIndex: i] == separator) {
            end = i;
            NSString *block = [text substringWithRange: NSMakeRange (start, end - start + 1)];
            start = end + 1;
            
            if ([block length] > 2) {
                TXPlace *place = [self parsePlace: block];
                if (place != nil) {
                    [placeList addObject:place];
                }
            }
        }
    }
    
    return placeList;
}

- (TXPlace *)parsePlace:(NSString *)text {
    NSUInteger n = [text length];
    
    unichar openBracket = '[';
    unichar closeBracket = ']';
    
    BOOL isOpen = NO;
    NSUInteger start = 0;
    NSUInteger end;
    
    
    for (NSUInteger i = 0; i < n; i++) {
        unichar current = [text characterAtIndex: i];
        if (isOpen) {
            if (current == openBracket) {
                start = i + 1;
                isOpen = NO;
            }
        } else {
            if (current == closeBracket) {
                end = i;
                isOpen = YES;
                
                NSString *block = [text substringWithRange: NSMakeRange (start, end - start)];
                if ([block length] > 2) {
                    NSArray *data =[block componentsSeparatedByString:@"|"];
                    TXPlace *place = [TXPlace new];
                    place.placeId = [data objectAtIndex:1];
                    place.type = [data objectAtIndex:2];
                    place.name = [data objectAtIndex:3];
                    return place;
                }
            }
        }
    }
    
    return nil;
}

@end
