////////////////////////////////////////////////////////////////////////////////
//
//  TXReadHistoryTask.h
//  Minimum
//
//  Created by Nail Sharipov on 14/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

#import "TXTask.h"

@class TXOrder;
@protocol TXOrderDAO;

@protocol TXQueryHistoryTaskDelegate <NSObject>

@required

- (void)onQueryHistoryTaskSuccess:(NSArray<TXOrder *> *)history;
- (void)onQueryHistoryTaskError:(NSString *)error;

@end

@interface TXQueryHistoryTask : NSObject<TXTask>

@property (weak, nonatomic) id<TXOrderDAO> orderDAO;
@property (weak, nonatomic) id<TXQueryHistoryTaskDelegate> delegate;

@property (nonatomic) NSUInteger index;
@property (nonatomic) NSUInteger size;
@property (nonatomic) NSUInteger dateTimeStamp;

@end
