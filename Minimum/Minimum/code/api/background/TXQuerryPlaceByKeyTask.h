////////////////////////////////////////////////////////////////////////////////
//
//  TXQuerryPlaceByKeyTask.h
//  Minimum
//
//  Created by Nail Sharipov on 25/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

#import "TXTask.h"
#import "TXServerTask.h"

@class TXPlace;

@protocol TXServer;
@protocol TXUserDAO;
@protocol TXPlaceDAO;

@protocol TXQuerryPlaceByKeyDelegate <NSObject>

@required

- (void)onQuerryPlaceByKeyTaskSuccess:(NSArray<TXPlace *> *)placeList;
- (void)onQuerryPlaceByKeyTaskStartServer;
- (void)onQuerryPlaceByKeyTaskStopServer;
- (void)onQuerryPlaceByKeyTaskError:(NSString *)error;

@end

@interface TXQuerryPlaceByKeyTask : NSObject<TXTask, TXServerTask>

@property (strong, nonatomic) id<TXServer> server;
@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) id<TXUserDAO> userDAO;
@property (weak, nonatomic) id<TXPlaceDAO> placeDAO;
@property (weak, nonatomic) id<TXQuerryPlaceByKeyDelegate> delegate;
@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSNumber *objectType;
@property (strong, nonatomic) NSString *number;

@end
