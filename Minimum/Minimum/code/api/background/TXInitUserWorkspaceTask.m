////////////////////////////////////////////////////////////////////////////////
//
//  TXInitUserWorkspaceTask.m
//  Minimum
//
//  Created by Nail Sharipov on 09/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXInitUserWorkspaceTask.h"
#import "TXCurrentOrder.h"
#import "SDDatabaseManager.h"
#import "TXServer.h"
#import "TXUserDAO.h"
#import "TXCityDAO.h"

@implementation TXInitUserWorkspaceTask {

}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    TXUser *user =_userDAO.actualUser;
    NSString *password = [_userDAO getPassword];
    TXCity *city =_cityDAO.actualCity;
    if (city != nil) {
        _server.domain = city.url;
        [_currentOrder clear];
    }
    if (user != nil && password != nil && password.length > 0 && city != nil) {
        [_databaseManager openDatabase:[self databaseNameForUser:user city:city]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [_delegate onInitUserWorkspaceTaskSuccess];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_delegate onInitUserWorkspaceTaskError:nil];
        });
    }
}


//-------------------------------------------------------------------------------------------
#pragma mark - Private Method
//-------------------------------------------------------------------------------------------

- (NSString *)databaseNameForUser:(TXUser *)user city:(TXCity *)city {
    return [NSString stringWithFormat:@"%@_%@", city.cityId, user.phone];
}


@end
