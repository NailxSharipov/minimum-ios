////////////////////////////////////////////////////////////////////////////////
//
//  TXInitUserWorkspaceTask.h
//  Minimum
//
//  Created by Nail Sharipov on 09/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

#import "TXTask.h"

@class SDDatabaseManager;
@class TXCurrentOrder;
@protocol TXUserDAO;
@protocol TXCityDAO;
@protocol TXServer;


@protocol TXInitUserWorkspaceTaskDelegate <NSObject>

@required

- (void)onInitUserWorkspaceTaskSuccess;
- (void)onInitUserWorkspaceTaskError:(NSString *)error;


@end

@interface TXInitUserWorkspaceTask : NSObject<TXTask>

@property(strong, nonatomic) id<TXUserDAO> userDAO;
@property(strong, nonatomic) id<TXCityDAO> cityDAO;
@property(strong, nonatomic) id<TXServer> server;
@property(strong, nonatomic) SDDatabaseManager *databaseManager;
@property (strong, nonatomic) TXCurrentOrder *currentOrder;
@property (weak, nonatomic) id<TXInitUserWorkspaceTaskDelegate> delegate;

@end
