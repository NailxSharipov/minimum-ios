////////////////////////////////////////////////////////////////////////////////
//
//  TXQueryDetailOrderTask.m
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXQueryDetailOrderTask.h"

#import "TXServer.h"
#import "TXUserDAO.h"
#import "TXDetailOrderDAO.h"
#import "TXUrlEditor.h"
#import "NSString+Encode.h"

@implementation TXQueryDetailOrderTask {
    NSURLSessionTask *_httpTask;
    NSUInteger _loadHistorySyncDateAnchor;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    NSString *phone = _userDAO.actualUser.phone;
    NSString *password = [_userDAO getPassword];
    [self cancel];
    
    if (phone != nil && phone.length > 0 && password != nil && password.length > 0) {
        TXUrlEditor *urlEditor = [[TXUrlEditor alloc] initWithServerURL: _server.rootUrl];
        
        [urlEditor addParameter:@"t" Value:@"42"];
        [urlEditor addParameter:@"l" Value:phone];
        [urlEditor addParameter:@"p" Value:password];
//        [urlEditor addParameter:@"version" Value:@"4.0.3"];
        [urlEditor addParameter:@"version" Value:@"4.1.7"];

        
        NSURL *url = [urlEditor getURL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        request.HTTPMethod = @"GET";
        _request = request;
        
        [_server executeGetTask:self];
    } else {
        [self onTaskError:NSLocalizedString(@"User login or password not set", nil)];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXServerTask>
//-------------------------------------------------------------------------------------------

- (void)setHttpTask:(NSURLSessionTask *)task {
    _httpTask = task;
}

// TODO добавить описание ошибок
- (void)onSuccessWithResponse:(NSHTTPURLResponse *)response andData:(NSData *)data {
    
    NSInteger code = response.statusCode;
    if (code >= 200 & code < 300) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding];
        NSLog(@"%@", text);
/*
    NSString *text = @"result=2\nid=60348636|from=Ленина 1 подъезд 2|to=Сагита Агиша 34ffwerw|price=105|status=Клиент не поехал серебристая/дэу-нексия/913|driverphone=|status_code=102\nid=60348633|from=Ленина 2 подъезд 2|to=Сагита Агиша 34!Пархоменко 7!Цюрупе 13|price=105|status=Клиент не поехал серебристая/дэу-нексия/943|driverphone=+79872502667|status_code=2\nid=60343|from=Ленина 1 подъезд 22|to=Сагита Агиша 14!Пархоменко 7!Цюрупе 113|price=125|status=Клиент приехал серебристая/дэу-нексия/234|driverphone=+79872503667|status_code=1";
*/
        if (text != nil) {
            if (text.isHtml == NO) {
                [_detailOrderDAO.databaseManager runInTransaction:^(BOOL *rollback) {
                    NSArray<TXDetailOrder *> *detailOrderList = [self parseDetailOrderList:text];
                    NSDate *now = [NSDate date];
                    NSUInteger nowTimeStamp = [SDSQLiteTypeConverter dateToTimeStamp:now];
                    nowTimeStamp = nowTimeStamp - 3600;
                    [_detailOrderDAO removeAllWithDateYongerThen:nowTimeStamp];
                    [_detailOrderDAO saveList:detailOrderList];
                    detailOrderList = [_detailOrderDAO queryAll];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_delegate onQueryDetailOrderTaskSuccess:detailOrderList];
                    });
                }];
            } else {
                [self onTaskError:NSLocalizedString(@"Could not connect to server", nil)];
            }
        } else {
            [self onTaskError:NSLocalizedString(@"Incorrect result from server", nil)];
        }
    } else {
        [self onTaskError:NSLocalizedString(@"Bad code result from server", nil)];
    }
}

- (void)onFailureWithError:(NSError *)error {
    [self onTaskError:[error localizedDescription]];
}

- (void)cancel {
    if (_httpTask != nil) {
        [_httpTask cancel];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Method
//-------------------------------------------------------------------------------------------

- (void)onTaskError:(NSString *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_delegate != nil) {
            [_delegate onQueryDetailOrderTaskError:error];
        }
    });
}

- (NSArray<TXDetailOrder *> *)parseDetailOrderList:(NSString *)text {
    NSArray *lines = [text componentsSeparatedByString:@"\n"];
    NSMutableArray<TXDetailOrder *> *detailOrderList = [NSMutableArray new];
    if (lines != nil && lines.count > 1) {
        for (NSUInteger i = 1; i < lines.count; i++)  {
            NSString *line = lines[i];
            if (line != nil && line.length > 10) {
                [detailOrderList addObject:[self getDetailOrder:line]];
            }
        }
    }
    
    return detailOrderList;
}

- (TXDetailOrder *)getDetailOrder:(NSString *) text {
    TXDetailOrder *detailOrder = [TXDetailOrder new];
    
    NSArray *block = [text componentsSeparatedByString:@"|"];
    NSDictionary *dict = [self collectData:block];
    
    detailOrder.orderId = dict[@"id"];
    NSMutableArray<NSString *> *placeList = [NSMutableArray new];
    [placeList addObject: dict[@"from"]];
    NSString *toPlaceText = dict[@"to"];
    NSArray<NSString *> *toPlaceList = [toPlaceText componentsSeparatedByString:@"%"];
    [placeList addObjectsFromArray:toPlaceList];
    detailOrder.placeList = placeList;
    
    detailOrder.price = dict[@"price"];
    detailOrder.phone = dict[@"driverphone"];
    
    NSString *status = dict[@"status"];
    
    NSArray *statusBlock = [status componentsSeparatedByString:@"/"];
    
    NSUInteger count = statusBlock.count;
    if (count > 0) {
        detailOrder.status = statusBlock[0];
    }
    if (count > 1) {
        detailOrder.car = statusBlock[1];
    }
    if (count > 2) {
        detailOrder.carNumber = statusBlock[2];
    }
    
    NSString *code = dict[@"status_code"];
    
    if (code != nil && code.length > 0) {
        NSInteger codeInt = [code integerValue];
        if (codeInt == 1) {
            detailOrder.status = NSLocalizedString(@"status_1", nil);
        } else if (codeInt == 2) {
            detailOrder.status = NSLocalizedString(@"status_2", nil);
        } else if (codeInt == 3) {
            detailOrder.status = NSLocalizedString(@"status_3", nil);
        } else if (codeInt == 4) {
            detailOrder.status = NSLocalizedString(@"status_4", nil);
        } else if (codeInt == 5) {
            detailOrder.status = NSLocalizedString(@"status_5", nil);
        } else if (codeInt == 6) {
            detailOrder.status = NSLocalizedString(@"status_6", nil);
        } else if (codeInt == 102) {
            detailOrder.status = NSLocalizedString(@"status_102", nil);
        } else if (codeInt == 103) {
            detailOrder.status = NSLocalizedString(@"status_103", nil);
        } else if (codeInt == 104) {
            detailOrder.status = NSLocalizedString(@"status_104", nil);
        }
        
        if (codeInt == 3 || codeInt == 4) {
            NSArray *statusBlockBlock = [statusBlock[0] componentsSeparatedByString:@" "];
            NSUInteger size = statusBlockBlock.count;
            if (size > 0) {
                NSString *color = statusBlockBlock[size - 1];
                if (color != nil && color.length > 0) {
                    detailOrder.car = [NSString stringWithFormat:@"%@ %@", color, detailOrder.car];
                }
            }
        }
    }
    
    detailOrder.code = [code integerValue];
    
    return detailOrder;
}

- (NSDictionary *)collectData: (NSArray *)data {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    for (NSString *block in data) {
        NSArray *sep = [block componentsSeparatedByString:@"="];
        if ([sep count] > 1) {
            [dict setObject:[sep objectAtIndex:1] forKey:[sep objectAtIndex:0]];
        }
    }
    return dict;
}

@end
