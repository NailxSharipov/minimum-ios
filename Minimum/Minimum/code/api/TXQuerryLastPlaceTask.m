////////////////////////////////////////////////////////////////////////////////
//
//  TXQuerryLastPlaceTask.m
//  Minimum
//
//  Created by Nail Sharipov on 24/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXQuerryLastPlaceTask.h"
#import "TXPlaceDAO.h"
#import "FMDB.h"

@implementation TXQuerryLastPlaceTask

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    [_placeDAO.databaseManager runInDatabase:^{
        NSArray<TXPlace *> *placeList = [_placeDAO queryLast];
        dispatch_async(dispatch_get_main_queue(), ^{
            [_delegate onQuerryLastPlaceTaskSuccess:placeList];
        });
    }];
}

- (void)cancel {

}


@end
