////////////////////////////////////////////////////////////////////////////////
//
//  TXRemindPasswordTask.m
//  Minimum
//
//  Created by Nail Sharipov on 03/02/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////


#import "TXServer.h"
#import "TXUrlEditor.h"

#import "NSString+Encode.h"
#import "TXRemindPasswordTask.h"

@implementation TXRemindPasswordTask {
    NSURLSessionTask *_httpTask;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    TXUrlEditor *urlEditor = [[TXUrlEditor alloc] initWithServerURL: _server.rootUrl];
    
    [urlEditor addParameter:@"t" Value:@"13"];
    [urlEditor addParameter:@"version" Value:@"2"];
    [urlEditor addParameter:@"l" Value:_phone];
    
    NSURL *url = [urlEditor getURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    _request = request;
    
    [_server executeGetTask:self];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXServerTask>
//-------------------------------------------------------------------------------------------

- (void)setHttpTask:(NSURLSessionTask *)task {
    _httpTask = task;
}

- (void)onSuccessWithResponse:(NSHTTPURLResponse *)response andData:(NSData *)data {
    NSInteger code = response.statusCode;
    if (code >= 200 & code < 300) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding];
        if (text != nil) {
            if (text.isHtml == NO) {
                NSString *error = [self parseRemindPassword:text];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (error == nil) {
                        [_delegate onRemindPasswordTaskError:error];
                    }
                });
            } else {
                [self onRemindPasswordTaskError:NSLocalizedString(@"Could not connect to server", nil)];
            }
        } else {
            [self onRemindPasswordTaskError:NSLocalizedString(@"Incorrect result from server", nil)];
        }
    } else {
        [self onRemindPasswordTaskError:NSLocalizedString(@"Bad code result from server", nil)];
    }
}

- (void)onFailureWithError:(NSError *)error {
    [self onRemindPasswordTaskError:[error localizedDescription]];
}

- (void)cancel {
    if (_httpTask != nil) {
        [_httpTask cancel];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Method
//-------------------------------------------------------------------------------------------

- (void)onRemindPasswordTaskError:(NSString *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_delegate != nil) { // check again couse it's othere thread!!!
            [_delegate onRemindPasswordTaskError:error];
        }
    });
    
}

- (NSString *)parseRemindPassword: (NSString *) text {
    NSString *message = nil;
    NSUInteger length = [text length];
    if (length > 2) {
        message = [text substringWithRange: NSMakeRange (2, length - 2)];
    }
    return message;
}

@end
