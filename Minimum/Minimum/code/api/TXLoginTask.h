////////////////////////////////////////////////////////////////////////////////
//
//  TXLoginTask.h
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

#import "TXTask.h"
#import "TXServerTask.h"

@class SDDatabase;
@protocol TXServer;
@protocol TXUserDAO;
@protocol TXCityDAO;

@protocol TXLoginTaskDelegate <NSObject>

@required

- (void)onLoginTaskSuccess;
- (void)onLoginTaskError:(NSString *)error;


@end

@interface TXLoginTask : NSObject<TXTask, TXServerTask>

@property (strong, nonatomic) id<TXServer> server;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) id<TXUserDAO> userDAO;
@property (weak, nonatomic) id<TXLoginTaskDelegate> delegate;

@end
