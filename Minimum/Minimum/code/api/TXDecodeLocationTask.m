////////////////////////////////////////////////////////////////////////////////
//
//  TXDecodeLocationTask.m
//  Minimum
//
//  Created by Nail Sharipov on 30/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXDecodeLocationTask.h"
#import "TXTaskExecutor.h"
#import "TXGoogleGeoLocationTask.h"
#import "TXQuerryPlaceByKeyTask.h"
#import "TXOSMGeoLocationTask.h"
#import "TXLocation.h"
#import "TXPlace.h"

@interface TXDecodeLocationTask () <TXGoogleGeoLocationTaskDelegate, TXOSMGeoLocationTaskDelegate, TXQuerryPlaceByKeyDelegate>

@end


@implementation TXDecodeLocationTask {
    TXLocation *_lastLocation;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXGoogleGeoLocationTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onGoogleGeoLocationTaskSuccess:(TXLocation *)location {
    [self findInOurDataBase:location];
}

- (void)onGoogleGeoLocationTaskFailure {
    [_delegate onDecodeLocationTaskError:nil];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXOSMGeoLocationTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onOSMGeoLocationTaskSuccess:(TXLocation *)location {
    [self findInOurDataBase:location];
}

- (void)onOSMGeoLocationTaskFailure {
    _googleTask.delegate = self;
    _googleTask.latitude = _latitude;
    _googleTask.longitude = _longitude;
    [_taskExecutor executeTask:_googleTask];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXQuerryPlaceByKeyDelegate>
//-------------------------------------------------------------------------------------------

- (void)onQuerryPlaceByKeyTaskSuccess:(NSArray<TXPlace *> *)placeList {
    if (placeList != nil && placeList.count > 0) {
        TXPlace *place = placeList.firstObject;
        place.home = _lastLocation.number;
        dispatch_async(dispatch_get_main_queue(), ^{
            [_delegate onDecodeLocationTaskSuccess:place];
        });
    }
}

- (void)onQuerryPlaceByKeyTaskStartServer {

}

- (void)onQuerryPlaceByKeyTaskStopServer {

}

- (void)onQuerryPlaceByKeyTaskError:(NSString *)error {

}


//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    _osmTask.delegate = self;
    _osmTask.latitude = _latitude;
    _osmTask.longitude = _longitude;
    [_taskExecutor executeTask:_osmTask];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXServerTask>
//-------------------------------------------------------------------------------------------


- (void)onFailureWithError:(NSError *)error {
    [self onTaskError:[error localizedDescription]];
}

- (void)cancel {
    [_googleTask cancel];
    [_osmTask cancel];
    [_querryPlaceByKeyTask cancel];
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Method
//-------------------------------------------------------------------------------------------

- (void)onTaskError:(NSString *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_delegate != nil) { // check again couse it's othere thread!!!
            [_delegate onDecodeLocationTaskError:error];
        }
    });
}

- (void)findInOurDataBase:(TXLocation *)location {
    _lastLocation = location;
    _querryPlaceByKeyTask.delegate = self;
    _querryPlaceByKeyTask.objectType = @(1);
    _querryPlaceByKeyTask.number = location.number;
    _querryPlaceByKeyTask.key = location.streetName;
    [_taskExecutor executeTask:_querryPlaceByKeyTask];
    
}



@end
