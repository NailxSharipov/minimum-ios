//
//  TXCancelOrderTask.h
//  Minimum
//
//  Created by Nail Sharipov on 01/02/16.
//  Copyright © 2016 Tensor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TXTask.h"
#import "TXServerTask.h"

@protocol TXServer;
@protocol TXUserDAO;
@protocol TXDetailOrderDAO;
@class TXDetailOrder;

@protocol TXCancelOrderTaskDelegate <NSObject>

@required

- (void)onCancelOrderTaskSuccess;
- (void)onCancelOrderTaskError:(NSString *)error;

@end

@interface TXCancelOrderTask : NSObject<TXTask, TXServerTask>

@property (strong, nonatomic) id<TXServer> server;
@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) id<TXUserDAO> userDAO;
@property (strong, nonatomic) id<TXDetailOrderDAO> detailOrderDAO;
@property (strong, nonatomic) TXDetailOrder *detailOrder;
@property (weak, nonatomic) id<TXCancelOrderTaskDelegate> delegate;

@end
