////////////////////////////////////////////////////////////////////////////////
//
//  TXRemindPasswordTask.h
//  Minimum
//
//  Created by Nail Sharipov on 03/02/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

#import "TXTask.h"
#import "TXServerTask.h"

@protocol TXServer;

@protocol TXRemindPasswordTaskDelegate <NSObject>

@required

- (void)onRemindPasswordTaskError:(NSString *)error;

@end

@interface TXRemindPasswordTask : NSObject<TXTask, TXServerTask>

@property (strong, nonatomic) id<TXServer> server;
@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) NSString *phone;
@property (weak, nonatomic) id<TXRemindPasswordTaskDelegate> delegate;

@end
