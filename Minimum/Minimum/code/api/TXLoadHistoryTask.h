////////////////////////////////////////////////////////////////////////////////
//
//  TXLoadHistoryTask.h
//  Minimum
//
//  Created by Nail Sharipov on 11/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

#import "TXTask.h"
#import "TXServerTask.h"


@protocol TXServer;
@protocol TXUserDAO;
@protocol TXOrderDAO;

@protocol TXLoadHistoryDelegate <NSObject>

@required

- (void)onLoadHistoryTaskSuccess:(NSUInteger)lastDateTimeStamp;
- (void)onLoadHistoryTaskError:(NSString *)error;

@end

@interface TXLoadHistoryTask : NSObject<TXTask, TXServerTask>

@property (strong, nonatomic) id<TXServer> server;
@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) id<TXUserDAO> userDAO;
@property (strong, nonatomic) id<TXOrderDAO> orderDAO;
@property (weak, nonatomic) id<TXLoadHistoryDelegate> delegate;
@property (nonatomic) NSUInteger index;
@property (nonatomic) NSUInteger size;

@end
