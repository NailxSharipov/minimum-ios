////////////////////////////////////////////////////////////////////////////////
//
//  TXQuerryLastPlaceTask.h
//  Minimum
//
//  Created by Nail Sharipov on 24/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXTask.h"

@class TXPlace;

@protocol TXPlaceDAO;

@protocol TXQuerryLastPlaceTaskDelegate <NSObject>

@required

- (void)onQuerryLastPlaceTaskSuccess:(NSArray<TXPlace *> *)placeList;
- (void)onQuerryLastPlaceTaskError:(NSString *)error;

@end

@interface TXQuerryLastPlaceTask : NSObject<TXTask>

@property (weak, nonatomic) id<TXPlaceDAO> placeDAO;
@property (weak, nonatomic) id<TXQuerryLastPlaceTaskDelegate> delegate;

@end
