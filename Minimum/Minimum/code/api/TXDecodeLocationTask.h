////////////////////////////////////////////////////////////////////////////////
//
//  TXDecodeLocationTask.h
//  Minimum
//
//  Created by Nail Sharipov on 30/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXTask.h"
#import "TXServerTask.h"

@protocol TXServer;
@protocol TXTaskExecutor;

@class TXGoogleGeoLocationTask;
@class TXOSMGeoLocationTask;
@class TXQuerryPlaceByKeyTask;
@class TXPlace;

@protocol TXDecodeLocationTaskDelegate <NSObject>

@required

- (void)onDecodeLocationTaskSuccess:(TXPlace *)place;
- (void)onDecodeLocationTaskError:(NSString *)error;

@end

@interface TXDecodeLocationTask : NSObject<TXTask>

@property (strong, nonatomic) id<TXTaskExecutor> taskExecutor;
@property (strong, nonatomic) TXGoogleGeoLocationTask *googleTask;
@property (strong, nonatomic) TXOSMGeoLocationTask *osmTask;;
@property (strong, nonatomic) TXQuerryPlaceByKeyTask *querryPlaceByKeyTask;

@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

@property (weak, nonatomic) id<TXDecodeLocationTaskDelegate> delegate;


@end
