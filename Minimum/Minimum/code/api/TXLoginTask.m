////////////////////////////////////////////////////////////////////////////////
//
//  TXLoginTask.m
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXLoginTask.h"

#import "SDDatabaseManager.h"
#import "TXServer.h"
#import "TXUrlEditor.h"
#import "TXUserDAO.h"
#import "TXCityDAO.h"
#import "NSString+Encode.h"

@implementation TXLoginTask {
    NSURLSessionTask *_httpTask;

}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    TXUrlEditor *urlEditor = [[TXUrlEditor alloc] initWithServerURL: _server.rootUrl];
    [urlEditor addParameter:@"t" Value:@"15"];
    [urlEditor addParameter:@"l" Value:_phone];
    [urlEditor addParameter:@"p" Value:_password];

    NSURL *url = [urlEditor getURL];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    _request = request;
    
    [_server executeGetTask:self];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXServerTask>
//-------------------------------------------------------------------------------------------

- (void)setHttpTask:(NSURLSessionTask *)task {
    _httpTask = task;
}

// TODO добавить описание ошибок
- (void)onSuccessWithResponse:(NSHTTPURLResponse *)response andData:(NSData *)data {
    
    NSInteger code = response.statusCode;
    if (code >= 200 & code < 300) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding];
        if (text != nil) {
            if (text.isHtml == NO) {
                NSArray *result = [self parseResult:text];
                BOOL isLogin = [[result objectAtIndex:0] boolValue];
                if (isLogin) {
                    TXUser *user = _userDAO.actualUser;
                    if (user == nil) {
                        user = [_userDAO createWithPhone:_phone];
                    } else {
                        user.phone = _phone;
                        [_userDAO save];
                    }
                    [_userDAO savePassword:_password];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (_delegate != nil) {
                            [_delegate onLoginTaskSuccess];
                        }
                    });
                } else {
                    NSString *message = [result objectAtIndex:1];
                    [self onLoginTaskError:message];
                }
            } else {
                [self onLoginTaskError:NSLocalizedString(@"Could not connect to server", nil)];
            }
        } else {
            [self onLoginTaskError:NSLocalizedString(@"Incorrect result from server", nil)];
        }
    } else {
        [self onLoginTaskError:NSLocalizedString(@"Bad code result from server", nil)];
    }
}

- (void)onFailureWithError:(NSError *)error {
    [self onLoginTaskError:[error localizedDescription]];
}

- (void)cancel {
    if (_httpTask != nil) {
        [_httpTask cancel];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Method
//-------------------------------------------------------------------------------------------

- (NSArray *)parseResult: (NSString *) text {
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:2];
    
    char code = [text characterAtIndex:0];
    if (code == '1') {
        [result addObject:@YES];
    } else {
        [result addObject:@NO];
    }
    
    NSString *message = nil;
    NSUInteger length = text.length;
    if (length > 2) {
        message = [text substringWithRange: NSMakeRange (2, length - 2)];
    }
    [result addObject:message];
    
    return result;
}

- (void)onLoginTaskError:(NSString *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_delegate != nil) { // check again couse it's othere thread!!!
            [_delegate onLoginTaskError:error];
        }
    });
    
}

@end
