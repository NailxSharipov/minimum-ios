////////////////////////////////////////////////////////////////////////////////
//
//  TXOSMGeoLocationTask.m
//  Minimum
//
//  Created by Nail Sharipov on 30/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXOSMGeoLocationTask.h"
#import "TXServer.h"
#import "TXUrlEditor.h"
#import "TXLocation.h"

#import "NSString+Encode.h"

@implementation TXOSMGeoLocationTask {
    NSURLSessionTask *_httpTask;
}

NSString* const OSM_GEOLOCATION_SERVER_URL = @"http://nominatim.openstreetmap.org/reverse";

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    TXUrlEditor *urlEditor = [[TXUrlEditor alloc] initWithServerURL:OSM_GEOLOCATION_SERVER_URL];
    
    [urlEditor addParameter:@"format" Value:@"json"];
    
    if (_key != nil) {
        [urlEditor addParameter:@"address" Value:_key];
    }
    
    if (_latitude != 0.0 && _longitude != 0.0) {
        [urlEditor addParameter:@"lat" Value:[NSString stringWithFormat:@"%f", _latitude]];
        [urlEditor addParameter:@"lon" Value:[NSString stringWithFormat:@"%f", _longitude]];
    }
    
    NSURL *url = [urlEditor getURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    _request = request;
    
    [_server executeGetTask:self];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXServerTask>
//-------------------------------------------------------------------------------------------

- (void)setHttpTask:(NSURLSessionTask *)task {
    _httpTask = task;
}

- (void)onSuccessWithResponse:(NSHTTPURLResponse *)response andData:(NSData *)data {
    NSInteger code = response.statusCode;
    if (code >= 200 & code < 300) {
        if (data != nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:nil];
            if (json != nil) {
                TXLocation *location = [self parseLocationFrom:json];
                if (location != nil) {
                    [_delegate onOSMGeoLocationTaskSuccess:location];
                } else {
                    [_delegate onOSMGeoLocationTaskFailure];
                }
            } else {
                [_delegate onOSMGeoLocationTaskFailure];
            }
        } else {
            [_delegate onOSMGeoLocationTaskFailure];
        }
    } else {
        [_delegate onOSMGeoLocationTaskFailure];
    }
}

- (void)onFailureWithError:(NSError *)error {
    NSLog(@"google decode error: %@", [error localizedDescription]);
    [_delegate onOSMGeoLocationTaskFailure];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)cancel {
    if (_httpTask != nil) {
        [_httpTask cancel];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (TXLocation *)parseLocationFrom :(NSDictionary *)json {
    TXLocation *location = nil;
    NSDictionary *address = json[@"address"];
    if (address != nil) {
        NSString *houseNumber = address[@"house_number"];
        NSString *road = address[@"road"];
        
        if (houseNumber != nil && road != nil) {
            location = [TXLocation new];
            
            location.number = houseNumber;
            location.street = road;
            
            NSLog(@"%@ %@", road, houseNumber);
        }
    }
    return location;
}

- (BOOL)isContain:(NSArray *)data key:(NSString *)key {
    for (NSString *str in data) {
        if ([str isEqualToString: key]) {
            return YES;
        }
    }
    return NO;
}

@end