////////////////////////////////////////////////////////////////////////////////
//
//  TXOSMGeoLocationTask.h
//  Minimum
//
//  Created by Nail Sharipov on 30/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXTask.h"
#import "TXServerTask.h"

@class TXLocation;
@protocol TXServer;

@protocol TXOSMGeoLocationTaskDelegate <NSObject>

@required

- (void)onOSMGeoLocationTaskSuccess:(TXLocation *)location;
- (void)onOSMGeoLocationTaskFailure;

@end


@interface TXOSMGeoLocationTask : NSObject<TXTask, TXServerTask>

@property (strong, nonatomic) id<TXServer> server;
@property (strong, nonatomic) NSURLRequest *request;

@property (strong, nonatomic) NSString *key;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

@property (weak, nonatomic) id<TXOSMGeoLocationTaskDelegate> delegate;

@end