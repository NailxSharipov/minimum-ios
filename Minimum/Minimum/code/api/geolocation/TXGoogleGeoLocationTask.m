////////////////////////////////////////////////////////////////////////////////
//
//  TXGoogleGeoLocationTask.m
//  Minimum
//
//  Created by Nail Sharipov on 30/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXGoogleGeoLocationTask.h"
#import "TXServer.h"
#import "TXUrlEditor.h"
#import "TXLocation.h"

#import "NSString+Encode.h"

@implementation TXGoogleGeoLocationTask {
    NSURLSessionTask *_httpTask;
}

NSString* const GOOGLE_GEOLOCATION_SERVER_URL = @"http://maps.googleapis.com/maps/api/geocode/json";

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    TXUrlEditor *urlEditor = [[TXUrlEditor alloc] initWithServerURL:GOOGLE_GEOLOCATION_SERVER_URL];

    if (_key != nil) {
       [urlEditor addParameter:@"address" Value:_key];
    }

    if (_latitude != 0.0 && _longitude != 0.0) {
       [urlEditor addParameter:@"latlng" Value:[NSString stringWithFormat:@"%f,%f", _latitude, _longitude]];
    }

    [urlEditor addParameter:@"sensor" Value:@"true"];
    [urlEditor addParameter:@"language" Value:@"ru"];

    NSURL *url = [urlEditor getURL];
     
     NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
     request.HTTPMethod = @"GET";
     _request = request;
     
     [_server executeGetTask:self];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXServerTask>
//-------------------------------------------------------------------------------------------

- (void)setHttpTask:(NSURLSessionTask *)task {
    _httpTask = task;
}

- (void)onSuccessWithResponse:(NSHTTPURLResponse *)response andData:(NSData *)data {
    NSInteger code = response.statusCode;
    if (code >= 200 & code < 300) {
        if (data != nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:nil];
            if (json != nil) {
                TXLocation *location = [self parseLocationFrom:json];
                if (location != nil) {
                    [_delegate onGoogleGeoLocationTaskSuccess:location];
                } else {
                    [_delegate onGoogleGeoLocationTaskFailure];
                }
            } else {
                [_delegate onGoogleGeoLocationTaskFailure];
            }
        } else {
            [_delegate onGoogleGeoLocationTaskFailure];
        }
    } else {
        [_delegate onGoogleGeoLocationTaskFailure];
    }
}

- (void)onFailureWithError:(NSError *)error {
    NSLog(@"google decode error: %@", [error localizedDescription]);
    [_delegate onGoogleGeoLocationTaskFailure];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)cancel {
    if (_httpTask != nil) {
        [_httpTask cancel];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (TXLocation *)parseLocationFrom :(NSDictionary *)json {
    NSArray *data = json[@"results"];
    
    for (NSDictionary *obj in data) {
        NSArray *types = obj[@"types"];
        
        if ([self isContain:types key:@"street_address"]) {
            NSArray *components = obj[@"address_components"];
            NSString *number = nil;
            NSString *street = nil;
            
            
            for (NSDictionary *componentData in components) {
                NSArray *componentTypes = componentData[@"types"];
                if ([self isContain:componentTypes key:@"street_number"]) {
                    number = componentData[@"long_name"];
                }
                if ([self isContain:componentTypes key:@"route"]) {
                    street = componentData[@"long_name"];
                }
            }
            
            if (number != nil && street != nil) {
                TXLocation *location = [TXLocation new];
                
                NSDictionary *geometry = obj[@"geometry"];
                if (geometry != nil) {
                    NSDictionary *locationDict = geometry[@"location"];
                    double lat = [locationDict[@"lat"] doubleValue];
                    double lng = [locationDict[@"lng"] doubleValue];
                    
                    location.lat = lat;
                    location.lng = lng;
                    
                    NSLog(@"lat: %f, lng: %f", lat, lng);
                }
                location.number = number;
                location.street = street;
                
                return location;
            }
        }
    }
    
    return nil;
}

- (BOOL)isContain:(NSArray *)data key:(NSString *)key {
    for (NSString *str in data) {
        if ([str isEqualToString: key]) {
            return YES;
        }
    }
    return NO;
}

@end