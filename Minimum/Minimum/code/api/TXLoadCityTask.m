////////////////////////////////////////////////////////////////////////////////
//
//  TXLoadCityTask.m
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXLoadCityTask.h"

#import "TXServer.h"
#import "TXUrlEditor.h"
#import "TXCityDAO.h"
#import "NSString+Encode.h"

@implementation TXLoadCityTask {
    NSURLSessionTask *_httpTask;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    TXUrlEditor *urlEditor = [[TXUrlEditor alloc] initWithServerURL: _server.rootUrl];
    [urlEditor addParameter:@"t" Value:@"brands"];
    [urlEditor addParameter:@"a" Value:@"all"];
    [urlEditor addParameter:@"out" Value:@"js"];
    [urlEditor addParameter:@"lang" Value:@"ru"];
    
    NSURL *url = [urlEditor getURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    _request = request;
    
    [_server executeGetTask:self];
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXServerTask>
//-------------------------------------------------------------------------------------------

- (void)setHttpTask:(NSURLSessionTask *)task {
    _httpTask = task;
}

// TODO добавить описание ошибок
- (void)onSuccessWithResponse:(NSHTTPURLResponse *)response andData:(NSData *)data {
    
    NSInteger code = response.statusCode;
    if (code >= 200 & code < 300) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding];
        if (text != nil) {
            if (text.isHtml == NO) {
                if ([_cityDAO createCityData:text] != nil) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (_delegate != nil) {
                            [_delegate onLoadCityTaskSuccess];
                        }
                    });
                } else {
                    [self onLoadCityTaskError:NSLocalizedString(@"Incorrect result from server", nil)];
                }
            } else {
                [self onLoadCityTaskError:NSLocalizedString(@"Could not connect to server", nil)];
            }
        } else {
            [self onLoadCityTaskError:NSLocalizedString(@"Incorrect result from server", nil)];
        }
    } else {
        [self onLoadCityTaskError:NSLocalizedString(@"Bad code result from server", nil)];
    }
}

- (void)onFailureWithError:(NSError *)error {
    [self onLoadCityTaskError:[error localizedDescription]];
}

- (void)cancel {
    if (_httpTask != nil) {
        [_httpTask cancel];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Method
//-------------------------------------------------------------------------------------------

- (void)onLoadCityTaskError:(NSString *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_delegate != nil) { // check again couse it's othere thread!!!
            [_delegate onLoadCityTaskError:error];
        }
    });
    
}

@end
