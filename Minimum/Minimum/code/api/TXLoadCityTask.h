////////////////////////////////////////////////////////////////////////////////
//
//  TXLoadCityTask.h
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

#import "TXTask.h"
#import "TXServerTask.h"


@protocol TXServer;
@protocol TXCityDAO;

@protocol TXLoadCityTaskDelegate <NSObject>

@required

- (void)onLoadCityTaskSuccess;
- (void)onLoadCityTaskError:(NSString *)error;


@end

@interface TXLoadCityTask : NSObject<TXTask, TXServerTask>

@property (strong, nonatomic) id<TXServer> server;
@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) id<TXCityDAO> cityDAO;
@property (weak, nonatomic) id<TXLoadCityTaskDelegate> delegate;


@end
