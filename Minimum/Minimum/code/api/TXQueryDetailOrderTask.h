////////////////////////////////////////////////////////////////////////////////
//
//  TXQueryDetailOrderTask.h
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXTask.h"
#import "TXServerTask.h"


@protocol TXServer;
@protocol TXUserDAO;
@protocol TXDetailOrderDAO;
@class TXDetailOrder;

@protocol TXQueryDetailOrderTaskDelegate <NSObject>

@required

- (void)onQueryDetailOrderTaskSuccess:(NSArray<TXDetailOrder *> *)detailOrderList;
- (void)onQueryDetailOrderTaskError:(NSString *)error;

@end

@interface TXQueryDetailOrderTask : NSObject<TXTask, TXServerTask>

@property (strong, nonatomic) id<TXServer> server;
@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) id<TXUserDAO> userDAO;
@property (strong, nonatomic) id<TXDetailOrderDAO> detailOrderDAO;
@property (weak, nonatomic) id<TXQueryDetailOrderTaskDelegate> delegate;

@end