////////////////////////////////////////////////////////////////////////////////
//
//  TXDeleteHistoryTask.m
//  Minimum
//
//  Created by Nail Sharipov on 18/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXDeleteHistoryTask.h"
#import "TXServer.h"
#import "TXUserDAO.h"
#import "TXOrderDAO.h"
#import "TXUrlEditor.h"

#import "NSString+Encode.h"

@implementation TXDeleteHistoryTask {
    NSURLSessionTask *_httpTask;
    NSUInteger _loadHistorySyncDateAnchor;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    NSString *phone = _userDAO.actualUser.phone;
    NSString *password = [_userDAO getPassword];
    
    if (phone != nil && phone.length > 0 && password != nil && password.length > 0) {
        TXUrlEditor *urlEditor = [[TXUrlEditor alloc] initWithServerURL: _server.rootUrl];
        
        [urlEditor addParameter:@"t" Value:@"del"];
        [urlEditor addParameter:@"l" Value:phone];
        [urlEditor addParameter:@"p" Value:password];
        [urlEditor addParameter:@"id" Value:_order.orderId];
        
        NSURL *url = [urlEditor getURL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        request.HTTPMethod = @"GET";
        _request = request;
        
        [_server executeGetTask:self];
    } else {
        [self onDeleteHistoryTaskError:NSLocalizedString(@"User login or password not set", nil)];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXServerTask>
//-------------------------------------------------------------------------------------------

- (void)setHttpTask:(NSURLSessionTask *)task {
    _httpTask = task;
}

- (void)onSuccessWithResponse:(NSHTTPURLResponse *)response andData:(NSData *)data {
    NSInteger code = response.statusCode;
    if (code >= 200 & code < 300) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding];
        if (text != nil) {
            if (text.isHtml == NO) {
                NSString *error = [self parseDeleteHistory:text];
                if (error == nil) {
                    [_orderDAO.databaseManager runInTransaction:^(BOOL *rollback) {
                        BOOL success = [_orderDAO remove:_order];
                        if (!success) {
                            *rollback = YES;
                        } else {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [_delegate onDeleteHistoryTaskSuccess:_order];
                            });
                        }
                    }];
                } else {
                    [self onDeleteHistoryTaskError:error];
                }
            } else {
                [self onDeleteHistoryTaskError:NSLocalizedString(@"Could not connect to server", nil)];
            }
        } else {
            [self onDeleteHistoryTaskError:NSLocalizedString(@"Incorrect result from server", nil)];
        }
    } else {
        [self onDeleteHistoryTaskError:NSLocalizedString(@"Bad code result from server", nil)];
    }
}

- (void)onFailureWithError:(NSError *)error {
    [self onDeleteHistoryTaskError:[error localizedDescription]];
}

- (void)cancel {
    if (_httpTask != nil) {
        [_httpTask cancel];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Method
//-------------------------------------------------------------------------------------------

- (void)onDeleteHistoryTaskError:(NSString *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_delegate != nil) { // check again couse it's othere thread!!!
            [_delegate onDeleteHistoryTaskError:error];
        }
    });
    
}

- (NSString *)parseDeleteHistory: (NSString *) text {
    //result=1 или result=0|error=текст ошибки
    if ([text containsString:@"result=0"]) {
        NSArray *blocks = [text componentsSeparatedByString:@"|"];
        if (blocks != nil && blocks.count > 1) {
            return blocks[1];
        }
    }
    return nil;
}

@end