////////////////////////////////////////////////////////////////////////////////
//
//  TXLoadHistoryTask.m
//  Minimum
//
//  Created by Nail Sharipov on 11/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXLoadHistoryTask.h"

#import "TXServer.h"
#import "TXUserDAO.h"
#import "TXOrderDAO.h"
#import "TXUrlEditor.h"

#import "NSString+Encode.h"

@implementation TXLoadHistoryTask {
    NSURLSessionTask *_httpTask;
    NSUInteger _loadHistorySyncDateAnchor;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    NSString *phone = _userDAO.actualUser.phone;
    NSString *password = [_userDAO getPassword];
    
    if (phone != nil && phone.length > 0 && password != nil && password.length > 0) {
        TXUrlEditor *urlEditor = [[TXUrlEditor alloc] initWithServerURL: _server.rootUrl];
        NSUInteger size = _size + 1;
        
        NSUInteger pg = _index / _size + 1;
        
        [urlEditor addParameter:@"t" Value:@"history"];
        [urlEditor addParameter:@"l" Value:phone];
        [urlEditor addParameter:@"p" Value:password];
        [urlEditor addParameter:@"pg" Value:[@(pg) stringValue]];
        [urlEditor addParameter:@"max" Value:[@(size) stringValue]];
        [urlEditor addParameter:@"version" Value:@"2"];
        
        NSURL *url = [urlEditor getURL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        request.HTTPMethod = @"GET";
        _request = request;
        
        [_server executeGetTask:self];
    } else {
        [self onLoadHistoryTaskError:NSLocalizedString(@"User login or password not set", nil)];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXServerTask>
//-------------------------------------------------------------------------------------------

- (void)setHttpTask:(NSURLSessionTask *)task {
    _httpTask = task;
}

// TODO добавить описание ошибок
- (void)onSuccessWithResponse:(NSHTTPURLResponse *)response andData:(NSData *)data {
    
    NSInteger code = response.statusCode;
    if (code >= 200 & code < 300) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding];
        if (text != nil) {
            if (text.isHtml == NO) {
                NSArray<TXOrder *> *orderList = [self parseHistory:text];
                __block BOOL success = NO;
                __block NSUInteger lastDateTimeStamp = 0;
                if (orderList != nil && orderList.count > 0) {
                    [_orderDAO.databaseManager runInTransaction:^(BOOL *rollback) {
                        if (_index == 0) {
                            _loadHistorySyncDateAnchor = [SDSQLiteTypeConverter dateToTimeStamp:[NSDate date]];
                        }
                        for (TXOrder *order in orderList) {
                            order.syncTimeStamp = _loadHistorySyncDateAnchor;
                            success = [_orderDAO save:order];
                            if (!success) {
                                break;
                            }
                        }

                        if (success) {
                            TXOrder *order = [orderList lastObject];
                            lastDateTimeStamp = order.dateTimeStamp;
                            success = [_orderDAO removeAllWithSyncDateOlderThen:order.syncTimeStamp andDateLaterThen:order.dateTimeStamp];
                        }
                        if (!success) {
                             *rollback = YES;
                        }
                    }];
                } else {
                    success = YES;
                    lastDateTimeStamp = 0;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (success) {
                        [_delegate onLoadHistoryTaskSuccess:lastDateTimeStamp];
                    } else {
                        [_delegate onLoadHistoryTaskError:nil];
                    }
                });
            } else {
                [self onLoadHistoryTaskError:NSLocalizedString(@"Could not connect to server", nil)];
            }
        } else {
            [self onLoadHistoryTaskError:NSLocalizedString(@"Incorrect result from server", nil)];
        }
    } else {
        [self onLoadHistoryTaskError:NSLocalizedString(@"Bad code result from server", nil)];
    }
}

- (void)onFailureWithError:(NSError *)error {
    [self onLoadHistoryTaskError:[error localizedDescription]];
}

- (void)cancel {
    if (_httpTask != nil) {
        [_httpTask cancel];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Method
//-------------------------------------------------------------------------------------------

- (void)onLoadHistoryTaskError:(NSString *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_delegate != nil) { // check again couse it's othere thread!!!
            [_delegate onLoadHistoryTaskError:error];
        }
    });
    
}

- (NSArray<TXOrder *> *)parseHistory: (NSString *) text {
    NSMutableArray<TXOrder *> *orderList = nil;
    
    NSArray *lines = [text componentsSeparatedByString:@"\n"];
    NSUInteger n = [lines count];

    if (n > 0) {
        NSString *line;
        // result
        line = [lines objectAtIndex:0];
        if ([[self getValue: line] isEqualToString:@"1"]) {
            
            orderList = [[NSMutableArray alloc] init];
            
            NSArray *bloks;
            NSString *tag;
            NSDictionary *values;
            NSUInteger i = 1;
            while (i < n) {
                line = [lines objectAtIndex:i];
                bloks = [line componentsSeparatedByString:@"|"];
                tag = [bloks objectAtIndex:0];
                
                i++;
                if ([tag isEqualToString:@"order"]) { // начало заказа
                    TXOrder *order = [TXOrder new];
                    [orderList addObject:order];
                    
                    values = [self collectData:bloks];
                    
                    NSMutableArray<TXPlace *> *placeList = [NSMutableArray new];
                    order.placeList = placeList;
                    
                    order.orderId = values[@"id"];

                    [order setDateFromText:values[@"order_date"]];
                    
                    line = [lines objectAtIndex:i];
                    bloks = [line componentsSeparatedByString:@"|"];
                    tag = [bloks objectAtIndex:0];
                    
                    while ([tag isEqualToString:@"route"]) {
                        values = [self collectData:bloks];
                        
                        TXPlace* place = [TXPlace new];
                        [placeList addObject:place];
                        
                        place.placeId = values[@"o"];
                        place.name = values[@"n"];
                        
                        NSString* home = values[@"h"];
                        NSString* type = values[@"type"];
                        if ([type isEqualToString:@"3"]) {
                            place.type = @"1"; // street
                        } else if ([type isEqualToString:@"4"]) {
                            place.type = @"2"; // statton
                        } else if ([type isEqualToString:@"5"]) {
                            place.type = @"3"; // object
                        } else {
                            place.type = @"1"; // street                        
                        }

                        NSArray* homeData = [home componentsSeparatedByString:@" "];
                        place.home = homeData[0];
                        
                        i++;
                        line = lines[i];
                        bloks = [line componentsSeparatedByString:@"|"];
                        tag = bloks[0];
                    }
                }
            }
        }
    }

    if ([orderList count] == 0) {
        orderList = nil;
    }
    
    return orderList;
}

- (NSString *)getValue: (NSString *)text {
    NSArray *lines = [text componentsSeparatedByString:@"="];
    return [lines objectAtIndex:1];
}

- (NSDictionary *)collectData: (NSArray *)data {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    for (NSString *block in data) {
        NSArray *sep = [block componentsSeparatedByString:@"="];
        if ([sep count] > 1) {
            [dict setObject:[sep objectAtIndex:1] forKey:[sep objectAtIndex:0]];
        }
    }
    return dict;
}

@end
