////////////////////////////////////////////////////////////////////////////////
//
//  TXChangePasswordTask.m
//  Minimum
//
//  Created by Nail Sharipov on 19/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXChangePasswordTask.h"
#import "TXServer.h"
#import "TXUserDAO.h"
#import "TXUrlEditor.h"

#import "NSString+Encode.h"


@implementation TXChangePasswordTask {
    NSURLSessionTask *_httpTask;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    NSString *phone = _userDAO.actualUser.phone;
    NSString *password = [_userDAO getPassword];
    
    if (phone != nil && phone.length > 0 && password != nil && password.length > 0) {
        TXUrlEditor *urlEditor = [[TXUrlEditor alloc] initWithServerURL: _server.rootUrl];
        
        [urlEditor addParameter:@"t" Value:@"passw"];
        [urlEditor addParameter:@"change_pass" Value:@"1"];
        [urlEditor addParameter:@"pass" Value:_password];
        [urlEditor addParameter:@"l" Value:phone];
        [urlEditor addParameter:@"p" Value:password];
        
        NSURL *url = [urlEditor getURL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        request.HTTPMethod = @"GET";
        _request = request;
        
        [_server executeGetTask:self];
    } else {
        [self onChangePasswordTaskError:NSLocalizedString(@"User login or password not set", nil)];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXServerTask>
//-------------------------------------------------------------------------------------------

- (void)setHttpTask:(NSURLSessionTask *)task {
    _httpTask = task;
}

- (void)onSuccessWithResponse:(NSHTTPURLResponse *)response andData:(NSData *)data {
    NSInteger code = response.statusCode;
    if (code >= 200 & code < 300) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding];
        if (text != nil) {
            if (text.isHtml == NO) {
                NSString *error = [self changePassword:text];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (error == nil) {
                        [_userDAO savePassword:_password];
                        [_delegate onChangePasswordTaskSuccess];
                    } else {
                        [_delegate onChangePasswordTaskError:error];
                    }
                });
            } else {
                [self onChangePasswordTaskError:NSLocalizedString(@"Could not connect to server", nil)];
            }
        } else {
            [self onChangePasswordTaskError:NSLocalizedString(@"Incorrect result from server", nil)];
        }
    } else {
        [self onChangePasswordTaskError:NSLocalizedString(@"Bad code result from server", nil)];
    }
}

- (void)onFailureWithError:(NSError *)error {
    [self onChangePasswordTaskError:[error localizedDescription]];
}

- (void)cancel {
    if (_httpTask != nil) {
        [_httpTask cancel];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Method
//-------------------------------------------------------------------------------------------

- (void)onChangePasswordTaskError:(NSString *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_delegate != nil) { // check again couse it's othere thread!!!
            [_delegate onChangePasswordTaskError:error];
        }
    });
    
}

- (NSString *)changePassword: (NSString *) text {
    NSString *message = nil;
    NSUInteger length = [text length];
    if (length > 2) {//error
        NSRange range = [text rangeOfString:@"error="];
        NSUInteger start = range.location + range.length;
        message = [text substringWithRange: NSMakeRange (start, length - start)];
    }
    return message;
}

@end
