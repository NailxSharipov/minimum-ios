////////////////////////////////////////////////////////////////////////////////
//
//  TXChangePasswordTask.h
//  Minimum
//
//  Created by Nail Sharipov on 19/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

#import "TXTask.h"
#import "TXServerTask.h"

@protocol TXServer;
@protocol TXUserDAO;

@protocol TXChangePasswordTaskDelegate <NSObject>

@required

- (void)onChangePasswordTaskSuccess;
- (void)onChangePasswordTaskError:(NSString *)error;

@end

@interface TXChangePasswordTask : NSObject<TXTask, TXServerTask>

@property (strong, nonatomic) id<TXServer> server;
@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) id<TXUserDAO> userDAO;
@property (strong, nonatomic) NSString *password;
@property (weak, nonatomic) id<TXChangePasswordTaskDelegate> delegate;


@end
