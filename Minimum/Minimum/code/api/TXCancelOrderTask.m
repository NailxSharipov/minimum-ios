////////////////////////////////////////////////////////////////////////////////
//
//  TXCancelOrderTask.m
//  Minimum
//
//  Created by Nail Sharipov on 01/02/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXCancelOrderTask.h"
#import "TXServer.h"
#import "TXUserDAO.h"
#import "TXDetailOrderDAO.h"
#import "TXUrlEditor.h"
#import "NSString+Encode.h"
#import "TXDetailOrder.h"

@implementation TXCancelOrderTask {
    NSURLSessionTask *_httpTask;
    NSUInteger _loadHistorySyncDateAnchor;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    NSString *phone = _userDAO.actualUser.phone;
    NSString *password = [_userDAO getPassword];
    [self cancel];
    
    if (phone != nil && phone.length > 0 && password != nil && password.length > 0) {
        TXUrlEditor *urlEditor = [[TXUrlEditor alloc] initWithServerURL: _server.rootUrl];
        
        [urlEditor addParameter:@"t" Value:@"5"];
        [urlEditor addParameter:@"l" Value:phone];
        [urlEditor addParameter:@"p" Value:password];
        [urlEditor addParameter:@"z" Value:_detailOrder.orderId];
        
        
        NSURL *url = [urlEditor getURL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        request.HTTPMethod = @"GET";
        _request = request;
        
        [_server executeGetTask:self];
    } else {
        [self onTaskError:NSLocalizedString(@"User login or password not set", nil)];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXServerTask>
//-------------------------------------------------------------------------------------------

- (void)setHttpTask:(NSURLSessionTask *)task {
    _httpTask = task;
}

// TODO добавить описание ошибок
- (void)onSuccessWithResponse:(NSHTTPURLResponse *)response andData:(NSData *)data {
    
    NSInteger code = response.statusCode;
    if (code >= 200 & code < 300) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding];
        if (text != nil) {
            if (text.isHtml == NO) {
                [_detailOrderDAO.databaseManager runInTransaction:^(BOOL *rollback) {
                    _detailOrder.code = 100;
                    _detailOrder.status = NSLocalizedString(@"order is canceled", nil);
                }];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_delegate onCancelOrderTaskSuccess];
                });
            } else {
                [self onTaskError:NSLocalizedString(@"Could not connect to server", nil)];
            }
        } else {
            [self onTaskError:NSLocalizedString(@"Incorrect result from server", nil)];
        }
    } else {
        [self onTaskError:NSLocalizedString(@"Bad code result from server", nil)];
    }
}

- (void)onFailureWithError:(NSError *)error {
    [self onTaskError:[error localizedDescription]];
}

- (void)cancel {
    if (_httpTask != nil) {
        [_httpTask cancel];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Method
//-------------------------------------------------------------------------------------------

- (void)onTaskError:(NSString *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_delegate != nil) {
            [_delegate onCancelOrderTaskError:error];
        }
    });
}


@end