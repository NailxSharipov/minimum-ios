////////////////////////////////////////////////////////////////////////////////
//
//  TXMakeOrderTask.m
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXMakeOrderTask.h"
#import "TXCurrentOrder.h"
#import "TXServer.h"
#import "TXUserDAO.h"
#import "TXCityDAO.h"
#import "TXUrlEditor.h"
#import "TXPlace.h"

#import "NSString+Encode.h"

@implementation TXMakeOrderTask {
    NSURLSessionTask *_httpTask;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXTask>
//-------------------------------------------------------------------------------------------

- (void)run {
    [self cancel];
    NSString *phone = _userDAO.actualUser.phone;
    NSString *password = [_userDAO getPassword];
    NSString *secondPhone = _userDAO.actualUser.secondPhone;
    NSString *brandId = _cityDAO.actualCity.brandId;
    
    if (phone != nil && phone.length > 0 && password != nil && password.length > 0) {
        TXUrlEditor *urlEditor = [[TXUrlEditor alloc] initWithServerURL: _server.rootUrl];
        
        TXPlace *from = _currentOrder.placeList.firstObject;
        NSString *comment = from.definition;
        
        [urlEditor addParameter:@"t" Value:@"order"];
        
        if (_isPriceOnly) {
            [urlEditor addParameter:@"a" Value:@"cost"];
        } else {
            [urlEditor addParameter:@"a" Value:@"send"];
            if (secondPhone != nil && secondPhone.length > 0) {
                [urlEditor addParameter:@"ph" Value:secondPhone];
            }
            if (comment != nil && comment.length > 0) {
                [urlEditor addParameter:@"com1" Value:comment];
            }
        }
        [urlEditor addParameter:@"l" Value:phone];
        [urlEditor addParameter:@"p" Value:password];
        [urlEditor addParameter:@"no" Value:@"1"];

        [urlEditor addParameter:@"brand" Value:brandId];

        NSString *ob = @"ob1";
        NSString *h = @"h1";;
        
        NSMutableString *trace = [NSMutableString new];

        [trace appendString:[NSString stringWithFormat:@"%@=%@", ob, from.placeId]];
        if (from.isStreet && from.home != nil && from.home.length > 0) {
            NSString *home = [from.home encodeString];
            [trace appendString:[NSString stringWithFormat:@"&%@=%@", h, home]];
        }

        NSArray *placeList = _currentOrder.getNotEmptyPlaceList;
        NSUInteger count = placeList.count - 1;
        
        [urlEditor addParameter:@"pn" Value:[@(count) stringValue]];

        NSUInteger n = placeList.count;
        for (NSUInteger j = 1; j < n; j++) {
            TXPlace *place = placeList[j];
            ob = [NSString stringWithFormat:@"ob%lu", (j + 1)];
            [trace appendString:[NSString stringWithFormat:@"&%@=%@", ob, place.placeId]];
            if (place.isStreet && place.home != nil && place.home.length > 0) {
                NSString *home = [place.home encodeString];
                h = [NSString stringWithFormat:@"h%lu", (j + 1)];
                [trace appendString:[NSString stringWithFormat:@"&%@=%@", h, home]];
            }
        }
        [urlEditor addEmptyParameter:trace];
        
        NSURL *url = [urlEditor getURL];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        request.HTTPMethod = @"GET";
        _request = request;
        
        [_server executeGetTask:self];
    } else {
        [self onTaskError:NSLocalizedString(@"User login or password not set", nil)];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXServerTask>
//-------------------------------------------------------------------------------------------

- (void)setHttpTask:(NSURLSessionTask *)task {
    _httpTask = task;
}

- (void)onSuccessWithResponse:(NSHTTPURLResponse *)response andData:(NSData *)data {
    NSInteger code = response.statusCode;
    if (code >= 200 & code < 300) {
        NSString *text = [[NSString alloc] initWithData:data encoding:NSWindowsCP1251StringEncoding];
        if (text != nil) {
            if (text.isHtml == NO) {
                NSDictionary *dict = [self parseMakeOrder:text];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (dict != nil) {
                        [_delegate onMakeOrderTaskSuccess:dict];
                    } else {
                        [_delegate onMakeOrderTaskError:nil];
                    }
                });
            } else {
                [self onTaskError:NSLocalizedString(@"Could not connect to server", nil)];
            }
        } else {
            [self onTaskError:NSLocalizedString(@"Incorrect result from server", nil)];
        }
    } else {
        [self onTaskError:NSLocalizedString(@"Bad code result from server", nil)];
    }
}

- (void)onFailureWithError:(NSError *)error {
    [self onTaskError:[error localizedDescription]];
}

- (void)cancel {
    if (_httpTask != nil) {
        [_httpTask cancel];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Method
//-------------------------------------------------------------------------------------------

- (void)onTaskError:(NSString *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_delegate != nil) { // check again couse it's othere thread!!!
            [_delegate onMakeOrderTaskError:error];
        }
    });
}

- (NSDictionary *)parseMakeOrder: (NSString *) text {
    //    1 80 66926477
    NSString *strCode = [text substringWithRange: NSMakeRange (0, 1)];
    NSInteger code = [strCode integerValue];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    if (code == 1) {
        NSArray *data = [text componentsSeparatedByString:@" "];
        
        NSUInteger size = [data count];
        
        if (size > 1) {
            NSString *price = data[1];
            dict[@"price"] = price;
        }
        
        if (size > 2) {
            NSString *orderId = data[2];
            dict[@"orderId"] = orderId;
        }
    } else if (code == 0) {
        if (text.length > 1) {
            NSString *error = [text substringFromIndex:2];
            dict[@"error"] = error;
        }
    }
    
    return dict;
}

@end
