////////////////////////////////////////////////////////////////////////////////
//
//  TXMakeOrderTask.h
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXTask.h"
#import "TXServerTask.h"

@class TXCurrentOrder;

@protocol TXServer;
@protocol TXUserDAO;
@protocol TXCityDAO;

@protocol TXMakeOrderTaskDelegate <NSObject>

@required

- (void)onMakeOrderTaskSuccess:(NSDictionary *)dict;
- (void)onMakeOrderTaskError:(NSString *)error;

@end

@interface TXMakeOrderTask : NSObject<TXTask, TXServerTask>

@property (strong, nonatomic) id<TXServer> server;
@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) id<TXUserDAO> userDAO;
@property (strong, nonatomic) id<TXCityDAO> cityDAO;
@property (weak, nonatomic) id<TXMakeOrderTaskDelegate> delegate;
@property (weak, nonatomic) TXCurrentOrder *currentOrder;
@property (nonatomic) BOOL isPriceOnly;


@end
