////////////////////////////////////////////////////////////////////////////////
//
//  TXDeleteHistoryTask.h
//  Minimum
//
//  Created by Nail Sharipov on 18/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

#import "TXTask.h"
#import "TXServerTask.h"

@class TXOrder;

@protocol TXServer;
@protocol TXUserDAO;
@protocol TXOrderDAO;

@protocol TXDeleteHistoryDelegate <NSObject>

@required

- (void)onDeleteHistoryTaskSuccess:(TXOrder *)order;
- (void)onDeleteHistoryTaskError:(NSString *)error;

@end

@interface TXDeleteHistoryTask : NSObject<TXTask, TXServerTask>

@property (strong, nonatomic) TXOrder *order;
@property (strong, nonatomic) id<TXServer> server;
@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) id<TXUserDAO> userDAO;
@property (strong, nonatomic) id<TXOrderDAO> orderDAO;
@property (weak, nonatomic) id<TXDeleteHistoryDelegate> delegate;


@end
