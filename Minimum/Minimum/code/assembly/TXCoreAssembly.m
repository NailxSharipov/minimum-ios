////////////////////////////////////////////////////////////////////////////////
//
//  TXCoreAssembly.m
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXCoreAssembly.h"
#import "TXServerImpl.h"
#import "TXSerialTaskExecutor.h"


@implementation TXCoreAssembly

//-------------------------------------------------------------------------------------------
#pragma mark - Interface Methods
//-------------------------------------------------------------------------------------------

- (id <TXServer>)buildRootServer {
    return [TyphoonDefinition withClass:[TXServerImpl class] configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(domain) with: @"taxi-leader.ru"];
                
                definition.scope = TyphoonScopeSingleton;
            }];
}

- (id <TXTaskExecutor>)buildTaskExecutor {
    return [TyphoonDefinition withClass:[TXSerialTaskExecutor class] configuration:^(TyphoonDefinition *definition)
            {
                [definition useInitializer:@selector(initWithName:) parameters:^(TyphoonMethod *initializer)
                 {
                     [initializer injectParameterWith:@"com.weakteam.taxi.core.BackgroundTask"];
                 }];
                
                definition.scope = TyphoonScopeSingleton;
            }];
}


@end
