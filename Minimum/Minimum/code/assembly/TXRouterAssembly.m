////////////////////////////////////////////////////////////////////////////////
//
//  TXRouterAssembly.m
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXRouterAssembly.h"

#import "TXViewControllerAssembly.h"

#import "TXRouterImpl.h"


@implementation TXRouterAssembly

- (id<TXRouter>)buildFromLaunchToLogin {
    return [TyphoonDefinition withClass:[TXRouterImpl class] configuration:^(TyphoonDefinition *definition)
            {
                TXRouteBlock routeBlock = ^(UIViewController *from, UIViewController<TXPassable> *destination, NSDictionary *intent) {
                    [from presentViewController:destination animated:NO completion:nil];
                };
                
                [definition injectProperty:@selector(destination) with:_vcAssembly.buildLoginNavigation];
                [definition injectProperty:@selector(routeBlock) with:routeBlock];
            }];
}

- (id<TXRouter>)buildFromLaunchToMain {
    return [TyphoonDefinition withClass:[TXRouterImpl class] configuration:^(TyphoonDefinition *definition)
            {
                TXRouteBlock routeBlock = ^(UIViewController *from, UIViewController<TXPassable> *destination, NSDictionary *intent) {
                    [from presentViewController:destination animated:YES completion:nil];
                };
                
                [definition injectProperty:@selector(destination) with:_vcAssembly.buildMain];
                [definition injectProperty:@selector(routeBlock) with:routeBlock];
            }];
}

- (id<TXRouter>)buildFromLoginToMain {
    return [TyphoonDefinition withClass:[TXRouterImpl class] configuration:^(TyphoonDefinition *definition)
            {
                TXRouteBlock routeBlock = ^(UIViewController *from, UIViewController<TXPassable> *destination, NSDictionary *intent) {
                    [from presentViewController:destination animated:YES completion:nil];
                };
                
                [definition injectProperty:@selector(destination) with:_vcAssembly.buildMain];
                [definition injectProperty:@selector(routeBlock) with:routeBlock];
            }];
}

- (id<TXRouter>)buildFromSideMenuToSelectCity {
    return [TyphoonDefinition withClass:[TXRouterImpl class] configuration:^(TyphoonDefinition *definition)
            {
                TXRouteBlock routeBlock = ^(UIViewController *from, UIViewController<TXPassable> *destination, NSDictionary *intent) {
                    [from presentViewController:destination animated:YES completion:nil];
                };
                
                [definition injectProperty:@selector(destination) with:_vcAssembly.buildSelectCity];
                [definition injectProperty:@selector(routeBlock) with:routeBlock];
            }];
}

@end
