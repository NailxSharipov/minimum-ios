////////////////////////////////////////////////////////////////////////////////
//
//  TXTaskAssembly.h
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "Typhoon.h"


@class TXCoreAssembly;
@class TXDataAssembly;
@class TXLoginTask;
@class TXLoadCityTask;
@class TXLoadHistoryTask;
@class TXInitUserWorkspaceTask;
@class TXQueryHistoryTask;
@class TXQuerryLastPlaceTask;
@class TXQuerryPlaceByKeyTask;
@class TXDeleteHistoryTask;
@class TXChangePasswordTask;
@class TXRemindPasswordTask;
@class TXMakeOrderTask;
@class TXQueryDetailOrderTask;
@class TXCancelOrderTask;

@class TXDecodeLocationTask;
@class TXGoogleGeoLocationTask;
@class TXOSMGeoLocationTask;

@interface TXTaskAssembly : TyphoonAssembly

@property(nonatomic, strong, readonly) TXCoreAssembly *coreAssembly;
@property(nonatomic, strong, readonly) TXDataAssembly *dataAssembly;

- (TXLoginTask *)buildLoginTask;
- (TXLoadCityTask *)buildLoadCityTask;
- (TXLoadHistoryTask *)buildLoadHistoryTask;
- (TXInitUserWorkspaceTask *)buildInitUserWorkspaceTask;

- (TXQueryHistoryTask *)buildQueryHistoryTask;
- (TXQuerryLastPlaceTask *)buildQueryLastPlaceTask;
- (TXQuerryPlaceByKeyTask *)buildQueryPlaceByKeyTask;

- (TXDeleteHistoryTask *)buildDeleteHistoryTask;
- (TXChangePasswordTask *)buildChangePasswordTask;
- (TXRemindPasswordTask *)buildRemindPasswordTask;

- (TXDecodeLocationTask *)buildDecodeLocationTask;
- (TXGoogleGeoLocationTask *)buildGoogleGeoLocationTask;
- (TXOSMGeoLocationTask *)buildOSMGeoLocationTask;

- (TXMakeOrderTask *)buildMakeOrderTask;
- (TXQueryDetailOrderTask *)buildQueryDetailOrderTask;
- (TXCancelOrderTask *)buildCancelOrderTask;


@end
