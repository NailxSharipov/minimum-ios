////////////////////////////////////////////////////////////////////////////////
//
//  TXDataAssembly.h
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "Typhoon.h"


@class TXTaskAssembly;
@class TXCoreAssembly;
@class SDDatabase;
@class TXCurrentOrder;
@class TXLocationManager;

@protocol TXUserDAO;
@protocol TXCityDAO;
@protocol TXPlaceDAO;
@protocol TXOrderDAO;
@protocol TXDetailOrderDAO;


@interface TXDataAssembly : TyphoonAssembly

@property(nonatomic, strong, readonly) TXTaskAssembly *taskAssembly;
@property(nonatomic, strong, readonly) TXCoreAssembly *coreAssembly;

- (id<TXUserDAO>)buildUserDAO;
- (id<TXCityDAO>)buildCityDAO;
- (id<TXPlaceDAO>)buildPlaceDAO;
- (id<TXOrderDAO>)buildOrderDAO;
- (id<TXDetailOrderDAO>)buildDetailOrderDAO;
- (TXCurrentOrder *)buildCurrentOrder;
- (SDDatabase *)buildDatabase;
- (TXLocationManager *)buildLocationManager;

@end
