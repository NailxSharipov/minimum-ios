////////////////////////////////////////////////////////////////////////////////
//
//  TXDataAssembly.m
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXDataAssembly.h"
#import "TXTaskAssembly.h"
#import "TXCoreAssembly.h"
#import "SDDatabaseManager.h"
#import "TXUserDAOImpl.h"
#import "TXCityDAOImpl.h"
#import "TXPlaceDAOImpl.h"
#import "TXOrderDAOImpl.h"
#import "TXDetailOrderDAOImpl.h"
#import "TXCurrentOrder.h"
#import "TXLocationManager.h"

#import "TXOrderShema.h"
#import "TXDetailOrderShema.h"
#import "TXPlaceShema.h"

@implementation TXDataAssembly

//-------------------------------------------------------------------------------------------
#pragma mark - Interface Methods
//-------------------------------------------------------------------------------------------

- (id <TXUserDAO>)buildUserDAO {
    return [TyphoonDefinition withClass:[TXUserDAOImpl class] configuration:^(TyphoonDefinition *definition)
    {
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (id <TXCityDAO>)buildCityDAO {
    return [TyphoonDefinition withClass:[TXCityDAOImpl class] configuration:^(TyphoonDefinition *definition)
    {
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (id<TXPlaceDAO>)buildPlaceDAO {
    return [TyphoonDefinition withClass:[TXPlaceDAOImpl class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(databaseManager) with: self.buildDatabase];
        definition.scope = TyphoonScopeSingleton;
    }];
}
- (id<TXOrderDAO>)buildOrderDAO {
    return [TyphoonDefinition withClass:[TXOrderDAOImpl class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(placeDAO) with: self.buildPlaceDAO];
        [definition injectProperty:@selector(databaseManager) with: self.buildDatabase];
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (id<TXDetailOrderDAO>)buildDetailOrderDAO {
    return [TyphoonDefinition withClass:[TXDetailOrderDAOImpl class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(databaseManager) with: self.buildDatabase];
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (TXCurrentOrder *)buildCurrentOrder {
    return [TyphoonDefinition withClass:[TXCurrentOrder class] configuration:^(TyphoonDefinition *definition)
    {
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (SDDatabase *)buildDatabase {
    return [TyphoonDefinition withClass:[SDDatabaseManager class] configuration:^(TyphoonDefinition *definition)
    {
        NSArray<Class> *shemaClassArray = @[
                                            TXOrderShema.class,
                                            TXPlaceShema.class,
                                            TXDetailOrderShema.class
                                            ];
        
        [definition useInitializer:@selector(initWithVersion:shemaClassArray:)
                        parameters:^(TyphoonMethod *initializer) {
                            [initializer injectParameterWith:@1];
                            [initializer injectParameterWith:shemaClassArray];
                        }];
        [definition initializer];
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (TXLocationManager *)buildLocationManager {
    return [TyphoonDefinition withClass:[TXLocationManager class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(taskExecutor) with: _coreAssembly.buildTaskExecutor];
        [definition injectProperty:@selector(decodeLocationTask) with: _taskAssembly.buildDecodeLocationTask];

        definition.scope = TyphoonScopeSingleton;
    }];
}

@end
