////////////////////////////////////////////////////////////////////////////////
//
//  TXTaskAssembly.m
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXTaskAssembly.h"

#import "TXCoreAssembly.h"
#import "TXDataAssembly.h"
#import "TXLoadCityTask.h"
#import "TXLoadHistoryTask.h"
#import "TXLoginTask.h"
#import "TXInitUserWorkspaceTask.h"
#import "TXQueryHistoryTask.h"
#import "TXQuerryLastPlaceTask.h"
#import "TXQuerryPlaceByKeyTask.h"
#import "TXDeleteHistoryTask.h"
#import "TXChangePasswordTask.h"
#import "TXRemindPasswordTask.h"
#import "TXQueryDetailOrderTask.h"
#import "TXCancelOrderTask.h"

#import "TXDecodeLocationTask.h"
#import "TXGoogleGeoLocationTask.h"
#import "TXOSMGeoLocationTask.h"

#import "TXMakeOrderTask.h"

@implementation TXTaskAssembly

//-------------------------------------------------------------------------------------------
#pragma mark - Interface Methods
//-------------------------------------------------------------------------------------------

- (TXLoginTask *)buildLoginTask {
    return [TyphoonDefinition withClass:[TXLoginTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(server) with:[_coreAssembly buildRootServer]];
        [definition injectProperty:@selector(userDAO) with:[_dataAssembly buildUserDAO]];
    }];
}

- (TXLoadCityTask *)buildLoadCityTask {
    return [TyphoonDefinition withClass:[TXLoadCityTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(server) with:[_coreAssembly buildRootServer]];
        [definition injectProperty:@selector(cityDAO) with:[_dataAssembly buildCityDAO]];
    }];
}

- (TXLoadHistoryTask *)buildLoadHistoryTask {
    return [TyphoonDefinition withClass:[TXLoadHistoryTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(server) with:[_coreAssembly buildRootServer]];
        [definition injectProperty:@selector(userDAO) with:[_dataAssembly buildUserDAO]];
        [definition injectProperty:@selector(orderDAO) with:[_dataAssembly buildOrderDAO]];
    }];
}

- (TXInitUserWorkspaceTask *)buildInitUserWorkspaceTask {
    return [TyphoonDefinition withClass:[TXInitUserWorkspaceTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(server) with:_coreAssembly.buildRootServer];
        [definition injectProperty:@selector(cityDAO) with:_dataAssembly.buildCityDAO];
        [definition injectProperty:@selector(userDAO) with:_dataAssembly.buildUserDAO];
        [definition injectProperty:@selector(databaseManager) with:_dataAssembly.buildDatabase];
        [definition injectProperty:@selector(currentOrder) with:_dataAssembly.buildCurrentOrder];
    }];
}

- (TXQueryHistoryTask *)buildQueryHistoryTask {
    return [TyphoonDefinition withClass:[TXQueryHistoryTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(orderDAO) with:[_dataAssembly buildOrderDAO]];
    }];
}

- (TXQuerryLastPlaceTask *)buildQueryLastPlaceTask {
    return [TyphoonDefinition withClass:[TXQuerryLastPlaceTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(placeDAO) with:[_dataAssembly buildPlaceDAO]];
    }];
}

- (TXQuerryPlaceByKeyTask *)buildQueryPlaceByKeyTask {
    return [TyphoonDefinition withClass:[TXQuerryPlaceByKeyTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(placeDAO) with:_dataAssembly.buildPlaceDAO];
        [definition injectProperty:@selector(server) with:_coreAssembly.buildRootServer];
        [definition injectProperty:@selector(userDAO) with:_dataAssembly.buildUserDAO];
    }];
}

- (TXDeleteHistoryTask *)buildDeleteHistoryTask {
    return [TyphoonDefinition withClass:[TXDeleteHistoryTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(server) with:[_coreAssembly buildRootServer]];
        [definition injectProperty:@selector(userDAO) with:[_dataAssembly buildUserDAO]];
        [definition injectProperty:@selector(orderDAO) with:[_dataAssembly buildOrderDAO]];
    }];
}

- (TXChangePasswordTask *) buildChangePasswordTask {
    return [TyphoonDefinition withClass:[TXChangePasswordTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(server) with:[_coreAssembly buildRootServer]];
        [definition injectProperty:@selector(userDAO) with:[_dataAssembly buildUserDAO]];
    }];
}

- (TXRemindPasswordTask *)buildRemindPasswordTask {
    return [TyphoonDefinition withClass:[TXRemindPasswordTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(server) with:[_coreAssembly buildRootServer]];
    }];
}

- (TXDecodeLocationTask *)buildDecodeLocationTask {
    return [TyphoonDefinition withClass:[TXDecodeLocationTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(taskExecutor) with: _coreAssembly.buildTaskExecutor];
        [definition injectProperty:@selector(googleTask) with: self.buildGoogleGeoLocationTask];
        [definition injectProperty:@selector(osmTask) with: self.buildOSMGeoLocationTask];
        [definition injectProperty:@selector(querryPlaceByKeyTask) with: self.buildQueryPlaceByKeyTask];
    }];
}

- (TXGoogleGeoLocationTask *)buildGoogleGeoLocationTask {
    return [TyphoonDefinition withClass:[TXGoogleGeoLocationTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(server) with:[_coreAssembly buildRootServer]];
    }];
}

- (TXOSMGeoLocationTask *)buildOSMGeoLocationTask {
    return [TyphoonDefinition withClass:[TXOSMGeoLocationTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(server) with:[_coreAssembly buildRootServer]];
    }];
}

- (TXMakeOrderTask *)buildMakeOrderTask {
    return [TyphoonDefinition withClass:[TXMakeOrderTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(server) with:_coreAssembly.buildRootServer];
        [definition injectProperty:@selector(userDAO) with:_dataAssembly.buildUserDAO];
        [definition injectProperty:@selector(cityDAO) with:_dataAssembly.buildCityDAO];
        [definition injectProperty:@selector(currentOrder) with:_dataAssembly.buildCurrentOrder];
    }];
}

- (TXQueryDetailOrderTask *)buildQueryDetailOrderTask {
    return [TyphoonDefinition withClass:[TXQueryDetailOrderTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(server) with:_coreAssembly.buildRootServer];
        [definition injectProperty:@selector(userDAO) with:_dataAssembly.buildUserDAO];
        [definition injectProperty:@selector(detailOrderDAO) with:_dataAssembly.buildDetailOrderDAO];
    }];
}

- (TXCancelOrderTask *)buildCancelOrderTask {
    return [TyphoonDefinition withClass:[TXCancelOrderTask class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(server) with:_coreAssembly.buildRootServer];
        [definition injectProperty:@selector(userDAO) with:_dataAssembly.buildUserDAO];
        [definition injectProperty:@selector(detailOrderDAO) with:_dataAssembly.buildDetailOrderDAO];
    }];
}


@end
