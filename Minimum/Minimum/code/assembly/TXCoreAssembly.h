////////////////////////////////////////////////////////////////////////////////
//
//  TXCoreAssembly.h
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "Typhoon.h"


@protocol TXServer;
@protocol TXTaskExecutor;


@interface TXCoreAssembly : TyphoonAssembly

- (id<TXServer>)buildRootServer;
- (id<TXTaskExecutor>)buildTaskExecutor;

@end
