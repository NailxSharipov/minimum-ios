////////////////////////////////////////////////////////////////////////////////
//
//  TXViewControllerAssembly.m
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXViewControllerAssembly.h"

#import "TXCoreAssembly.h"
#import "TXDataAssembly.h"
#import "TXRouterAssembly.h"
#import "TXTaskAssembly.h"

#import "TXOrderNavigationController.h"
#import "TXMakeOrderViewController.h"
#import "TXEditOrderViewController.h"
#import "TXDetailPlaceViewController.h"
#import "TXEditPlaceViewController.h"
#import "TXPlaceOnMapViewController.h"
#import "TXCurrentOrderViewController.h"

#import "TXLaunchViewController.h"
#import "TXLoginNavigationController.h"
#import "TXLoginViewController.h"

#import "TXMainViewController.h"
#import "TXSideMenuViewController.h"

#import "TXProfileViewController.h"
#import "TXSelectCityViewController.h"
#import "TXHistoryViewController.h"


@implementation TXViewControllerAssembly

//-------------------------------------------------------------------------------------------
#pragma mark - Interface Methods
//-------------------------------------------------------------------------------------------

- (TXLaunchViewController *)buildLaunch {
    return [TyphoonDefinition withClass:[TXLaunchViewController class] configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(taskExecutor) with: _coreAssembly.buildTaskExecutor];
                [definition injectProperty:@selector(loadCityTask) with: _taskAssembly.buildLoadCityTask];
                [definition injectProperty:@selector(loginTask) with: _taskAssembly.buildLoginTask];
                [definition injectProperty:@selector(prepareUserWorkspaceTask) with: _taskAssembly.buildInitUserWorkspaceTask];
                [definition injectProperty:@selector(routeToLogin) with: _routerAssembly.buildFromLaunchToLogin];
                [definition injectProperty:@selector(routeToMain) with: _routerAssembly.buildFromLaunchToMain];
                [definition injectProperty:@selector(userDAO) with: _dataAssembly.buildUserDAO];
            }];
}

- (TXLoginViewController *)buildLogin {
    return [TyphoonDefinition withClass:[TXLoginViewController class] configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(userDAO) with: _dataAssembly.buildUserDAO];
                [definition injectProperty:@selector(cityDAO) with: _dataAssembly.buildCityDAO];
                [definition injectProperty:@selector(taskExecutor) with: _coreAssembly.buildTaskExecutor];
                [definition injectProperty:@selector(selectCity) with: self.buildSelectCity];
                [definition injectProperty:@selector(loginTask) with: _taskAssembly.buildLoginTask];
                [definition injectProperty:@selector(remidPasswordTask) with: _taskAssembly.buildRemindPasswordTask];
                [definition injectProperty:@selector(routeToMain) with: _routerAssembly.buildFromLoginToMain];
                [definition injectProperty:@selector(prepareUserWorkspaceTask) with: _taskAssembly.buildInitUserWorkspaceTask];
            }];
}

- (TXLoginNavigationController *)buildLoginNavigation {
    return [TyphoonDefinition withClass:[TXLoginNavigationController class] configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(login) with: self.buildLogin];
            }];
}

- (TXMainViewController *)buildMain {
    return [TyphoonDefinition withClass:[TXMainViewController class] configuration:^(TyphoonDefinition *definition)
            {
                [definition useInitializer:@selector(initWithRear:front:)
                                parameters:^(TyphoonMethod *initializer) {
                                    [initializer injectParameterWith:[self buildSideMenu]];
                                    [initializer injectParameterWith:[self buildOrderNavigation]];
                                }];
            }];
}

- (TXOrderNavigationController *)buildOrderNavigation {
    return [TyphoonDefinition withClass:[TXOrderNavigationController class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(makeOrder) with: self.buildMakeOrder];
        [definition injectProperty:@selector(editOrder) with: self.buildEditOrder];
        [definition injectProperty:@selector(placeOnMap) with: self.buildPlaceOnMap];
        
    }];
}

- (TXMakeOrderViewController *)buildMakeOrder {
    return [TyphoonDefinition withClass:[TXMakeOrderViewController class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(currentOrder) with: _dataAssembly.buildCurrentOrder];
        [definition injectProperty:@selector(main) with: self.buildMain];
        [definition injectProperty:@selector(profile) with: self.buildProfile];
        [definition injectProperty:@selector(locationManager) with: _dataAssembly.buildLocationManager];
        [definition injectProperty:@selector(makeOrderTask) with: _taskAssembly.buildMakeOrderTask];
        [definition injectProperty:@selector(cityDAO) with: _dataAssembly.buildCityDAO];
        [definition injectProperty:@selector(userDAO) with: _dataAssembly.buildUserDAO];
        [definition injectProperty:@selector(taskExecutor) with: _coreAssembly.buildTaskExecutor];
        [definition injectProperty:@selector(currentOrderVC) with: self.buildCurrentOrderVC];
    }];
}

- (TXEditOrderViewController *)buildEditOrder {
    return [TyphoonDefinition withClass:[TXEditOrderViewController class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(detailPlace) with: self.buildDetailPlace];
        [definition injectProperty:@selector(editPlace) with: self.buildEditPlace];
    }];
}

- (TXDetailPlaceViewController *)buildDetailPlace {
    return [TyphoonDefinition withClass:[TXDetailPlaceViewController class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(taskExecutor) with: _coreAssembly.buildTaskExecutor];
        [definition injectProperty:@selector(querryLastPlaceTask) with: _taskAssembly.buildQueryLastPlaceTask];
        [definition injectProperty:@selector(querryPlaceByKeyTask) with: _taskAssembly.buildQueryPlaceByKeyTask];
    }];
}

- (TXEditPlaceViewController *)buildEditPlace {
    return [TyphoonDefinition withClass:[TXEditPlaceViewController class] configuration:^(TyphoonDefinition *definition)
    {
    
    }];
}

- (TXPlaceOnMapViewController *)buildPlaceOnMap {
    return [TyphoonDefinition withClass:[TXPlaceOnMapViewController class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(locationManager) with: _dataAssembly.buildLocationManager];
        [definition injectProperty:@selector(taskExecutor) with: _coreAssembly.buildTaskExecutor];
        [definition injectProperty:@selector(decodeLocationTask) with: _taskAssembly.buildDecodeLocationTask];
    }];
}

- (TXCurrentOrderViewController *)buildCurrentOrderVC {
    return [TyphoonDefinition withClass:[TXCurrentOrderViewController class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(taskExecutor) with: _coreAssembly.buildTaskExecutor];
        [definition injectProperty:@selector(queryDetailOrderTask) with: _taskAssembly.buildQueryDetailOrderTask];
        [definition injectProperty:@selector(cancelOrderTask) with: _taskAssembly.buildCancelOrderTask];
        
    }];
}

- (TXProfileViewController *)buildProfile {
    return [TyphoonDefinition withClass:[TXProfileViewController class] configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(main) with: self.buildMain];
                [definition injectProperty:@selector(taskExecutor) with: _coreAssembly.buildTaskExecutor];
                [definition injectProperty:@selector(userDAO) with: _dataAssembly.buildUserDAO];
                [definition injectProperty:@selector(changePasswordTask) with: _taskAssembly.buildChangePasswordTask];
            }];
}

- (TXSelectCityViewController *)buildSelectCity {
    return [TyphoonDefinition withClass:[TXSelectCityViewController class] configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(taskExecutor) with: _coreAssembly.buildTaskExecutor];
                [definition injectProperty:@selector(loadCityTask) with: _taskAssembly.buildLoadCityTask];
                [definition injectProperty:@selector(cityDAO) with: _dataAssembly.buildCityDAO];
                [definition injectProperty:@selector(prepareUserWorkspaceTask) with: _taskAssembly.buildInitUserWorkspaceTask];
            }];
}
- (TXSideMenuViewController *)buildSideMenu {
    return [TyphoonDefinition withClass:[TXSideMenuViewController class] configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(userDAO) with: _dataAssembly.buildUserDAO];
                [definition injectProperty:@selector(cityDAO) with: _dataAssembly.buildCityDAO];
                [definition injectProperty:@selector(taskExecutor) with: _coreAssembly.buildTaskExecutor];
                [definition injectProperty:@selector(routeToSelectCity) with: _routerAssembly.buildFromSideMenuToSelectCity];
                [definition injectProperty:@selector(main) with: self.buildMain];
                [definition injectProperty:@selector(history) with: self.buildHistory];
                [definition injectProperty:@selector(profile) with: self.buildProfile];
                [definition injectProperty:@selector(currentOrderVC) with: self.buildCurrentOrderVC];
                [definition injectProperty:@selector(currentOrder) with: _dataAssembly.buildCurrentOrder];
                [definition injectProperty:@selector(loadHistoryTask) with: _taskAssembly.buildLoadHistoryTask];
                [definition injectProperty:@selector(queryHistoryTask) with: _taskAssembly.buildQueryHistoryTask];
                [definition injectProperty:@selector(makeOrder) with: self.buildMakeOrder];
            }];
}

- (TXHistoryViewController *)buildHistory {
    return [TyphoonDefinition withClass:[TXHistoryViewController class] configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(taskExecutor) with: _coreAssembly.buildTaskExecutor];
                [definition injectProperty:@selector(loadHistoryTask) with: _taskAssembly.buildLoadHistoryTask];
                [definition injectProperty:@selector(queryHistoryTask) with: _taskAssembly.buildQueryHistoryTask];
                [definition injectProperty:@selector(deleteHistoryTask) with: _taskAssembly.buildDeleteHistoryTask];
                [definition injectProperty:@selector(currentOrder) with: _dataAssembly.buildCurrentOrder];
                [definition injectProperty:@selector(main) with: self.buildMain];
            }];
}

@end
