////////////////////////////////////////////////////////////////////////////////
//
//  TXViewControllerAssembly.h
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "Typhoon.h"


@class TXCoreAssembly;
@class TXDataAssembly;
@class TXRouterAssembly;
@class TXTaskAssembly;

@class TXLaunchViewController;
@class TXLoginNavigationController;
@class TXLoginViewController;

@class TXMainViewController;
@class TXSideMenuViewController;

@class TXOrderNavigationController;
@class TXMakeOrderViewController;
@class TXEditOrderViewController;
@class TXDetailPlaceViewController;
@class TXEditPlaceViewController;
@class TXPlaceOnMapViewController;
@class TXCurrentOrderViewController;

@class TXProfileViewController;
@class TXSelectCityViewController;
@class TXHistoryViewController;


@interface TXViewControllerAssembly : TyphoonAssembly

@property(nonatomic, strong, readonly) TXCoreAssembly *coreAssembly;
@property(nonatomic, strong, readonly) TXDataAssembly *dataAssembly;
@property(nonatomic, strong, readonly) TXRouterAssembly *routerAssembly;
@property(nonatomic, strong, readonly) TXTaskAssembly *taskAssembly;

- (TXLaunchViewController *)buildLaunch;
- (TXLoginViewController *)buildLogin;
- (TXLoginNavigationController *)buildLoginNavigation;

- (TXMainViewController *)buildMain;
- (TXSideMenuViewController *)buildSideMenu;

- (TXOrderNavigationController *)buildOrderNavigation;
- (TXMakeOrderViewController *)buildMakeOrder;
- (TXEditOrderViewController *)buildEditOrder;
- (TXDetailPlaceViewController *)buildDetailPlace;
- (TXEditPlaceViewController *)buildEditPlace;
- (TXPlaceOnMapViewController *)buildPlaceOnMap;
- (TXCurrentOrderViewController *)buildCurrentOrderVC;

- (TXProfileViewController *)buildProfile;
- (TXSelectCityViewController *)buildSelectCity;
- (TXHistoryViewController *)buildHistory;


@end
