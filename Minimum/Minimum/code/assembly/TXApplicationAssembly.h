////////////////////////////////////////////////////////////////////////////////
//
//  TXApplicationAssembly.h
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "Typhoon.h"


@class AppDelegate;
@class TXCoreAssembly;
@class TXDataAssembly;
@class TXViewControllerAssembly;


@interface TXApplicationAssembly : TyphoonAssembly

@property(nonatomic, strong, readonly) TXCoreAssembly *coreAssembly;
@property(nonatomic, strong, readonly) TXDataAssembly *dataAssembly;
@property(nonatomic, strong, readonly) TXViewControllerAssembly *vcAssembly;

- (AppDelegate *)appDelegate;
- (UIWindow *)mainWindow;

@end
