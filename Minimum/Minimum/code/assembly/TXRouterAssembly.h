////////////////////////////////////////////////////////////////////////////////
//
//  TXRouterAssembly.h
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "Typhoon.h"

@class TXViewControllerAssembly;

@protocol TXRouter;


@interface TXRouterAssembly : TyphoonAssembly

@property(nonatomic, strong, readonly) TXViewControllerAssembly *vcAssembly;

- (id<TXRouter>)buildFromLaunchToLogin;
- (id<TXRouter>)buildFromLaunchToMain;
- (id<TXRouter>)buildFromLoginToMain;
- (id<TXRouter>)buildFromSideMenuToSelectCity;

@end
