//
//  TXApplicationAssembly.m
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//

#import "AppDelegate.h"
#import "TXApplicationAssembly.h"
#import "TXCoreAssembly.h"
#import "TXDataAssembly.h"
#import "TXViewControllerAssembly.h"

@implementation TXApplicationAssembly

//-------------------------------------------------------------------------------------------
#pragma mark - Interface Methods
//-------------------------------------------------------------------------------------------

- (AppDelegate *)appDelegate {
    return [TyphoonDefinition withClass:[AppDelegate class] configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(launch) with:[_vcAssembly buildLaunch]];
                [definition injectProperty:@selector(window) with:[self mainWindow]];
            }];
}

- (UIWindow *)mainWindow
{
    return [TyphoonDefinition withClass:[UIWindow class] configuration:^(TyphoonDefinition *definition)
            {
                [definition useInitializer:@selector(initWithFrame:) parameters:^(TyphoonMethod *initializer)
                 {
                     [initializer injectParameterWith:[NSValue valueWithCGRect:[[UIScreen mainScreen] bounds]]];
                 }];
            }];
}

@end
