//
//  TXLocation.m
//  Minimum
//
//  Created by Nail Sharipov on 30/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//

#import "TXLocation.h"

@implementation TXLocation

-(NSString *)streetName {
//    return _street;
    return [self relaceReservedWords:_street];
}

- (NSString *) relaceReservedWords: (NSString *) original {
    NSString *shortName = [NSString stringWithString:original];
    
    shortName = [shortName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    shortName = [shortName stringByReplacingOccurrencesOfString:@"улица" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"улица" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"площадь" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"проспект" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"набережная" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"бульвар" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"просек" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"переулок" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"шоссе" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"аллея" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"тупик" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"холм" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"проезд" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"район" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"метро" withString:@""];
    
    shortName = [shortName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    return shortName;
}

@end
