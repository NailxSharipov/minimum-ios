////////////////////////////////////////////////////////////////////////////////
//
//  TXLocationManager.h
//  Minimum
//
//  Created by Nail Sharipov on 30/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXPlace.h"
#import "TXDecodeLocationTask.h"

@protocol TXTaskExecutor;

@protocol TXLocationManagerDelegate <NSObject>

@required

- (void)onLocationManagerSuccess:(TXPlace *)place;
- (void)onLocationManagerError:(NSString *)error;

@end

@interface TXLocationManager : NSObject

@property (nonatomic, readonly) double latitude;
@property (nonatomic, readonly) double longitude;
@property (strong, nonatomic, readonly) TXPlace *place;

@property (strong, nonatomic) TXDecodeLocationTask *decodeLocationTask;
@property (strong, nonatomic) id<TXTaskExecutor> taskExecutor;

@property (weak, nonatomic) id<TXLocationManagerDelegate> delegate;

- (void)checkAccess;

@end
