//
//  TXLocation.h
//  Minimum
//
//  Created by Nail Sharipov on 30/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TXLocation : NSObject

@property(strong, nonatomic) NSString* number;
@property(strong, nonatomic) NSString* streetType;
@property(strong, nonatomic) NSString* street;
@property(nonatomic) double lat;
@property(nonatomic) double lng;

- (NSString *) streetName;

@end
