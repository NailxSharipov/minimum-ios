////////////////////////////////////////////////////////////////////////////////
//
//  TXLocationManager.m
//  Minimum
//
//  Created by Nail Sharipov on 30/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXLocationManager.h"
#import "TXTaskExecutor.h"
#import "TXDecodeLocationTask.h"
#import <CoreLocation/CoreLocation.h>

@interface TXLocationManager () <CLLocationManagerDelegate, TXDecodeLocationTaskDelegate>

@end

@implementation TXLocationManager {
    CLLocationManager *_locationManager;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Initialization & Destruction
//-------------------------------------------------------------------------------------------

- (instancetype)init {
    self = [super init];
    if (self != nil) {
        if ([CLLocationManager locationServicesEnabled]) {
            _latitude = 0.0;
            _longitude = 0.0;
            _locationManager = [CLLocationManager new];
            _locationManager.delegate = self;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        }
    }
    return self;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <CLLocationManagerDelegate>
//-------------------------------------------------------------------------------------------

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [_locationManager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    double latitude = location.coordinate.latitude;
    double longitude = location.coordinate.longitude;

    if (fabs(_latitude - latitude) > 0.001 || fabs(_longitude - longitude) > 0.001) {
        _latitude = latitude;
        _longitude = longitude;
        [self decodePlace];
    } else if (_place == nil) {
        [self decodePlace];    
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"GeoLocation fail %@", error);
}

- (void)checkAccess {
    CLAuthorizationStatus status = kCLAuthorizationStatusNotDetermined;
    if ([CLLocationManager locationServicesEnabled] && status == kCLAuthorizationStatusNotDetermined) {
        [_locationManager requestWhenInUseAuthorization];
    }
}

//-------------------------------------------------------------------------------------------
#pragma mark - <TXDecodeLocationTaskDelegate>
//-------------------------------------------------------------------------------------------

- (void)onDecodeLocationTaskSuccess:(TXPlace *)place {
    NSLog(@"onDecodeLocationTaskSuccess %@", place );
    _place = place;
    [_delegate onLocationManagerSuccess:_place];
}

- (void)onDecodeLocationTaskError:(NSString *)error {
    NSLog(@"onDecodeLocationTaskError");
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (void)decodePlace {
    _decodeLocationTask.delegate = self;
    _decodeLocationTask.latitude = _latitude;
    _decodeLocationTask.longitude = _longitude;
    [_taskExecutor executeTask:_decodeLocationTask];
}

@end
