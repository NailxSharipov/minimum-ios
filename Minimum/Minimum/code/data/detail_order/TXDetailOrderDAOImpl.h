////////////////////////////////////////////////////////////////////////////////
//
//  TXDetailOrderDAOImpl.h
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXDetailOrderDAO.h"
#import "TXPlaceDAO.h"

@interface TXDetailOrderDAOImpl : NSObject<TXDetailOrderDAO>

@property (weak, nonatomic) SDDatabaseManager *databaseManager;

@end
