////////////////////////////////////////////////////////////////////////////////
//
//  TXDetailOrderShema.h
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "SDShema.h"

@class TXDetailOrder;

@interface TXDetailOrderShema : NSObject<SDShema>

@property (weak, nonatomic) SDDatabaseManager *databaseManager;

- (TXDetailOrder *)instantiateModel:(FMResultSet *) result;


@end