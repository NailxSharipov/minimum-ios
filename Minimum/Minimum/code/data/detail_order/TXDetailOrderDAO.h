//
//  TXDetailOrderDAO.h
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXDetailOrder.h"
#import "TXPlace.h"
#import "SDStore.h"

@protocol TXDetailOrderDAO<SDBaseDAO>

@required

- (NSArray<TXDetailOrder *> *)queryAll;

- (BOOL)save:(TXDetailOrder *)detailOrder;

- (BOOL)saveList:(NSArray<TXDetailOrder *> *)detailOrderList;

- (BOOL)removeAllWithDateYongerThen:(NSUInteger)date;


@end
