////////////////////////////////////////////////////////////////////////////////
//
//  TXDetailOrderShema.m
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXDetailOrderShema.h"
#import "TXDetailOrder.h"
#import "SDDatabaseManager.h"
#import "FMDB.h"

@implementation TXDetailOrderShema

NSString* const TXDetailOrderName = @"detail_order";

- (NSString *)name {
    return TXDetailOrderName;
}

- (NSString *)table {
    return [_databaseManager tableName:TXDetailOrderName];
}


- (void)creatShemaVersion:(NSUInteger)version {
    NSString*sql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ ("
                    "orderId                  TEXT NOT NULL PRIMARY KEY,"
                    "dateTimeStamp            INTEGER DEFAULT 0,"
                    "jsonPlaceList            TEXT DEFAULT NULL,"
                    "status                   TEXT DEFAULT NULL,"
                    "code                     INTEGER DEFAULT 0,"
                    "car                      TEXT DEFAULT NULL,"
                    "carNumber                TEXT DEFAULT NULL,"
                    "phone                    TEXT DEFAULT NULL,"
                    "price                    TEXT DEFAULT NULL"
                    ");", self.table];


    [_databaseManager.database executeUpdate:sql];
}

- (void)dropShemaVersion:(NSUInteger)version {
    NSString *sql = [NSString stringWithFormat:@"DROP TABLE IF EXISTS %@", self.table];
    [_databaseManager.database executeUpdate:sql];
}

- (TXDetailOrder *)instantiateModel:(FMResultSet *) result {
    TXDetailOrder *detailOrder = [TXDetailOrder new];
    detailOrder.orderId = [result stringForColumn:@"orderId"];
    detailOrder.dateTimeStamp = [result intForColumn:@"dateTimeStamp"];
    NSString *jsonPlaceList = [result stringForColumn:@"jsonPlaceList"];
    detailOrder.status = [result stringForColumn:@"status"];
    detailOrder.code = [result intForColumn:@"code"];
    detailOrder.car = [result stringForColumn:@"car"];
    detailOrder.carNumber = [result stringForColumn:@"carNumber"];
    detailOrder.phone = [result stringForColumn:@"phone"];
    detailOrder.price = [result stringForColumn:@"price"];
    
    NSData *jsonData = [jsonPlaceList dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *placeList = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
    detailOrder.placeList = placeList;
    
    return detailOrder;
}

@end