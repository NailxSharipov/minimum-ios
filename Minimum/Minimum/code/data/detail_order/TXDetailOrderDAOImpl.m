////////////////////////////////////////////////////////////////////////////////
//
//  TXDetailOrderDAOImpl.m
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXDetailOrderDAOImpl.h"
#import "TXDetailOrderShema.h"
#import "TXPlaceDAOImpl.h"
#import "SDStore.h"
#import "FMDB.h"

typedef NS_ENUM(NSUInteger, OrderSQLIndex) {
    queryAll = 0,
    queryCode = 1,
    save = 2,
    syncDelete = 3,
    deleteDetailOrder = 4,
    last = 5
};

@implementation TXDetailOrderDAOImpl {
    NSMutableArray *_sql;
}

- (instancetype)init {
    self = [super init];
    if (self != nil) {
        NSUInteger count = last - 1;
        _sql = [[NSMutableArray alloc] initWithCapacity:count];
        for (NSUInteger i = 0;  i < count; i++) {
            [_sql addObject:[NSNull null]];
        }
    }
    return self;
}

- (TXOrder *)queryCurrent {
    return nil;
}

- (NSArray<TXDetailOrder *> *)queryAll {
    TXDetailOrderShema *shema = [_databaseManager shemaForClass:TXDetailOrderShema.class];
    if (_sql[queryAll] == [NSNull null]) {
        _sql[queryAll] = [NSString stringWithFormat:
                               @"SELECT * FROM %@ "
                               "ORDER BY dateTimeStamp DESC "
                               , shema.table];
    }
    
    FMDatabase *db = _databaseManager.database;
    FMResultSet *s = [db executeQuery:_sql[queryAll]];
    
    NSMutableArray<TXDetailOrder *> *detailOrderList = [NSMutableArray new];
    
    while ([s next]) {
        TXDetailOrder *detailOrder = [shema instantiateModel:s];
        [detailOrderList addObject:detailOrder];
    }
    
    return detailOrderList;
}

- (BOOL)save:(TXDetailOrder *)detailOrder {
    FMDatabase *db = _databaseManager.database;
    
    if (_sql[save] == [NSNull null]) {
        TXDetailOrderShema *shema = [_databaseManager shemaForClass:TXDetailOrderShema.class];
        _sql[save] = [NSString stringWithFormat:
                      @"INSERT OR REPLACE INTO %@ ("
                      "orderId,"
                      "dateTimeStamp,"
                      "jsonPlaceList,"
                      "status,"
                      "code,"
                      "car,"
                      "carNumber,"
                      "phone,"
                      "price"
                      ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", shema.table];
        _sql[queryCode] = [NSString stringWithFormat:
                           @"SELECT code, dateTimeStamp FROM %@ "
                           "WHERE orderId = ?", shema.table];
    }
    
    FMResultSet *s = [db executeQuery:_sql[queryCode], detailOrder.orderId];
    BOOL shouldSave = NO;
    if ([s next]) {
        NSUInteger code = [s intForColumn:@"code"];
        if (code < detailOrder.code) {
            shouldSave = YES;
            NSUInteger dateTimeStamp = [s intForColumn:@"dateTimeStamp"];
            if (detailOrder.dateTimeStamp == 0 && dateTimeStamp != 0) {
                detailOrder.dateTimeStamp = dateTimeStamp;
            }
        }
    } else {
        NSDate *now = [NSDate date];
        NSUInteger nowTimeStamp = [SDSQLiteTypeConverter dateToTimeStamp:now];
        detailOrder.dateTimeStamp = nowTimeStamp;
        shouldSave = YES;
    }
    
    BOOL success = YES;
    if(shouldSave) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:detailOrder.placeList options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonPlaceList = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        success = [db executeUpdate:_sql[save],
                   detailOrder.orderId,
                   @(detailOrder.dateTimeStamp),
                   jsonPlaceList,
                   detailOrder.status,
                   @(detailOrder.code),
                   detailOrder.car,
                   detailOrder.carNumber,
                   detailOrder.phone,
                   detailOrder.price
                   ];
    }

    return success;
}

- (BOOL)saveList:(NSArray<TXDetailOrder *> *)detailOrderList {
    BOOL success = YES;
    for (TXDetailOrder *detailOrder in detailOrderList) {
        success = success & [self save: detailOrder];
    }
    return success;
}

- (BOOL)removeAllWithDateYongerThen:(NSUInteger)date {
    if (_sql[syncDelete] == [NSNull null]) {
        TXDetailOrderShema *shema = [_databaseManager shemaForClass:TXDetailOrderShema.class];
        _sql[syncDelete] = [NSString stringWithFormat:
                            @"DELETE FROM %@ "
                            "WHERE dateTimeStamp < ?"
                            , shema.table];
    }
    
    FMDatabase *db = _databaseManager.database;
    BOOL success = [db executeUpdate:_sql[syncDelete], @(date)];

    return success;
}


@end
