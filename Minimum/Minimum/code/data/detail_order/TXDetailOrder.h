////////////////////////////////////////////////////////////////////////////////
//
//  TXDetailOrder.h
//  Minimum
//
//  Created by Nail Sharipov on 31/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

@class TXPlace;

@interface TXDetailOrder : NSObject

@property (strong, nonatomic) NSString *orderId;
@property (strong, nonatomic) NSArray<NSString *> *placeList;

@property (nonatomic) NSUInteger dateTimeStamp;
@property (strong, nonatomic) NSString *status;
@property (nonatomic) NSUInteger code;
@property (strong, nonatomic) NSString *car;
@property (strong, nonatomic) NSString *carNumber;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *price;


@end