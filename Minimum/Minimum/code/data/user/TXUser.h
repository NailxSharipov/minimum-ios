////////////////////////////////////////////////////////////////////////////////
//
//  TXUser.h
//  data
//
//  Created by Шарипов Н.Г. on 15/12/15.
//  Copyright © 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

@class TXCity;

@interface TXUser : NSObject

@property(nonatomic, strong) NSString *phone;
@property(nonatomic, strong) NSString *secondPhone;


@end