////////////////////////////////////////////////////////////////////////////////
//
//  TXUserDAO.h
//  data
//
//  Created by Шарипов Н.Г. on 15/12/15.
//  Copyright © 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXUser.h"


@protocol TXUserDAO <NSObject>

@property (nonatomic, weak, readonly) TXUser *actualUser;

- (TXUser *)createWithPhone:(NSString *)phone;

- (void)save;

- (NSString *)getPassword;

- (void)savePassword:(NSString *)password;

- (void)deletePassword;


@end