////////////////////////////////////////////////////////////////////////////////
//
//  TXUserDAO.m
//  data
//
//  Created by Шарипов Н.Г. on 15/12/15.
//  Copyright © 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXUserDAOImpl.h"
#import "FDKeychain.h"


@interface TXUserDAOImpl () {
    TXUser *_actualUser;
}

@end

@implementation TXUserDAOImpl

NSString *TXUSER_SERVICE_NAME   = @"com.weakteam.taxi.data";
NSString *TXUSER_PHONE          = @"phoneNumber";
NSString *TXUSER_SECOND_PHONE   = @"secondPhoneNumber";
NSString *TXUSER_PASSWORD       = @"password";

//-------------------------------------------------------------------------------------------
#pragma mark - <SDUserDAO>
//-------------------------------------------------------------------------------------------

- (TXUser *)actualUser {
    if (_actualUser == nil) {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *phone = [prefs objectForKey:TXUSER_PHONE];
        
        if (phone != nil && phone.length > 0) {
            _actualUser = [[TXUser alloc] init];
            _actualUser.phone = phone;
            _actualUser.secondPhone = [prefs objectForKey:TXUSER_SECOND_PHONE];
        }
    }
    
    return _actualUser;
}

- (TXUser *)createWithPhone:(NSString *)phone {
    if (phone != nil) {
        _actualUser = [[TXUser alloc] init];
        _actualUser.phone = phone;
        [self save];
    }
    return _actualUser;
}

- (void)save {
    if (_actualUser != nil && _actualUser.phone != nil && _actualUser.phone.length > 0) {
        [[NSUserDefaults standardUserDefaults] setObject:_actualUser.phone forKey:TXUSER_PHONE];
        if (_actualUser.secondPhone != nil) {
            [[NSUserDefaults standardUserDefaults] setObject:_actualUser.secondPhone forKey:TXUSER_SECOND_PHONE];
        } else {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:TXUSER_SECOND_PHONE];
        }
    }
}

- (NSString *)getPassword {
    NSString *phone = _actualUser.phone;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *password = [prefs objectForKey:TXUSER_PASSWORD];
    if (phone != nil && phone.length > 0) {
        password = [prefs objectForKey:TXUSER_PASSWORD];
        password = [FDKeychain itemForKey:TXUSER_PASSWORD
                               forService:TXUSER_SERVICE_NAME
                                    error:nil];
    }
    return password;
}

- (void)savePassword:(NSString *)password {
    NSString *phone = _actualUser.phone;
    if (phone != nil && phone.length > 0) {
        if (password != nil) {
            [FDKeychain saveItem:password
                          forKey:TXUSER_PASSWORD
                      forService:TXUSER_SERVICE_NAME
                           error:nil];
        } else {
            [FDKeychain deleteItemForKey:TXUSER_PASSWORD
                              forService:TXUSER_SERVICE_NAME
                                   error:nil];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs removeObjectForKey:TXUSER_PASSWORD];
        }
    }
}

- (void)deletePassword {
    [self savePassword:nil];
}

@end