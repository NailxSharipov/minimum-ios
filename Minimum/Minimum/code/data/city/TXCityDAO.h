////////////////////////////////////////////////////////////////////////////////
//
//  TXCityDAO.h
//  data
//
//  Created by Шарипов Н.Г. on 15/12/15.
//  Copyright © 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXCity.h"


@protocol TXCityDAO <NSObject>

@property (nonatomic, weak) TXCity *actualCity;
@property (nonatomic, weak, readonly) NSArray<TXCity *> *cityData;

- (NSArray<TXCity *> *)createCityData:(NSString *)data;



@end
