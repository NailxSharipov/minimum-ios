////////////////////////////////////////////////////////////////////////////////
//
//  TXCity.h
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>


@interface TXCity : NSObject

@property (strong, nonatomic) NSString *cityId;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *url;

@property (strong, nonatomic) NSString *brandId;
@property (strong, nonatomic) NSString *brand;
@property (strong, nonatomic) NSString *phonePrefix;
@property (strong, nonatomic) NSString *money;

- (instancetype) initWithDictionary: (NSDictionary *)dictionary;

@end
