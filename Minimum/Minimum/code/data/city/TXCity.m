////////////////////////////////////////////////////////////////////////////////
//
//  TXCity.m
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXCity.h"


@implementation TXCity

//-------------------------------------------------------------------------------------------
#pragma mark - Initialization & Destruction
//-------------------------------------------------------------------------------------------

- (instancetype) initWithDictionary: (NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        {
            NSString *value = [dictionary objectForKey:@"id"];
            if (value != nil) {
                _cityId = value;
            }
        }
        {
            NSString *value = [dictionary objectForKey:@"title"];
            if (value != nil) {
                _title = value;
            }
        }
        {
            NSString *value = [dictionary objectForKey:@"url"];
            if (value != nil) {
                _url = value;
            }
        }
    }
    return self;
}

@end
