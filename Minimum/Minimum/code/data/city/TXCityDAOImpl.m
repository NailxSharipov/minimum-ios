////////////////////////////////////////////////////////////////////////////////
//
//  TXCityDAOImpl.m
//  Minimum
//
//  Created by Nail Sharipov on 07/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXCityDAOImpl.h"

#import "TXCity.h"


@implementation TXCityDAOImpl {
    TXCity *_actualCity;
    NSArray<TXCity *> *_cityData;
}

NSString *TXCITY_CITY_ID = @"cityId";

//-------------------------------------------------------------------------------------------
#pragma mark - <TXCityDAO>
//-------------------------------------------------------------------------------------------

- (TXCity *)actualCity {
    if (_actualCity == nil) {
        NSArray<TXCity *> *cityData = self.cityData;
        if (cityData != nil) {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *cityId = [prefs objectForKey:TXCITY_CITY_ID];
            if (cityId != nil) {
                for (TXCity *elmCity in cityData) {
                    if ([elmCity.cityId isEqual: cityId]) {
                        _actualCity = elmCity;
                        break;
                    }
                }
            }
        }
    }
    return _actualCity;
}

- (void)setActualCity:(TXCity *)city {
    if (city != nil) {
        _actualCity = city;
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:city.cityId forKey:TXCITY_CITY_ID];
    }
}

- (NSArray<TXCity *> *)createCityData:(NSString *)data {
    [data writeToURL:[self cityFileUrl]
          atomically:YES
            encoding:NSUTF8StringEncoding
               error:nil];
    _cityData = [self parseCityDataFromText:data];
    return _cityData;
}

- (NSArray<TXCity *> *)cityData {
    if (_cityData == nil) {
        NSString *data = [[NSString alloc] initWithContentsOfURL:[self cityFileUrl]
                                                        encoding:NSUTF8StringEncoding
                                                           error:nil];
        _cityData = [self parseCityDataFromText:data];
    }
    return _cityData;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (NSURL *)cityFileUrl {
    return [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"city.txt"]];
}

- (NSArray<TXCity *> *)parseCityDataFromText: (NSString *) text {
    NSData *data = [text dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&error];
    NSMutableArray *cityData = [[NSMutableArray alloc] init];
    for (NSDictionary *cityDict in jsonArray) {
        TXCity* city = [[TXCity alloc] init];
        city.cityId = cityDict[@"town_id"];
        city.title = cityDict[@"title"];
        city.brandId = cityDict[@"id"];
        city.brand = cityDict[@"name"];
        city.url = cityDict[@"url"];
        city.phonePrefix = cityDict[@"telpref"];
        NSString *money = cityDict[@"money"];
        if ([money isEqualToString: @"{rubles}"]) {
            money = @"рублей";
        }
        city.money = money;
        
        if (![self isCityData:cityData containCity: city]) {
            [cityData addObject:city];
        }
    }
    
    
    return cityData;
}

- (BOOL)isCityData:(NSArray<TXCity *> *)cityData containCity:(TXCity *)city {
    BOOL contain = NO;
    for (TXCity *elm in cityData) {
        if ([elm.cityId isEqual:city.cityId]) {
            contain = YES;
            break;
        }
    }
    return contain;
}


@end
