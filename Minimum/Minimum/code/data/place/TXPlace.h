////////////////////////////////////////////////////////////////////////////////
//
//  TXPlace.h
//  Minimum
//
//  Created by Nail Sharipov on 11/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "SDModel.h"

@interface TXPlace : SDModel

@property (strong, nonatomic) NSString *placeId;   // in our database
@property (strong, nonatomic) NSString *type;      // 1-улицы, 2- остановки, 3-прочие объекты, 4-коментарии
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSString *home;
@property (strong, nonatomic) NSString *definition;

// not stored
@property (nonatomic) BOOL isFrom;

- (NSString *)address;
- (NSString *)typeName;

- (BOOL)isStreet;
- (BOOL)isStation;
- (BOOL)isObject;

- (NSString *)action;

- (BOOL)isCorect;

- (void)clear;

- (void)setWithPlace:(TXPlace *)place;

- (BOOL)isEmpty;

@end
