////////////////////////////////////////////////////////////////////////////////
//
//  TXPlaceDAOImpl.m
//  Minimum
//
//  Created by Nail Sharipov on 12/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXPlaceDAOImpl.h"
#import "TXPlaceShema.h"
#import "TXOrderShema.h"
#import "TXOrder.h"
#import "FMDB.h"

typedef NS_ENUM(NSUInteger, SQLIndex) {
    queryForOrder = 0,
    queryLast = 1,
    queryByKey = 2,
    queryByKeyAndType = 3,
    save = 4,
    saveNilDefinition = 5,
    last = 6
};


@implementation TXPlaceDAOImpl {
    NSMutableArray *_sql;
}

- (instancetype)init {
    self = [super init];
    if (self != nil) {
        NSUInteger count = last - 1;
        _sql = [[NSMutableArray alloc] initWithCapacity:count];
        for (NSUInteger i = 0;  i < count; i++) {
            [_sql addObject:[NSNull null]];
        }
    }
    return self;
}

- (NSArray<TXPlace *> *)queryFor:(TXOrder *)order {
    return [self queryForOrderId:order.orderId];
}

- (NSArray<TXPlace *> *)queryForOrderId:(NSString *)orderId {
    TXPlaceShema *shema = [_databaseManager shemaForClass:TXPlaceShema.class];
    if (_sql[queryForOrder] == [NSNull null]) {
        TXOrderShema *orderShema = [_databaseManager shemaForClass:TXOrderShema.class];
        _sql[queryForOrder] = [NSString stringWithFormat:
                               @"SELECT p.placeId, p.type, p.name, p.home, p.definition FROM %@ p "
                               "INNER JOIN %@ op ON p.placeId = op.placeId "
                               "WHERE op.orderId = ? "
                               "ORDER BY op.position ASC", shema.table, orderShema.tableOrderToPlace];
    }
    
    FMDatabase *db = _databaseManager.database;
    FMResultSet *s = [db executeQuery:_sql[queryForOrder], orderId];
    
    NSMutableArray<TXPlace *> *placeList = [NSMutableArray new];
    while ([s next]) {
        TXPlace *place = [shema instantiateModel:s];
        [placeList addObject:place];
    }
    
    return placeList;
}

- (NSArray<TXPlace *> *)queryLast {
    TXPlaceShema *shema = [_databaseManager shemaForClass:TXPlaceShema.class];
    if (_sql[queryLast] == [NSNull null]) {
        TXOrderShema *orderShema = [_databaseManager shemaForClass:TXOrderShema.class];
        _sql[queryLast] = [NSString stringWithFormat:
                           @"SELECT DISTINCT p.placeId, p.type, p.name, p.home, p.definition FROM %@ p "
                           "INNER JOIN %@ op ON p.placeId = op.placeId "
                           "INNER JOIN %@ o ON op.orderId = o.orderId "
                           "ORDER BY o.dateTimeStamp DESC"
                           , shema.table, orderShema.tableOrderToPlace, orderShema.table];
    }
    
    FMDatabase *db = _databaseManager.database;
    FMResultSet *s = [db executeQuery:_sql[queryLast]];
    
    NSMutableArray<TXPlace *> *placeList = [NSMutableArray new];
    while ([s next]) {
        TXPlace *place = [shema instantiateModel:s];
        [placeList addObject:place];
    }
    
    return placeList;
}

- (NSArray<TXPlace *> *)queryByKey:(NSString *)key {
    TXPlaceShema *shema = [_databaseManager shemaForClass:TXPlaceShema.class];
    if (_sql[queryByKey] == [NSNull null]) {
        _sql[queryByKey] = [NSString stringWithFormat:
                               @"SELECT placeId, type, name, home, definition FROM %@ "
                               "WHERE lowname LIKE printf('%%%%%%s%%%%', ?) ORDER BY type "
                               "LIMIT 0, 40"
                               , shema.table];
    }
    
    FMDatabase *db = _databaseManager.database;
    NSString *lowKey = [key lowercaseString];
    FMResultSet *s = [db executeQuery:_sql[queryByKey], lowKey];
    
    NSMutableArray<TXPlace *> *placeList = [NSMutableArray new];
    while ([s next]) {
        TXPlace *place = [shema instantiateModel:s];
        [placeList addObject:place];
    }
    
    return placeList;
}

- (TXPlace *)queryByKey:(NSString *)key andObjectType:(NSNumber *)objectType {
    TXPlaceShema *shema = [_databaseManager shemaForClass:TXPlaceShema.class];
    if (_sql[queryByKeyAndType] == [NSNull null]) {
        _sql[queryByKeyAndType] = [NSString stringWithFormat:
                            @"SELECT placeId, type, name, home, definition FROM %@ "
                            "WHERE type=? AND lowname LIKE printf('%%%%%%s%%%%', ?) "
                            "LIMIT 0, 1"
                            , shema.table];
    }
    
    FMDatabase *db = _databaseManager.database;
    NSString *lowKey = [key lowercaseString];
    FMResultSet *s = [db executeQuery:_sql[queryByKeyAndType], objectType, lowKey];
    
    TXPlace *place = nil;
    if ([s next]) {
        place = [shema instantiateModel:s];
    }
    [s close];
    
    return place;
}

- (BOOL)save:(TXPlace *)place {
    return [self saveList:@[place]];
}

- (BOOL)saveList:(NSArray<TXPlace *> *)placeList {
    FMDatabase *db = _databaseManager.database;
    
    if (_sql[save] == [NSNull null]) {
        TXPlaceShema *shema = [_databaseManager shemaForClass:TXPlaceShema.class];
        
        _sql[save] = [NSString stringWithFormat:
                      @"INSERT OR REPLACE INTO %@ ("
                      "placeId,"
                      "type,"
                      "name,"
                      "lowname,"
                      "home,"
                      "definition"
                      ") VALUES (?, ?, ?, ?, ?, ?)", shema.table];
        
        _sql[saveNilDefinition] = [NSString stringWithFormat:
                                   @"INSERT OR REPLACE INTO %@ ("
                                   "placeId,"
                                   "type,"
                                   "name,"
                                   "lowname,"
                                   "home"
                                   ") VALUES (?, ?, ?, ?, ?)", shema.table];
    }
    
    BOOL success = YES;
    
    for (TXPlace *place in placeList) {
        NSString *key = nil;
        if (place.key == nil) {
            key = [place.name lowercaseString];
        } else {
            key = [NSString stringWithFormat:@"%@ %@", [place.name lowercaseString], [place.key lowercaseString]];
        }
        if (place.definition != nil) {
            success = success && [db executeUpdate:_sql[save], place.placeId, place.type, place.name, key, place.home, place.definition];
        } else {
            success = success && [db executeUpdate:_sql[saveNilDefinition], place.placeId, place.type, place.name, key, place.home];
        }
    }
    
    return success;
}

@end
