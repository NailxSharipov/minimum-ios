////////////////////////////////////////////////////////////////////////////////
//
//  TXPlaceShema.h
//  Minimum
//
//  Created by Nail Sharipov on 11/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "SDShema.h"
#import "TXPlace.h"

@interface TXPlaceShema : NSObject<SDShema>

@property (weak, nonatomic) SDDatabaseManager *databaseManager;

- (TXPlace *)instantiateModel:(FMResultSet *) result;

@end
