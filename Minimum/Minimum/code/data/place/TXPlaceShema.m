////////////////////////////////////////////////////////////////////////////////
//
//  TXPlaceShema.m
//  Minimum
//
//  Created by Nail Sharipov on 11/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////


#import "TXPlaceShema.h"
#import "SDDatabaseManager.h"
#import "FMDB.h"

NSString* const TXPlaceName = @"place";

@implementation TXPlaceShema

- (NSString *)name {
    return TXPlaceName;
}

- (NSString *)table {
    return [_databaseManager tableName:TXPlaceName];
}

- (void)creatShemaVersion:(NSUInteger)version {
    NSArray *sql = @[
                     [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ ("
                      "placeId                               TEXT NOT NULL PRIMARY KEY,"
                      "type                                  TEXT DEFAULT NULL,"
                      "name                                  TEXT DEFAULT NULL,"
                      "lowname                               TEXT DEFAULT NULL,"
                      "home                                  TEXT DEFAULT NULL,"
                      "definition                            TEXT DEFAULT NULL"
                      ");", self.table],
                     [NSString stringWithFormat:@"CREATE INDEX IF NOT EXISTS %@_idx_type ON %@ (type);", self.name, self.table]
                     ];
    for (NSString *s in sql) {
        [_databaseManager.database executeUpdate:s];
    }
}

- (void)dropShemaVersion:(NSUInteger)version {
    NSString *sql = [NSString stringWithFormat:@"DROP TABLE IF EXISTS %@", self.table];
    [_databaseManager.database executeUpdate:sql];
}

- (TXPlace *)instantiateModel:(FMResultSet *) result {
    TXPlace *place = [TXPlace new];
    
    place.placeId = [result stringForColumn:@"placeId"];
    place.type = [result stringForColumn:@"type"];
    place.name = [result stringForColumn:@"name"];
    place.home = [result stringForColumn:@"home"];
    place.definition = [result stringForColumn:@"definition"];
    
    return place;
}

@end
