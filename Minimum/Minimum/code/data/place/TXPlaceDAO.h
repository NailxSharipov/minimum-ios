////////////////////////////////////////////////////////////////////////////////
//
//  TXPlaceDAO.h
//  Minimum
//
//  Created by Nail Sharipov on 12/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXPlace.h"
#import "SDStore.h"
#import "TXOrder.h"

@protocol TXPlaceDAO<SDBaseDAO>

@required

- (NSArray<TXPlace *> *)queryFor:(TXOrder *)order;

- (NSArray<TXPlace *> *)queryForOrderId:(NSString *)orderId;

- (NSArray<TXPlace *> *)queryLast;

- (NSArray<TXPlace *> *)queryByKey:(NSString *)key;

- (TXPlace *)queryByKey:(NSString *)key andObjectType:(NSNumber *)objectType;

- (BOOL)save:(TXPlace *)place;

- (BOOL)saveList:(NSArray<TXPlace *> *)placeList;

@end
