////////////////////////////////////////////////////////////////////////////////
//
//  TXPlaceDAOImpl.h
//  Minimum
//
//  Created by Nail Sharipov on 12/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXPlaceDAO.h"

@interface TXPlaceDAOImpl : NSObject<TXPlaceDAO>

@property (weak, nonatomic) SDDatabaseManager *databaseManager;

@end
