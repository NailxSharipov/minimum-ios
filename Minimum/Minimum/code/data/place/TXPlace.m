//
//  TXPlace.m
//  Minimum
//
//  Created by Nail Sharipov on 11/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//

#import "TXPlace.h"

@implementation TXPlace

- (NSString *)address {
    NSMutableString *value = nil;
    if (_name != nil) {
        value = [[NSMutableString alloc] initWithString:_name];
        if (_home != nil && ![_home isEqualToString:@"null"]) {
            [value appendString:@" "];
            [value appendString:_home];
        }
        if (_isFrom && _definition != nil && ![_definition isEqualToString:@"null"]) {
            [value appendString:@" "];
            [value appendString:_definition];
        }
    }
    
    return value;
}

- (NSString *)action {
    NSString *result;
    if (_isFrom) {
        result = NSLocalizedString(@"Departure", nil);
    } else {
        result = NSLocalizedString(@"Destination", nil);
    }
    
    return result;
}

- (NSString *)description {
    NSString *value = [self address];
    if (value == nil) {
        value = [self action];;
    }
    
    return value;
}

- (NSString *)typeName {
    NSString *value = nil;
    if (_type == nil) {
        value = NSLocalizedString(@"Anknown", nil);
    } else if ([_type isEqual:@"1"]) {
        value = NSLocalizedString(@"Street", nil);
    } else if ([_type isEqual:@"2"]) {
        value = NSLocalizedString(@"Station", nil);
    } else if ([_type isEqual:@"3"]) {
        value = NSLocalizedString(@"Object", nil);
    }
    return value;
}

- (BOOL)isStreet {
    return _type != nil && [_type isEqual:@"1"];
}

- (BOOL)isStation {
    return _type != nil && [_type isEqual:@"2"];
}

- (BOOL)isObject {
    return _type != nil && [_type isEqual:@"3"];
}

- (BOOL)isCorect {
    BOOL corect = _placeId != nil && _name != nil;
    if (corect && _type != nil && [_type isEqualToString:@"1"]) {
        corect = _home != nil && _home.length > 0;
    }
    return corect;
}

- (void)clear {
    _placeId = nil;
    _type = nil;
    _name = nil;
    _home = nil;
    _definition = nil;
}

- (void)setWithPlace:(TXPlace *)place {
    _placeId = place.placeId;
    _type = place.type;
    _name = place.name;
    _home = place.home;
    _definition = place.definition;
}

- (BOOL)isEmpty {
    return _placeId == nil && _name == nil;
}


@end
