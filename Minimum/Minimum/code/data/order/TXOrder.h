////////////////////////////////////////////////////////////////////////////////
//
//  TXOrder.h
//  Minimum
//
//  Created by Nail Sharipov on 11/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>


@interface TXOrder : NSObject

@property (strong, nonatomic)NSString *orderId;
@property (nonatomic)NSUInteger dateTimeStamp;
@property (nonatomic)NSUInteger syncTimeStamp;
@property (strong, nonatomic)NSArray *placeList;

- (void)setDateFromText:(NSString *)text;

- (NSString *)textDate;

@end
