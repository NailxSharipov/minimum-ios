////////////////////////////////////////////////////////////////////////////////
//
//  TXOrderShema.h
//  Minimum
//
//  Created by Nail Sharipov on 11/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "SDShema.h"
#import "TXOrder.h"


@interface TXOrderShema : NSObject<SDShema>

@property (weak, nonatomic) SDDatabaseManager *databaseManager;

- (TXOrder *)instantiateModel:(FMResultSet *) result;

- (NSString *)tableOrderToPlace;


@end