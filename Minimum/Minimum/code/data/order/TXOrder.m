////////////////////////////////////////////////////////////////////////////////
//
//  TXOrder.m
//  Minimum
//
//  Created by Nail Sharipov on 11/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXOrder.h"
#import "SDSQLiteTypeConverter.h"

NSDateFormatter* TXOrderDateFormat;

@implementation TXOrder

- (void)setDateFromText:(NSString *)text {
    if (text != nil && text.length > 0) {
        //28.08.2014 12:44
        if (TXOrderDateFormat == nil) {
            TXOrderDateFormat = [[NSDateFormatter alloc] init];
            [TXOrderDateFormat setDateFormat:@"dd.MM.yyyy HH:mm"];
        }
        NSDate *date = [TXOrderDateFormat dateFromString:text];
        _dateTimeStamp = [SDSQLiteTypeConverter dateToTimeStamp:date];
    }
}

- (NSString *)textDate {
    if (_dateTimeStamp > 0) {
        NSDate *date = [SDSQLiteTypeConverter timeStampToDate:_dateTimeStamp];
        return [TXOrderDateFormat stringFromDate:date];
    } else {
        return nil;
    }
}

@end
