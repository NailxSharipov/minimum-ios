////////////////////////////////////////////////////////////////////////////////
//
//  TXOrderDAOImpl.h
//  Minimum
//
//  Created by Nail Sharipov on 12/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXOrderDAO.h"
#import "TXPlaceDAO.h"


@interface TXOrderDAOImpl : NSObject<TXOrderDAO>

@property (weak, nonatomic) id<TXPlaceDAO> placeDAO;
@property (weak, nonatomic) SDDatabaseManager *databaseManager;

@end