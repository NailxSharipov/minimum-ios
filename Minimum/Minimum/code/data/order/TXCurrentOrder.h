//
//  TXCurrentOrder.h
//  Minimum
//
//  Created by Nail Sharipov on 23/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TXPlace;
@class TXOrder;

@interface TXCurrentOrder : NSObject

@property (strong, nonatomic, readonly) NSMutableArray<TXPlace *> *placeList;
@property (strong, nonatomic) TXPlace *selectedPlace;
@property (strong, nonatomic) NSString *orderId;

- (TXPlace *)addPlace;

- (void)removePlace:(TXPlace *)place;

- (NSUInteger) getPlaceCount;

- (void)clear;

- (void)setWithOrder:(TXOrder *)order;

- (NSArray<TXPlace *> *)getNotEmptyPlaceList;

- (void)switchPlace:(NSUInteger)index;

- (BOOL)isValid;

@end
