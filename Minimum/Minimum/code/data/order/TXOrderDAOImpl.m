////////////////////////////////////////////////////////////////////////////////
//
//  TXOrderDAOImpl.m
//  Minimum
//
//  Created by Nail Sharipov on 12/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXOrderDAOImpl.h"
#import "TXOrderShema.h"
#import "TXPlaceDAOImpl.h"
#import "SDStore.h"
#import "FMDB.h"

typedef NS_ENUM(NSUInteger, OrderSQLIndex) {
    queryFromDate = 0,
    queryFromIndexSize = 1,
    updateSaveRoot = 2,
    updateSaveOrderToPlace = 3,
    syncDelete = 4,
    deleteOrder = 5,
    deleteOrderToPlace = 6,
    last = 7
};

@implementation TXOrderDAOImpl {
    NSMutableArray *_sql;
}

- (instancetype)init {
    self = [super init];
    if (self != nil) {
        NSUInteger count = last - 1;
        _sql = [[NSMutableArray alloc] initWithCapacity:count];
        for (NSUInteger i = 0;  i < count; i++) {
            [_sql addObject:[NSNull null]];
        }
    }
    return self;
}

- (TXOrder *)queryCurrent {
    return nil;
}

- (NSArray<TXOrder *> *)queryFromDate:(NSUInteger)dateTimeStamp {
    TXOrderShema *shema = [_databaseManager shemaForClass:TXOrderShema.class];
    if (_sql[queryFromDate] == [NSNull null]) {
        _sql[queryFromDate] = [NSString stringWithFormat:
                               @"SELECT * FROM %@ "
                               "WHERE ? <= dateTimeStamp "
                               "ORDER BY dateTimeStamp DESC "
                               , shema.table];
    }
    
    FMDatabase *db = _databaseManager.database;
    FMResultSet *s = [db executeQuery:_sql[queryFromDate], @(dateTimeStamp)];
    
    NSMutableArray<TXOrder *> *orderList = [NSMutableArray new];
    
    while ([s next]) {
        TXOrder *order = [shema instantiateModel:s];
        [orderList addObject:order];
    }
    
    for (TXOrder *order in orderList) {
        order.placeList = [_placeDAO queryFor:order];
    }
    
    return orderList;
}

- (NSArray<TXOrder *> *)queryFromIndex:(NSUInteger)index size:(NSUInteger)size {
    TXOrderShema *shema = [_databaseManager shemaForClass:TXOrderShema.class];
    if (_sql[queryFromIndexSize] == [NSNull null]) {
        _sql[queryFromIndexSize] = [NSString stringWithFormat:
                                    @"SELECT * FROM %@ "
                                    "ORDER BY dateTimeStamp DESC "
                                    "LIMIT ?, ?", shema.table];
    }

    FMDatabase *db = _databaseManager.database;
    FMResultSet *s = [db executeQuery:_sql[queryFromIndexSize], @(index), @(size)];
   
    NSMutableArray<TXOrder *> *orderList = [NSMutableArray new];
    
    while ([s next]) {
        TXOrder *order = [shema instantiateModel:s];
        [orderList addObject:order];
    }
    
    for (TXOrder *order in orderList) {
        order.placeList = [_placeDAO queryFor:order];
    }

    return orderList;
}

- (BOOL)save:(TXOrder *)order {
    FMDatabase *db = _databaseManager.database;

    if (_sql[updateSaveRoot] == [NSNull null]) {
        TXOrderShema *shema = [_databaseManager shemaForClass:TXOrderShema.class];
        
        _sql[updateSaveRoot] = [NSString stringWithFormat:
                                @"INSERT OR REPLACE INTO %@ ("
                                "orderId,"
                                "dateTimeStamp,"
                                "syncTimeStamp"
                                ") VALUES (?, ?, ?)", shema.table];

        _sql[deleteOrderToPlace] = [NSString stringWithFormat:
                                    @"DELETE FROM %@ WHERE orderId = ?", shema.tableOrderToPlace];
        
        _sql[updateSaveOrderToPlace] = [NSString stringWithFormat:
                                        @"INSERT OR REPLACE INTO %@ ("
                                        "orderId,"
                                        "placeId,"
                                        "position"
                                        ") VALUES (?, ?, ?)", shema.tableOrderToPlace];
    }
    
    BOOL success = NO;
    
    if (order.placeList != nil && order.placeList.count > 0) {
        success = [db executeUpdate:_sql[updateSaveRoot], order.orderId, @(order.dateTimeStamp), @(order.syncTimeStamp)];
        success = success && [db executeUpdate:_sql[deleteOrderToPlace], order.orderId];
        NSUInteger count = order.placeList.count;
        for (NSUInteger index = 0; index < count; index++) {
            if (success) {
                TXPlace *place = order.placeList[index];
                success = [db executeUpdate:_sql[updateSaveOrderToPlace], order.orderId, place.placeId, @(index)];
                if (success) {
                    success = [_placeDAO save:place];
                }
            } else {
                break;
            }
        }
    }

    return success;
}

- (BOOL)removeAllWithSyncDateOlderThen:(NSUInteger)syncDate andDateLaterThen:(NSUInteger)date {
    if (_sql[syncDelete] == [NSNull null]) {
        TXOrderShema *shema = [_databaseManager shemaForClass:TXOrderShema.class];
        _sql[syncDelete] = [NSString stringWithFormat:
                            @"SELECT orderId FROM %@ "
                            "WHERE syncTimeStamp < ? AND dateTimeStamp > ?"
                            , shema.table];
    }
    
    FMDatabase *db = _databaseManager.database;
    FMResultSet *s = [db executeQuery:_sql[syncDelete], @(syncDate), @(date)];
    
    NSMutableArray<NSString *> *orderIdList = [NSMutableArray new];
    
    while ([s next]) {
        NSString *orderId = [s stringForColumn:@"orderId"];
        [orderIdList addObject:orderId];
    }
    
    BOOL success = YES;
    
    for (NSString *orderId in orderIdList) {
        success = [self removeByOrderId:orderId];
        if (!success) {
            break;
        }
    }
    
    return success;
}

- (BOOL)remove:(TXOrder *)order {
    return [self removeByOrderId:order.orderId];
}

- (BOOL)removeByOrderId:(NSString *)orderId {
    if (_sql[deleteOrder] == [NSNull null]) {
        TXOrderShema *shema = [_databaseManager shemaForClass:TXOrderShema.class];
        _sql[deleteOrder] = [NSString stringWithFormat:
                             @"DELETE FROM %@ WHERE orderId = ?", shema.table];
        _sql[deleteOrderToPlace] = [NSString stringWithFormat:
                                    @"DELETE FROM %@ WHERE orderId = ?", shema.tableOrderToPlace];
    }
    
    FMDatabase *db = _databaseManager.database;
    BOOL success = [db executeUpdate:_sql[deleteOrder], orderId];
    if (success) {
        success = [db executeUpdate:_sql[deleteOrderToPlace], orderId];
    }
    return success;
}

@end