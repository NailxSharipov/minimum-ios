//
//  TXCurrentOrder.m
//  Minimum
//
//  Created by Nail Sharipov on 23/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//

#import "TXCurrentOrder.h"
#import "TXPlace.h"
#import "TXOrder.h"

@implementation TXCurrentOrder

- (instancetype)init {
    self = [super init];
    if (self) {
        _placeList = [NSMutableArray new];
        [self addPlace];
        [self addPlace];
    }
    return self;
}

- (TXPlace *)addPlace {
    TXPlace *place = [TXPlace new];
    [_placeList addObject:place];
    if (_placeList.count == 1) {
        place.isFrom = YES;
    } else {
        place.isFrom = NO;
    }
    return place;
}


- (void)removePlace:(TXPlace *)place {
    NSInteger index = [self getPlaceIndex:place];
    if (index >= 2) {
        [_placeList removeObjectAtIndex:index];
    } else if (index == 1) {
        if (_placeList.count > 2) {
            [_placeList removeObjectAtIndex:index];
        } else {
            [place clear];
        }
    } else {
        [place clear];
    }
}

- (NSUInteger) getPlaceCount {
    NSUInteger count = 0;
    for (TXPlace *place in _placeList) {
        if (place != nil && [place isEmpty]) {
            count++;
        }
    }
    return count;
}

- (void)clear {
    NSInteger n = _placeList.count;
    for (NSInteger i = n - 1; i >= 0; i--) {
        if (i >= 2) {
            [_placeList removeObjectAtIndex:i];
        } else {
            [_placeList[i] clear];
        }
    }
}

- (void)setWithOrder:(TXOrder *)order {
    [self clear];
    NSUInteger i = 0;
    for(TXPlace *place in order.placeList) {
        if (i >= _placeList.count) {
            [_placeList addObject:place];
        }
        [_placeList[i] setWithPlace:place];
        i++;
    }
}

- (NSArray<TXPlace *> *)getNotEmptyPlaceList {
    NSMutableArray *notEmptyPlaceList = nil;
    if ([self isValid]) {
        notEmptyPlaceList = [[NSMutableArray alloc] initWithCapacity:_placeList.count];
        for (TXPlace *place in _placeList) {
            if ([place isCorect]) {
                [notEmptyPlaceList addObject:place];
            }
        }
    }
    
    return notEmptyPlaceList;
}

- (void)switchPlace:(NSUInteger)index {
    if (![_placeList[index] isEmpty] && ![_placeList[index + 1] isEmpty]) {
        TXPlace *place = [TXPlace new];
        [place setWithPlace:_placeList[index]];
        [_placeList[index] setWithPlace: _placeList[index + 1]];
        [_placeList[index + 1] setWithPlace: place];
    }
}

- (BOOL)isValid {
    int count = 0;
    for (TXPlace *place in _placeList) {
        if ([place isCorect]) {
            count++;
        }
    }
    return count >= 2;
}


- (NSInteger)getPlaceIndex:(TXPlace *)place {
    NSUInteger n = [_placeList count];
    for (NSUInteger i = 0; i < n; i++) {
        TXPlace *p = _placeList[i];
        if (place == p) {
            return i;
        }
    }
    return -1;
}

@end
