////////////////////////////////////////////////////////////////////////////////
//
//  TXOrderDAO.h
//  Minimum
//
//  Created by Nail Sharipov on 12/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXOrder.h"
#import "TXPlace.h"
#import "SDStore.h"

@protocol TXOrderDAO<SDBaseDAO>

@required

- (TXOrder *)queryCurrent;

- (NSArray<TXOrder *> *)queryFromDate:(NSUInteger)dateTimeStamp;

- (NSArray<TXOrder *> *)queryFromIndex:(NSUInteger)index size:(NSUInteger)size;

- (BOOL)save:(TXOrder *)order;

- (BOOL)removeAllWithSyncDateOlderThen:(NSUInteger)syncDate andDateLaterThen:(NSUInteger)date;

- (BOOL)remove:(TXOrder *)order;

@end