////////////////////////////////////////////////////////////////////////////////
//
//  TXOrderShema.m
//  Minimum
//
//  Created by Nail Sharipov on 11/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXOrderShema.h"
#import "SDDatabaseManager.h"
#import "FMDB.h"

NSString* const TXOrderName = @"order";
NSString* const TXOrderToPlaceName = @"order_place";

@implementation TXOrderShema

- (NSString *)name {
    return TXOrderName;
}

- (NSString *)table {
    return [_databaseManager tableName:TXOrderName];
}

- (NSString *)tableOrderToPlace {
    return [_databaseManager tableName:TXOrderToPlaceName];
}

- (void)creatShemaVersion:(NSUInteger)version {
    NSArray<NSString *> *sql = @[
                                 [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ ("
                                  "orderId                  TEXT NOT NULL PRIMARY KEY,"
                                  "dateTimeStamp            INTEGER DEFAULT 0,"
                                  "syncTimeStamp            INTEGER DEFAULT 0"
                                  ");", self.table],
                                 [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ ("
                                  "pk                       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
                                  "orderId                  TEXT NOT NULL,"
                                  "placeId                  TEXT NOT NULL,"
                                  "position                 INTEGER DEFAULT 0"
                               ");", self.tableOrderToPlace]
                                ];

    for (NSString *s in sql) {
        [_databaseManager.database executeUpdate:s];
    }
}

- (void)dropShemaVersion:(NSUInteger)version {
    NSArray<NSString *> *sql = @[
                                 [NSString stringWithFormat:@"DROP TABLE IF EXISTS %@", self.table],
                                 [NSString stringWithFormat:@"DROP TABLE IF EXISTS %@", self.tableOrderToPlace],
                                 ];

    for (NSString *s in sql) {
        [_databaseManager.database executeUpdate:s];
    }
}

- (TXOrder *)instantiateModel:(FMResultSet *) result {
    TXOrder *order = [TXOrder new];
    order.orderId = [result stringForColumn:@"orderId"];
    order.dateTimeStamp = [result intForColumn:@"dateTimeStamp"];
    order.syncTimeStamp = [result intForColumn:@"syncTimeStamp"];
    return order;
}


@end
