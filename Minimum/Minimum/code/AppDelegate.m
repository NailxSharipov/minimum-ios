////////////////////////////////////////////////////////////////////////////////
//
//  AppDelegate.m
//  Minimum
//
//  Created by Nail Sharipov on 06/01/16.
//  Copyright © 2016 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "AppDelegate.h"

#import <GoogleMaps/GoogleMaps.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "TXLoginViewController.h"
#import "TXColorPallete.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self configureUIStyles];
    
    self.window.rootViewController = self.launch;
    [self.window makeKeyAndVisible];
    
    [GMSServices provideAPIKey:@"AIzaSyDY6cTCYHtimr5nR5tcMDM0M1dMQ3OFTjk"];
    [Fabric with:@[[Crashlytics class]]];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)configureUIStyles {
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [UIApplication sharedApplication].statusBarHidden = NO;
    
    [UINavigationBar appearance].barTintColor = [TXColorPallete navigationBarColor];
    [UINavigationBar appearance].translucent = NO;
    
    [UIToolbar appearance].barTintColor = [TXColorPallete navigationBarColor];
    [UIToolbar appearance].translucent = NO;
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor clearColor];
    shadow.shadowOffset = CGSizeMake(1, 0);
    
    UIFont *navbarFont = [UIFont systemFontOfSize:18.0];
    NSDictionary *titleAttributes = @{
                                      NSForegroundColorAttributeName : [TXColorPallete navigationBarTitleColor],
                                      NSShadowAttributeName : shadow,
                                      NSFontAttributeName : navbarFont
                                      };
    
    [[UINavigationBar appearance] setTitleTextAttributes:titleAttributes];
}

@end