////////////////////////////////////////////////////////////////////////////////
//
//  TXRouter.h
//  common
//
//  Created by Nail Sharipov on 19/12/15.
//  Copyright © 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@protocol TXPassable;


@protocol TXRouter <NSObject>

@required

@property(nonatomic, strong, readonly) UIViewController<TXPassable> *destination;

- (void)routeFrom:(UIViewController *)from;
- (void)routeFrom:(UIViewController *)from withIntent:(NSDictionary *)intent;


@end
