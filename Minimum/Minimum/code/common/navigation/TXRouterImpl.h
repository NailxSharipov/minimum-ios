////////////////////////////////////////////////////////////////////////////////
//
//  TXRouterImpl.h
//  common
//
//  Created by Nail Sharipov on 19/12/15.
//  Copyright © 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import "TXRouter.h"
#import "TXPassable.h"


typedef void (^TXRouteBlock)(UIViewController *from, UIViewController<TXPassable> *destination, NSDictionary *intent);


@interface TXRouterImpl : NSObject<TXRouter>

@property(nonatomic, strong) UIViewController<TXPassable> *destination;
@property(nonatomic, copy) TXRouteBlock routeBlock;


@end
