////////////////////////////////////////////////////////////////////////////////
//
//  TXRouterImpl.m
//  common
//
//  Created by Nail Sharipov on 19/12/15.
//  Copyright © 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXRouterImpl.h"


@implementation TXRouterImpl

//-------------------------------------------------------------------------------------------
#pragma mark - Interface Methods
//-------------------------------------------------------------------------------------------

- (void)routeFrom:(UIViewController *)from {
    _routeBlock(from, _destination, nil);
}

- (void)routeFrom:(UIViewController *)from withIntent:(NSDictionary *)intent {
    _destination.intent = intent;
    _routeBlock(from, _destination, intent);
}


@end
