////////////////////////////////////////////////////////////////////////////////
//
//  TXPassableViewController.h
//  common
//
//  Created by Шарипов Н.Г. on 18/12/15.
//  Copyright © 2015 Tensor. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import "TXPassable.h"


@interface TXPassableViewController : UIViewController<TXPassable>

@property (nonatomic, strong) NSDictionary *intent;


@end
