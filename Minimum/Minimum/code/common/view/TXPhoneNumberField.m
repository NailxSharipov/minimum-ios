////////////////////////////////////////////////////////////////////////////////
//
//  TXPhoneNumberField.m
//  TaxiMini
//
//  Created by Nail Sharipov on 22/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXPhoneNumberField.h"


@interface TXPhoneNumberField () <UITextFieldDelegate>


@end

@implementation TXPhoneNumberField {
    NSString *_unformatText;
    NSString *_prefix;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Initialization & Destruction
//-------------------------------------------------------------------------------------------

- (instancetype)init {
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

//-------------------------------------------------------------------------------------------
#pragma mark - <UITextFieldDelegate>
//-------------------------------------------------------------------------------------------

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self) {
        NSUInteger size = [string length];
        NSUInteger length = [_unformatText length];
        
        if ((size==0 && length > 0) || (size == 1 && ([string intValue] || [string isEqualToString:@"0"]) && length < 10)) {
            
            // backspace
            if (size == 0) {
                _unformatText = [_unformatText substringWithRange: NSMakeRange (0, length - 1)];
            } else {
                _unformatText = [NSString stringWithFormat:@"%@%@", _unformatText, string];
            }
            
            textField.text = [self getTextFormat:_unformatText];
            
            if (_phoneNumberEvent != nil) {
                [_phoneNumberEvent onChangedPhone: _unformatText];
            }
        }
        return NO;
    } else {
        return YES;
    }
    
}



-(BOOL)textFieldShouldBeginEditing:(UITextField*)textField {
    if (textField == self) {
        if (_unformatText == nil || _unformatText.length == 0) {
            [self hideHint];
        }
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (textField == self) {
        if (_unformatText == nil || _unformatText.length == 0) {
            [self showHint];
        }
    }
    return YES;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Overridden Methods
//-------------------------------------------------------------------------------------------

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

// fix switch language button crush
- (id)customOverlayContainer {
    return self;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Public Methods
//-------------------------------------------------------------------------------------------

- (void)setUnformatText:(NSString *)unformatText {
    if (unformatText == nil || unformatText.length == 0) {
        _unformatText = @"";
        [self showHint];
    } else {
        _unformatText = unformatText;
        self.text = [self getTextFormat:_unformatText];
        [self hideHint];
    }
}

- (NSString *)unformatText {
    return _unformatText;
}

- (void)setPrefix:(NSString *)prefix {
    if (prefix == nil) {
        _prefix = @"";
    } else {
        _prefix = prefix;
    }
}

- (NSString *)prefix {
    return _prefix;
}

//-------------------------------------------------------------------------------------------
#pragma mark - Private Methods
//-------------------------------------------------------------------------------------------

- (void)commonInit {
    self.delegate = self;
     _unformatText = @"";
    _prefix = @"+7";
    [self showHint];
}

- (NSString *)getTextFormat:(NSString *)unformatText {
    NSInteger length = [unformatText length];
    NSString *text;
    
    NSString *prefix = nil;
    if (_prefix == nil || _prefix.length == 0 || [_prefix isEqualToString:@"8"]) {
        prefix = @"+7";
    } else {
        prefix = _prefix;
    }
    
    if (length == 0) {
        text = @"";
    } else if (length < 3) {
        text = [NSString stringWithFormat:@"%@ (%@", prefix, unformatText];
    } else if (length < 6) {
        NSString * abc = [unformatText substringWithRange: NSMakeRange (0, 3)];
        NSString * end = [unformatText substringWithRange: NSMakeRange (3, length - 3)];
        
        text = [NSString stringWithFormat:@"%@ (%@) %@", prefix, abc, end];
    } else {
        NSString * abc = [unformatText substringWithRange: NSMakeRange (0, 3)];
        NSString * def = [unformatText substringWithRange: NSMakeRange (3, 3)];
        NSString * end = [unformatText substringWithRange: NSMakeRange (6, length - 6)];
        
        text = [NSString stringWithFormat:@"%@ (%@) %@-%@", prefix, abc, def, end];
    }
    
    return text;
}

- (void)showHint {
    self.text = @"(000)-000-0000";
    self.textColor = [UIColor lightGrayColor];
    UIFont* italicFont = [UIFont italicSystemFontOfSize:self.font.pointSize];
    self.font = italicFont;
}

- (void)hideHint {
    if (_unformatText == nil || _unformatText.length == 0) {
        self.text = @"";
    }
    self.textColor = [UIColor darkTextColor];
    UIFont* font = [UIFont systemFontOfSize:self.font.pointSize];
    self.font = font;
}

+ (NSString *)getTextFormat:(NSString *)unformatText prefix:(NSString *)prefix {
    NSInteger length = [unformatText length];
    NSString *text;
    NSString *correctPrefix = nil;
    if (prefix == nil || prefix.length == 0 || [prefix isEqualToString:@"8"]) {
        correctPrefix = @"+7";
    } else {
        correctPrefix = prefix;
    }
    if (length == 0) {
        text = @"";
    } else if (length < 3) {
        text = [NSString stringWithFormat:@"%@ (%@", correctPrefix, unformatText];
    } else if (length < 6) {
        NSString * abc = [unformatText substringWithRange: NSMakeRange (0, 3)];
        NSString * end = [unformatText substringWithRange: NSMakeRange (3, length - 3)];
        
        text = [NSString stringWithFormat:@"%@ (%@) %@", correctPrefix, abc, end];
    } else {
        NSString * abc = [unformatText substringWithRange: NSMakeRange (0, 3)];
        NSString * def = [unformatText substringWithRange: NSMakeRange (3, 3)];
        NSString * end = [unformatText substringWithRange: NSMakeRange (6, length - 6)];
        
        text = [NSString stringWithFormat:@"%@ (%@) %@-%@", correctPrefix, abc, def, end];
    }
    
    return text;
}

@end
