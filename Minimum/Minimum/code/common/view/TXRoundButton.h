////////////////////////////////////////////////////////////////////////////////
//
//  TXRoundButton.h
//  Minimum
//
//  Created by Nail Sharipov on 26/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>


@interface TXRoundButton : UIButton

/**
 * Заливать фон кнопки, или только бордер
 */
@property (nonatomic) BOOL filled;
@property (strong, nonatomic) NSNumber *borderWidth;
@property (strong, nonatomic) NSNumber *cornerRadius;
@property (strong, nonatomic) UIColor *fontColorInNormalState;
@property (strong, nonatomic) UIColor *fontColorInPressedState;
@property (strong, nonatomic) UIColor *fontColorInDisabledState;
@property (strong, nonatomic) UIColor *backgroundColorInNormalState;
@property (strong, nonatomic) UIColor *backgroundColorInPressedState;
@property (strong, nonatomic) UIColor *borderColor;


@end
