////////////////////////////////////////////////////////////////////////////////
//
//  PhoneNumberField.h
//  TaxiMini
//
//  Created by Nail Sharipov on 22/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>


@protocol TXPhoneNumberEvent <NSObject>

@optional

- (void)onChangedPhone:(NSString *)phone;

@end

@interface TXPhoneNumberField : UITextField

@property (strong, nonatomic) NSString *helpText;
@property (strong, nonatomic) NSString *prefix;
@property (strong, nonatomic) NSString *unformatText;
@property (weak) id<TXPhoneNumberEvent> phoneNumberEvent;

+ (NSString *)getTextFormat:(NSString *)unformatText prefix:(NSString *)prefix;

@end
