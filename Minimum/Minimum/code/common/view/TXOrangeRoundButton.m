////////////////////////////////////////////////////////////////////////////////
//
//  TXOrangeRoundButton.m
//  Minimum
//
//  Created by Nail Sharipov on 26/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXOrangeRoundButton.h"


@implementation TXOrangeRoundButton

- (UIColor *)fontColorInNormalState {
    return [UIColor blackColor];
}

- (UIColor *)fontColorInPressedState {
    return [UIColor colorWithWhite:231.0f/255.0f alpha:1.0f];
}

- (UIColor *)fontColorInDisabledState {
    return [UIColor colorWithWhite:231.0f/255.0f alpha:1.0f];
}

- (UIColor *)borderColor {
    return [UIColor colorWithRed:1.0f green:108.0f/255.0f blue:67.0f/255.0f alpha:1.0f];
}

- (void)setFilled:(BOOL)filled {
    [super setFilled:filled];
    
    self.layer.borderColor = [UIColor whiteColor].CGColor;
}


@end
