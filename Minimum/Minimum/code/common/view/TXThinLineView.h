////////////////////////////////////////////////////////////////////////////////
//
//  TXThinLineView.h
//  Minimum
//
//  Created by Nail Sharipov on 16/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, TSLineAlign) {
    TSLineAlignBottom = 0,
    TSLineAlignTop = 1,
    TSLineAlignLeft = 2,
    TSLineAlignRight = 3,
};

@interface TXThinLineView : UIView

@property (strong, nonatomic) UIColor *lineColor;

@property (nonatomic) CGFloat depth;

@property (nonatomic) NSNumber *align;

@end

