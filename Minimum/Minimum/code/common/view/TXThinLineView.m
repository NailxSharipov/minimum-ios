////////////////////////////////////////////////////////////////////////////////
//
//  TXThinLineView.m
//  Minimum
//
//  Created by Nail Sharipov on 16/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXThinLineView.h"


@implementation TXThinLineView

//-------------------------------------------------------------------------------------------
#pragma mark - Initialization & Destruction
//-------------------------------------------------------------------------------------------

- (instancetype) init {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGFloat lineHeight = [self lineHeight];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetAllowsAntialiasing(context, YES);
    CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
    CGContextSetFillColorWithColor(context, self.lineColor.CGColor);
    CGContextSetLineWidth(context, lineHeight);
    if (_align.integerValue == TSLineAlignBottom) {
        CGContextFillRect(context, CGRectMake(0, rect.size.height - lineHeight, rect.size.width, lineHeight));
    } else if (_align.integerValue == TSLineAlignTop) {
        CGContextFillRect(context, CGRectMake(0, 0, rect.size.width, lineHeight));
    } else if (_align.integerValue == TSLineAlignLeft) {
        CGContextFillRect(context, CGRectMake(0, 0, lineHeight, rect.size.height));
    } else if (_align.integerValue == TSLineAlignRight) {
        CGContextFillRect(context, CGRectMake(0, rect.size.width - lineHeight, lineHeight, rect.size.height));
    }
}

- (CGFloat)lineHeight {
    if (_depth > 0.0f) {
        return (_depth / [UIScreen mainScreen].scale) / 2.0f;
    } else {
        return (1.0f / [UIScreen mainScreen].scale) / 2.0f;
    }
}


@end

