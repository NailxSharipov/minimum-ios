////////////////////////////////////////////////////////////////////////////////
//
//  TXTextFieldModern.m
//  TaxiMini
//
//  Created by Nail Sharipov on 23/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#import "TXTextFieldModern.h"


@interface TXTextFieldModern ()<UITextFieldDelegate>

@end

@implementation TXTextFieldModern

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];        
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

// hide keyboard
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self) {
        [textField resignFirstResponder];
    }
    return YES;
}

// fix switch language button crush
- (id)customOverlayContainer {
    return self;
}

- (void) commonInit {
    [self setDelegate:self];
}

@end
